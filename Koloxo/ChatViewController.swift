
import UIKit
import TwilioChatClient

class ChatViewController: UIViewController {
    
    var chatClient:TwilioChatClient?
    var channel:TCHChannel?
    var messages:[TCHMessage] = []
    
    var userid:Int = 10
    var propertyID:Int = 10
    var partnerID:Int = 10
    
    let messageRetrieveCount = 10
    
    let chatManager = ChatManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Started")
        login(with: "18")
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        logout()
    }
    
    @IBOutlet weak var messageField: UITextField!
    @IBAction func sendBTN(_ sender: UIButton) {
        self.sendMessage()
    }
    

    func login(with id:String){
        chatManager.getAccessToken(identity: "\(id)") { (token) in
            self.chatManager.createChatClient(with: token, delegate: self) { (clientR) in
                self.chatClient = clientR
//                if let _ = self.chatClient{
//                    chatManager.createOrJoinChannel(userID: self.userid, partnerID: self.partnerID, propertyID: self.propertyID, client: self.chatClient!, channelR: { (channelR) in
//                        self.channel = channelR
//                    })
//                }
            }
        }
        
        
       
    }
    
    func logout()
    {
        if let _ = chatClient
        {
            chatClient!.delegate = nil
            chatClient!.shutdown()
            self.chatClient = nil
        }
    }
    
    
    
    func sendMessage(){
        if let messages = self.channel?.messages{
            let messageOptions = TCHMessageOptions().withBody("\(messageField.text ?? "123")")
            messages.sendMessage(with: messageOptions, completion: { (result, message) in
                print("Sent message: \(self.messageField.text ?? "123") ",result.isSuccessful())
            })
        }
    }
    
    func loadMessages(){
        messages.removeAll()
        if channel?.synchronizationStatus == .all
        {   //UInt(self.messageCount)
        
            channel?.messages?.getLastWithCount(UInt(messageRetrieveCount))
            { (result, items) in

                self.messages = items ?? [TCHMessage]()
                //TODO: TableView reload
                //TODO:scroll to bottom of table
                for item in self.messages{
                    print(item.body as Any)
                }
            }
        }
    }
   

}

extension ChatViewController: TwilioChatClientDelegate{
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        if status == .completed{
            chatManager.createOrJoinChannel(channelSend: "channelName", client: self.chatClient!, channelR: { (channelR) in
                self.channel = channelR
                self.loadMessages()
            })
        }
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel,
                    messageAdded message: TCHMessage) {
        self.messages.append(message)
        //TODO: Table reload
       //TODO: scroll to bottom of table
    }
}
