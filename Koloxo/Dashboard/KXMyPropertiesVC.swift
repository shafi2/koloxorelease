//
//  KXMyPropertiesVC.swift
//  KoloxoScreens
//
//  Created by Appzoc on 08/06/18.
//  Copyright © 2018 AppZoc. All rights reserved.
//

import UIKit
import Kingfisher

class myPropertiesCell: UITableViewCell
{
    @IBOutlet var propertyImage: UIImageView!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var visitorsCountLBL: UILabel!
    @IBOutlet var editBTN: UIButton!
    @IBOutlet var chatBTN: UIButton!
    @IBOutlet var receivedRequestBTN: UIButton!
    @IBOutlet var offersBTN: UIButton!
    
    @IBOutlet var draftView: UIView!
    @IBOutlet var visitorsView: UIView!
    @IBOutlet var editView: UIView!
    @IBOutlet var menuBottomView: UIView!
    @IBOutlet var waitingForApprovalView: UIView!
    @IBOutlet var sellRentIndicator: UILabel!
    @IBOutlet var rotationContainer: UIView!
    @IBOutlet var currencyIndicator: UILabel!
    
    
    override func awakeFromNib() {
        rotationContainer.transform = CGAffineTransform.identity
        rotationContainer.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi * 0.25))
    }
    
}

class myPropertiesCell_draft: UITableViewCell
{
    @IBOutlet var propertyImage: UIImageView!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var visitorsCountLBL: UILabel!
    @IBOutlet var editBTN: UIButton!
    
    @IBOutlet var draftView: UIView!
    @IBOutlet var visitorsView: UIView!
    @IBOutlet var editView: UIView!
    @IBOutlet var menuBottomView: UIView!
    @IBOutlet var waitingForApprovalView: UIView!
    @IBOutlet var currencyIndicator: UILabel!
    
    
    @IBOutlet var rotationContainer: UIView!
    @IBOutlet var sellRentIndicator: UILabel!
    
    override func awakeFromNib() {
        rotationContainer.transform = CGAffineTransform.identity
        rotationContainer.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi * 0.25))
    }
    
}

class myPropertiesCellApproval: UITableViewCell
{
    @IBOutlet var propertyImage: UIImageView!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var visitorsCountLBL: UILabel!
    @IBOutlet var editBTN: UIButton!
    
    @IBOutlet var draftView: UIView!
    @IBOutlet var visitorsView: UIView!
    @IBOutlet var editView: UIView!
    @IBOutlet var menuBottomView: UIView!
    @IBOutlet var waitingForApprovalView: UIView!
    @IBOutlet var currencyIndicator: UILabel!
    
    @IBOutlet var rejectActionBTN: UIButton!
    @IBOutlet var pendingRejectLBL: UILabel!
    
    @IBOutlet var timerIconImage: UIImageView!
    @IBOutlet var rotationContainer: UIView!
    @IBOutlet var sellRentIndicator: UILabel!
    
    override func awakeFromNib() {
        rotationContainer.transform = CGAffineTransform.identity
        rotationContainer.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi * 0.25))
    }
    
}

class KXMyPropertiesVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    
    var contentArray = [[String:Any]]()
    var userID = Credentials.shared.userId
    final var isFromAddProperty: Bool = false
    final var propName = ""
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!
    
    fileprivate var chatMembersInfo:[KXChatMyPropertyModel] = []

    override func viewDidLoad()
    {
        super.viewDidLoad()
         self.getAllData()
//        Webservices.getMethod(url: "myAddedProperties?user_id=\(userID)")
//        { (isFetched, resultData) in
//            if isFetched{
//                print(resultData)
//                self.contentArray = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
//
//                self.contentTV.reloadData()
//            }
//        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    func getAllData()
    {
        Webservices.getMethod(url: "myAddedProperties?user_id=\(userID)"){
            (isFetched, resultData) in
            if isFetched{
                //print(resultData)
                self.contentArray = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
                //self.contentArray.reverse()
                self.contentTV.reloadData()
            }
        }
        
    }
    
    @IBAction func chatBTNTapped(_ sender: UIButton)
    {
        let chatVC = self.storyboardChat?.instantiateViewController(withIdentifier: "KXChatMembersViewController") as! KXChatMembersViewController
        chatVC.passingPropertyID = (contentArray[sender.tag])["id"] as? Int ?? 0
         chatVC.propertyName = (contentArray[sender.tag])["property_name"] as? String ?? "Chat Request"
        chatVC.propertyOwnerId = (contentArray[sender.tag])["user_id"] as? Int ?? 0
       self.navigationController?.pushViewController(chatVC, animated: true)
        
    }
    
    @IBAction func editBTNTapped(_ sender: UIButton)
    {
        let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        vc.passingPropertyID = (contentArray[sender.tag])["id"] as? Int ?? 0
        vc.editingMode = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func receivedRequestBTNTapped(_ sender: UIButton) {
        let requestsVC = storyboardDashboard?.instantiateViewController(withIdentifier: "KXPropertyListViewController") as! KXPropertyListViewController
        if let id = contentArray[sender.tag]["id"] as? Int{
            requestsVC.passingPropertyID = id
        }
        self.navigationController?.pushViewController(requestsVC, animated: true)
    }
    
    @IBAction func offersBTNTapped(_ sender: UIButton) {
        let propertyTitle = contentArray[sender.tag]["property_name"] as? String ?? ""
        let propertyType = contentArray[sender.tag]["property_type"] as? Int ?? -1
        
        let vc = storyboardOfferList!.instantiateViewController(withIdentifier: "KX_OfferList") as! KX_OfferList
        vc.propetyTittle = propertyTitle
        vc.propetyTyp = "\(propertyType)"
        vc.propetyId = "\(contentArray[sender.tag]["id"] as? Int ?? 0)"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func rejectedBTNTapped(_ sender: UIButton) {
        let reason = contentArray[sender.tag]["reason"] as? String ?? ""
        
        guard reason != "" else {
            return
        }
        
        BaseAlert.alert(withTitle: "Property rejected", message: reason, tintColor: .themeColor, andPresentOn: self)
        

    }
    
    @IBAction func Back_Button(_ sender: Any)
    {
        if isFromAddProperty {
            let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
            
            self.navigationController?.pushViewController(vc, animated: true)

        }else {
            navigationController?.popViewController(animated: true)
        }
        
        
    }
    
    @IBAction func Humberger_menu(_ sender: Any)
    {
        showSideMenu()
    }
    
    
    // Tab Actions
    @IBAction func tabHomeTapped(_ sender: UIButton)
    {
                let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton)
    {
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton)
    {
                let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                vc.segueType = .search
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton)
    {
                let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                vc.segueType = .chat
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton)
    {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


// TableView Delegates
extension KXMyPropertiesVC:UITableViewDelegate,UITableViewDataSource
    
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        
    {
        
        return contentArray.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        
    {
        
    
        let singleModel: KXChatMyPropertyModel = KXChatMyPropertyModel()
        singleModel.propertyId = (contentArray[indexPath.row])["id"] as? Int ?? 0
        singleModel.propertyName = (contentArray[indexPath.row])["property_name"] as? String ?? ""
        singleModel.propertyLocation = (contentArray[indexPath.row])["address"] as? String ?? ""
        singleModel.propertyImage = (contentArray[indexPath.row])["image"] as? String ?? ""
        singleModel.property_type = (contentArray[indexPath.row])["property_type"] as? Int ?? 1
        singleModel.propertyOwnerId = (contentArray[indexPath.row])["user_id"] as? Int ?? 0
        
       
        
        //debugPrint(singleModel as AnyObject)
        chatMembersInfo.append(singleModel)
        let i = (contentArray[indexPath.row])["states_completed"] as! Int
        
        if (contentArray[indexPath.row])["admin_approval"] as? Int == 1  {
            
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "myPropertiesCell") as! myPropertiesCell
            
            cell.propertyTitleLBL.text = (contentArray[indexPath.row])["property_name"] as? String ?? "No Title"
            
            //cell.sellRentIndicator.transform = CGAffineTransform(rotationAngle: CGFloat.pi/4)
            
            if let sellType = (contentArray[indexPath.row])["sale_type"] as? [String:Any]{
                if sellType["type"] as! String == "RENT"{
                    cell.sellRentIndicator.text = "RENT"
                }else{
                    cell.sellRentIndicator.text = "SELL"
                }
            }
            
            cell.propertyLocationLBL.text = (contentArray[indexPath.row])["address"] as? String ?? "No Address"
            
            cell.propertyPriceLBL.text = "\(((contentArray[indexPath.row])["price"] as? Double ?? 0.0).showPriceInDollar())"  //showPrice2Precision()
            cell.currencyIndicator.text = "\((contentArray[indexPath.row])["currency_code"] ?? "")"
            cell.visitorsCountLBL.text = (contentArray[indexPath.row])["visitors_count"] as? String ?? "00"
            
            cell.editBTN.tag = indexPath.row
            cell.receivedRequestBTN.tag = indexPath.row
            cell.offersBTN.tag = indexPath.row
            cell.chatBTN.tag = indexPath.row
            
            let receivedRequestCount = (contentArray[indexPath.row])["property_requests_count"] as? Int ?? 0
            if receivedRequestCount > 99 && receivedRequestCount != 0 {
                cell.receivedRequestBTN.setTitle("Received Requests(99+)", for: [])
            }else if receivedRequestCount == 0 {
                cell.receivedRequestBTN.setTitle("Received Requests(0)", for: [])  //0 Requests
            }else {
                cell.receivedRequestBTN.setTitle("Received Requests (\(receivedRequestCount))", for: [])
            }
            
            let property_offers_count = (contentArray[indexPath.row])["property_offers_count"] as? Int ?? 0
            if property_offers_count > 99 && property_offers_count != 0 {
                cell.offersBTN.setTitle("Offers(99+)", for: [])
                cell.offersBTN.setTitleColor(UIColor.acceptGreen, for: [])
            }else if property_offers_count == 0 {
                cell.offersBTN.setTitle("Offers(0)", for: []) //0 Offers
                cell.offersBTN.setTitleColor(UIColor.themeGray, for: [])

            }else {
                cell.offersBTN.setTitle("Offers(\(property_offers_count))", for: [])
                cell.offersBTN.setTitleColor(UIColor.acceptGreen, for: [])

                
            }

            let property_chats_count = (contentArray[indexPath.row])["property_chats_count"] as? Int ?? 0
            if property_chats_count > 99 && property_chats_count != 0 {
                cell.chatBTN.setTitle("Chats(99+)", for: [])
            }else if property_offers_count == 0 {
                cell.chatBTN.setTitle("Chats(0)", for: [])  //0 Chats
            }else {
                cell.chatBTN.setTitle("Chats(\(property_chats_count))", for: [])
            }
            
            let imageURL = URL(string: baseImageURL+((contentArray[indexPath.row])["image"] as? String ?? "No Image"))
            
            
            cell.propertyImage.kf.setImage(with: imageURL, placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"),
                                           
                                           options: [.requestModifier(getImageAuthorization())],
                                           
                                           progressBlock: nil,
                                           
                                           completionHandler: nil)
            
            
            return cell
            
        }
        else if i < 5
        {
            
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "myPropertiesCell_draft") as! myPropertiesCell_draft
            
            cell.propertyTitleLBL.text = (contentArray[indexPath.row])["property_name"] as? String ?? "No Title"
           
            
            cell.propertyLocationLBL.text = (contentArray[indexPath.row])["address"] as? String ?? "No Address"
            
            cell.propertyPriceLBL.text = ((contentArray[indexPath.row])["price"] as? Double ?? 0.0).showPriceInDollar() //showPriceInKilo()
            cell.currencyIndicator.text = "\((contentArray[indexPath.row])["currency_code"] ?? "")"
            cell.visitorsCountLBL.text = (contentArray[indexPath.row])["visitors_count"] as? String ?? "00"
            
            cell.editBTN.tag = indexPath.row
            
            if let sellType = (contentArray[indexPath.row])["sale_type"] as? [String:Any]{
                if sellType["type"] as! String == "RENT"{
                    cell.sellRentIndicator.text = "RENT"
                }else{
                    cell.sellRentIndicator.text = "SELL"
                }
            }
            
            let imageURL = URL(string: baseImageURL + ((contentArray[indexPath.row])["image"] as? String ?? "No Image"))
            
            
            
            cell.propertyImage.kf.setImage(with: imageURL, placeholder:  #imageLiteral(resourceName: "buildingPlaceHolder"),
                                           
                                           options: [.requestModifier(getImageAuthorization())],
                                           
                                           progressBlock: nil,
                                           
                                           completionHandler: nil)
            
            
            return cell
        }else
        {
            
            //myPropertiesCellApproval
            let cell = tableView.dequeueReusableCell(withIdentifier: "myPropertiesCellApproval") as! myPropertiesCellApproval
            
            
            
            if  (contentArray[indexPath.row])["admin_approval"] as? Int == 3  { // Rejected State
                cell.rejectActionBTN.isHidden = false
                cell.pendingRejectLBL.text = "Rejected" //Pending for approval from Koloxo Home
                cell.pendingRejectLBL.textColor = UIColor.rejectRedLight
                cell.timerIconImage.image = #imageLiteral(resourceName: "rejectedPro")
                
            }else {
                // pending state - approval-3
                cell.rejectActionBTN.isHidden = true
                cell.pendingRejectLBL.text = "Pending for approval from Koloxo Home" //Pending for approval from Koloxo Home
                cell.pendingRejectLBL.textColor = UIColor.pendingYellow
                cell.timerIconImage.image = #imageLiteral(resourceName: "timerIcon.png")

            }
            cell.rejectActionBTN.tag = indexPath.row
            
            
            cell.propertyTitleLBL.text = (contentArray[indexPath.row])["property_name"] as? String ?? "No Title"
            
            cell.propertyLocationLBL.text = (contentArray[indexPath.row])["address"] as? String ?? "No Address"
            
            cell.propertyPriceLBL.text = ((contentArray[indexPath.row])["price"] as? Double ?? 0.0).showPriceInDollar()
            cell.currencyIndicator.text = "\((contentArray[indexPath.row])["currency_code"] ?? "")"
            cell.visitorsCountLBL.text = (contentArray[indexPath.row])["visitors_count"] as? String ?? "00"
            
            self.propName = (contentArray[indexPath.row])["property_name"] as? String ?? "No Title"
            
            cell.editBTN.tag = indexPath.row
            
            if let sellType = (contentArray[indexPath.row])["sale_type"] as? [String:Any]{
                if sellType["type"] as! String == "RENT"{
                    cell.sellRentIndicator.text = "RENT"
                }else{
                    cell.sellRentIndicator.text = "SELL"
                }
            }
            
            
            let imageURL = URL(string: baseImageURL + ((contentArray[indexPath.row])["image"] as? String ?? "No Image"))
            
            
            
            cell.propertyImage.kf.setImage(with: imageURL, placeholder:  #imageLiteral(resourceName: "buildingPlaceHolder"),
                                           
                                           options: [.requestModifier(getImageAuthorization())],
                                           
                                           progressBlock: nil,
                                           
                                           completionHandler: nil)
            
            

            return cell

        }
        
        
        
        
        
//        return UITableViewCell()
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if let _ = tableView.cellForRow(at: indexPath) as? myPropertiesCell_draft {
            //print("DraftCelllllllllllll")
            return
        }
       // print("Not a draft celllllllllll")
        
        
        
        let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
        vc.isFromMyProperty = true
        let propertyId = (contentArray[indexPath.row])["id"] as? Int ?? 0
        print(propertyId)
        if propertyId == 0 {
            
            return
            
        }
        
        if let movingIn = contentArray[indexPath.row]["moving_in"] as? String, let movingOut = contentArray[indexPath.row]["moving_out"] as? String {
            if let inDate = movingIn.components(separatedBy: " ").first, let outDate = movingOut.components(separatedBy: " ").first {
                vc.moving_inMyrequest = inDate
                vc.durationMyRequest = outDate
                if let propertyModel = (contentArray[indexPath.row])["property"] as? [String:Any] {
                    if let typeof = propertyModel["property_type"] as? Int {
                        vc.typeOfProperty = typeof // 1-sell, rent - 2
                        print("typeof",typeof)
                    }else {
                        vc.typeOfProperty = 1 // sell
                    }
                }
                
                print("vc.moving_inMyrequest",inDate)
                print("vc.durationMyRequest",outDate)
                
            }
            
        }
        
        if let propertyModel = (contentArray[indexPath.row])["property"] as? [String:Any] {
            if let typeof = propertyModel["property_type"] as? Int {
                vc.typeOfProperty = typeof // 1-sell, rent - 2
                print("typeof",typeof)
            }else {
                vc.typeOfProperty = 1 // sell
            }
        }

        
        vc.propertyId = propertyId
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let i = (contentArray[indexPath.row])["states_completed"] as! Int
        if  i < 5
        {
            //print("i : ",i)
            return 134
            
        }
        else {
            return 190
        }
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            
            BaseAlert.completionAlert(withTitle: "Alert", message: "Are you sure you want to delete property ?", tintColor: UIColor.themeColor, andPresentOn: self, completion: {
                let propertyId = (self.contentArray[indexPath.row])["id"] as? Int ?? 0
                Webservices.getMethod(url: "deleteProperty?property_id=\(propertyId)"){ (isFetched, resultData) in
                    if isFetched {
                        //print("Deleted Property")
                        self.getAllData()
                    }
                }
            })
            //print("ok")
        }
        
    }
    
}








//func getImageAuthorization() -> AnyModifier
//{
//    let modifier = AnyModifier
//    { request in
//        var requestMutable = request
//        let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//        let base64Credentials = credentialData.base64EncodedString(options: [])
//
//        requestMutable.setValue("Basic \(base64Credentials)", forHTTPHeaderField: "Authorization")
//
//        return requestMutable
//    }
//    return modifier
//}













