//
//  KXMyRequestVC.swift
//  KoloxoScreens
//
//  Created by Appzoc on 08/06/18.
//  Copyright © 2018 AppZoc. All rights reserved.
//

import UIKit

class myRequestCell: UITableViewCell
{
    @IBOutlet var propertyImage: UIImageView!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var visitorsCountLBL: UILabel!
    
    //AceptedView
    @IBOutlet var requestStatus: UILabel!
    @IBOutlet var offerStatus: UILabel!
    @IBOutlet var requestStatusPendingLBL: UILabel!
    
    @IBOutlet var requestAcceptedView: UIView!
    
    @IBOutlet var requestPendingView: UIView!
    
    @IBOutlet var offerStatusView: UIView!
    
    @IBOutlet var SellRentIndicator: UILabel!
    
    @IBOutlet var sellTypeContainer: UIView!
    @IBOutlet var rateBTN: UIButton!
    @IBOutlet var iconImage: UIImageView!
    @IBOutlet var widthOfferStatusView: NSLayoutConstraint!
    @IBOutlet var movingInOutView: UIView!
    @IBOutlet var movingInLBL: UILabel!
    @IBOutlet var movingOutLBL: UILabel!
    
    override func awakeFromNib() {
        
        sellTypeContainer.transform = CGAffineTransform.identity
        sellTypeContainer.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi * 0.25))
    }
}


class KXMyRequestVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!
    
    var reviewQuestionsData:[ReviewQuestionModel] = []
    var contentArray = [[String:Any]]()
    var userID = Credentials.shared.userId
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        getWebData()
        getReviewQuestions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    
    fileprivate func getWebData(){
        Webservices.getMethod(url: "propertyRequestsSent?user_id=\(userID)")
        { (isFetched, resultData) in
            print(resultData)
            
            let resultArray = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
            for item in resultArray {
                if let status = item["deal_confirmation"] as? Int {
                    if status == 0 || status == 10 { // 0 -> pending, 10 -> not yet requested any offer
                        self.contentArray.append(item)
                    }
                }
            }
            
            self.contentTV.reloadData()
        }
    }
    
    private func getReviewQuestions() {
        //ReviewQuestions
        Webservices.getMethodWithoutLoading(url: "getReviewQuestions?user_id=\(Credentials.shared.userId)") { (isComplete, json) in
            if isComplete{
                do{
                    if let trueData = json["Data"] as? [[String:Any]]{
                        let data = try JSONSerialization.data(withJSONObject: trueData, options: .prettyPrinted)
                        let parsedData = try JSONDecoder().decode([ReviewQuestionModel].self, from: data)
                        self.reviewQuestionsData = parsedData
                        print("Succeess-getReviewQuestions")
                    }else{
                        print("Failed conversion")
                    }
                }catch{
                    print(error)
                }
            }
            
        }


    }
    
    

    @IBAction func Back_Button(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_menu(_ sender: Any)
    {
        showSideMenu()
    }
    
    
    // Tab Actions
    @IBAction func tabHomeTapped(_ sender: UIButton)
    {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton)
    {
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton)
    {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton)
    {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton)
    {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func rateBTNTapped(_ sender: UIButton) {
        // rate the property owner
        guard !reviewQuestionsData.isEmpty else {
//            getReviewQuestions()
            return
        }
        
        if let propertyModel = (contentArray[sender.tag])["property"] as? [String:Any] {
            if let propertyId = propertyModel["id"] as? Int, let userId = propertyModel["user_id"] as? Int {
                let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXRateUserPopUpController") as! KXRateUserPopUpController
                vc.questionsSource = self.reviewQuestionsData.filter({$0.question_type == 1})
                vc.passingPropertyID = propertyId  //myRentalData[sender.tag].propertyID
                vc.toUserID = userId
                vc.ratingType = .owner
                self.present(vc, animated: true, completion: nil)
            }else {
                print("parsing errorrr11111")
            }
            
        }else {
            print("parsing errorrr2222")

        }
        
    }
    
    
}

// TableView Delegates
extension KXMyRequestVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return self.contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myRequestCell") as! myRequestCell
        
        let propertyModel = (contentArray[indexPath.row])["property"] as? [String:Any] ?? [String:Any]()
        cell.propertyTitleLBL.text = propertyModel["property_name"] as? String ?? "No Title"
        cell.propertyLocationLBL.text = propertyModel["address"] as? String ?? "No Address"
        
        cell.propertyPriceLBL.text = (propertyModel["usd_price"] as? Double ?? 0.0).showPriceInDollar() //showPriceInKilo()
        cell.rateBTN.tag = indexPath.row
        
        
        if let sellType = propertyModel["property_type"] as? Int {
            if sellType == 1{
                cell.SellRentIndicator.text = "SELL"
                cell.movingInOutView.isHidden = true
            }else{
                cell.SellRentIndicator.text = "RENT"
                
                if let movingIn = contentArray[indexPath.row]["moving_in"] as? String, let movingOut = contentArray[indexPath.row]["moving_out"] as? String {
                    if let inDate = movingIn.components(separatedBy: " ").first, let outDate = movingOut.components(separatedBy: " ").first {
                        print("vc.moving_",inDate)
                        print("outttttt",outDate)
                        if  inDate.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy") != "", outDate.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy") != ""{
                            cell.movingInLBL.text = inDate.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                            cell.movingOutLBL.text = outDate.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                            cell.movingInOutView.isHidden = false
                        }else{
                            cell.movingInOutView.isHidden = true
                        }
                    }else {
                        cell.movingInOutView.isHidden = true
                    }
                }else {
                    cell.movingInOutView.isHidden = true
                }
            }
        }
        
        if let status = contentArray[indexPath.row]["status"] as? Int {
            if  status == 1{
                cell.requestPendingView.isHidden = true
                
                cell.requestStatus.text = "Accepted"
                cell.requestStatus.textColor = UIColor.acceptGreen
            }else if  status == 0{
                cell.requestPendingView.isHidden = false
                cell.requestStatusPendingLBL.text = "Request Pending"
                cell.requestStatusPendingLBL.textColor = UIColor.pendingYellow
                cell.iconImage.image = #imageLiteral(resourceName: "timerIcon.png")

                cell.requestStatus.text = "Pending"
                cell.requestStatus.textColor = UIColor.pendingYellow
            }else if  status == 2{
                cell.requestPendingView.isHidden = false
                cell.requestStatusPendingLBL.text = "Request Rejected"
                cell.requestStatusPendingLBL.textColor = UIColor.rejectRedLight
                cell.iconImage.image = #imageLiteral(resourceName: "rejectedPro")

                cell.requestStatus.text = "Rejected"
                cell.requestStatus.textColor = UIColor.rejectRed
            }
        }
        
        
        if let status = contentArray[indexPath.row]["deal_confirmation"] as? Int{
            cell.offerStatusView.isHidden = false
            cell.widthOfferStatusView.constant = 128
            if  status == 1{
                cell.offerStatus.text = "Accepted"
                cell.offerStatus.textColor = UIColor.acceptGreen
            }else if  status == 0{
                cell.offerStatus.text = "Pending"
                cell.offerStatus.textColor = UIColor.pendingYellow
            }else if  status == 2{
                cell.offerStatus.text = "Rejected"
                cell.offerStatus.textColor = UIColor.rejectRed
            }else {
             //10 - not send any offers
                cell.widthOfferStatusView.constant = 0
                cell.offerStatusView.isHidden = true
            }
            cell.layoutIfNeeded()
        }
        
        
        
        if let PropertyImageArray = propertyModel["property_images"] as? [[String:Any]]{
            if PropertyImageArray.count > 0
            {
                for item in PropertyImageArray{
                    if item["type_id"] as! Int == 6{
                        cell.propertyImage.kf.setImage(with: URL(string: baseImageURL + (item["image"] as! String)), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"),
                                                       options: [.requestModifier(getImageAuthorization())],
                                                       progressBlock: nil,
                                                       completionHandler: nil)
                        
                    }
                }

            }
            else
            {
                cell.propertyImage.image = #imageLiteral(resourceName: "sampleProperty.png")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let status = contentArray[indexPath.row]["status"] as? Int {
            if  status == 1 {
                
                if let propertyModel2 = (contentArray[indexPath.row])["property"] as? [String:Any] {
                    if let type = propertyModel2["property_type"] as? Int {
                       // vc.typeOfProperty = typeof // 1-sell, rent - 2
                        if type == 2{
                            
                            
                            let vc = storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                            vc.propertyId = contentArray[indexPath.row]["property_id"] as! Int
                            vc.isFromMyRequest = true
                            if let movingIn = contentArray[indexPath.row]["moving_in"] as? String, let movingOut = contentArray[indexPath.row]["moving_out"] as? String {
                                if let inDate = movingIn.components(separatedBy: " ").first, let outDate = movingOut.components(separatedBy: " ").first {
                                    vc.moving_inMyrequest = inDate
                                    vc.durationMyRequest = outDate
                                    
                                    let inDateForCompare = inDate.convertToADate(inputFormat: "yyyy-MM-dd")
                                    
                                    // Check if the property requested moving_in Date is older than current, if so cannot make confirm deal
                                    
                                    if inDateForCompare.compare(Date()) == .orderedAscending {
                                        // moving in date is older
                                        showBanner(message: "Date Expired.Please Request again")
                                        return
                                    }
                                    
                                    if let propertyModel = (contentArray[indexPath.row])["property"] as? [String:Any] {
                                        if let typeof = propertyModel["property_type"] as? Int {
                                            vc.typeOfProperty = typeof // 1-sell, rent - 2
                                            print("typeof",typeof)
                                        }else {
                                            vc.typeOfProperty = 1 // sell
                                        }
                                    }
                                    
                                    print("vc.moving_inMyrequest",inDate)
                                    print("vc.durationMyRequest",outDate)
                                    
                                }
                                
                            }
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                            
                        }
                        
                        
                      //  print("typeof",type)
                    
                    else {
                       // vc.typeOfProperty = 1 // sell
                        
                        let vc = storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                        vc.propertyId = contentArray[indexPath.row]["property_id"] as! Int
                        vc.isFromMyRequest = true
                        if let movingIn = contentArray[indexPath.row]["moving_in"] as? String, let movingOut = contentArray[indexPath.row]["moving_out"] as? String {
                            if let inDate = movingIn.components(separatedBy: " ").first, let outDate = movingOut.components(separatedBy: " ").first {
                                vc.moving_inMyrequest = inDate
                                vc.durationMyRequest = outDate
                                
                              //  let inDateForCompare = inDate.convertToADate(inputFormat: "yyyy-MM-dd")
                                
                                // Check if the property requested moving_in Date is older than current, if so cannot make confirm deal
                                
//                                if inDateForCompare.compare(Date()) == .orderedAscending {
//                                    // moving in date is older
//                                    showBanner(message: "Date Expired.Please Request again")
//                                    return
//                                }
//
//                                if let propertyModel = (contentArray[indexPath.row])["property"] as? [String:Any] {
//                                    if let typeof = propertyModel["property_type"] as? Int {
//                                        vc.typeOfProperty = typeof // 1-sell, rent - 2
//                                        print("typeof",typeof)
//                                    }else {
//                                        vc.typeOfProperty = 1 // sell
//                                    }
//                                }
                                
                            //    print("vc.moving_inMyrequest",inDate)
                            //    print("vc.durationMyRequest",outDate)
                                
                            }
                            
                        }
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                         
                    }
                }
            }
                
                
                

//                let vc = storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
//                vc.propertyId = contentArray[indexPath.row]["property_id"] as! Int
//                vc.isFromMyRequest = true
//                if let movingIn = contentArray[indexPath.row]["moving_in"] as? String, let movingOut = contentArray[indexPath.row]["moving_out"] as? String {
//                    if let inDate = movingIn.components(separatedBy: " ").first, let outDate = movingOut.components(separatedBy: " ").first {
//                        vc.moving_inMyrequest = inDate
//                        vc.durationMyRequest = outDate
//
//                        let inDateForCompare = inDate.convertToADate(inputFormat: "yyyy-MM-dd")
//
//                        // Check if the property requested moving_in Date is older than current, if so cannot make confirm deal
//
//                        if inDateForCompare.compare(Date()) == .orderedAscending {
//                            // moving in date is older
//                            showBanner(message: "Date Expired.Please Request again")
//                            return
//                        }
//
//
//
//
//                        if let propertyModel = (contentArray[indexPath.row])["property"] as? [String:Any] {
//                            if let typeof = propertyModel["property_type"] as? Int {
//                                vc.typeOfProperty = typeof // 1-sell, rent - 2
//                                print("typeof",typeof)
//                            }else {
//                                vc.typeOfProperty = 1 // sell
//                            }
//                        }
//
//                        print("vc.moving_inMyrequest",inDate)
//                        print("vc.durationMyRequest",outDate)
//
//                    }
//
//                }
//
//                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        }
    }

}













