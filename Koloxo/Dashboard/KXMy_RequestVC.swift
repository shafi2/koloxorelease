//
//  KXMy_RequestVC.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 02/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//


import UIKit
import SideMenu

class KXMy_RequestVC: UIViewController {
    
    @IBOutlet var propertyTV: UITableView!
    
    fileprivate var propertySource: [KXRequestModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDataWeb()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_menu(_ sender: Any) {
        showSideMenu()
//        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        // vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

// Handling table view data source
extension KXMy_RequestVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return propertySource.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "KXMyRequestTableViewCell") as? KXMyRequestTableViewCell else { return UITableViewCell() }
        
        let source = propertySource[indexPath.row]
        //        let imageURL = URL(string: baseImageURL2 + (source.property?.property_images[0].image)!)
        //
        //        cell.propertyImage.kf.setImage(with: imageURL!, placeholder: #imageLiteral(resourceName: "placeholderproperty"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.propertyTitleLBL.text = source.property?.property_name
        cell.propertyLocationLBL.text = source.property?.city
        cell.propertyPriceLBL.text = source.property?.price.description
        cell.propertyStatusLBL.text = source.status.rawValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}

// Handling table view delegate
extension KXMy_RequestVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboardChat!.instantiateViewController(withIdentifier: "KXChatScreenViewController") as! KXChatScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

// Handling web service
extension KXMy_RequestVC {
    
    fileprivate func getDataWeb() {
        let url = "propertyRequestsReceived"
        var params = Json()
        params.updateValue(13, forKey: "user_id")
        Webservices2.postMethod(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? JsonArray, !data.isEmpty else { return }
            print(data)
            let values = KXRequestModel.getValue(from: data)
            self.propertySource = values
            BaseThread.asyncMain {
                self.propertyTV.reloadData()
            }
        }
    }
}




