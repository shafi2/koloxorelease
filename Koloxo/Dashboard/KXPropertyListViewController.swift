//
//  KXPropertyListViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 09/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
//import SideMenu

class KXPropertyListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
 @IBOutlet var Propertyrequest_tab: UITableView!
    var requestedUsers:[RequestedUsers] = []
    var passingPropertyID = 0
    var passingRequestID = 0
    var requesterID = 0
    var isFromPushNotification:Bool = false
    
    final var moving_inMyrequest: String = ""
    final var durationMyRequest: String = ""
    final var typeOfProperty: Int = 1
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!
    
    fileprivate var segueCounter:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpWebCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        segueCounter = 0
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    @IBAction func viewProfileTapped(_ sender: StandardButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXProfileEditVC") as! KXProfileEditVC
        vc.passingUserID = "\(requestedUsers[sender.tag].id)"
        vc.profileReviewType = .tenant
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func setUpWebCall(){
        //RequestedUsers?property_id=226
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        Webservices.getMethodWith(url: "RequestedUsers", parameter: ["property_id":"\(passingPropertyID)","tokenkey":tokenKey,"unix_time":unixTime]) { (isComplete, json) in
            let trueData = json["Data"] as? [[String:Any]]
            //let tempData = trueData!["users"]
            let decoder = JSONDecoder()
            do{
                let jsonData = try JSONSerialization.data(withJSONObject: trueData as Any, options: .prettyPrinted)
                let parsedData = try decoder.decode([RequestedUsers].self, from: jsonData)
              //  self.requestedUsers = parsedData
                self.upateDataSourceForMultipleRerquest(requestedUsers: parsedData)

                //print("Requested Users",self.requestedUsers)
                //Old - let filteredArray = self.requestedUsers.filter({$0.propertyRequest[0].status == 0})
                let filteredArray = self.requestedUsers
               // print("Filtered Array",filteredArray)
                self.requestedUsers = filteredArray
//                for (index,item) in self.requestedUsers.enumerated(){
//                    if item.propertyRequest[0].status == 0{
//                        self.requestedUsers.remove(at: index)
//                    }
//                }
                
                self.Propertyrequest_tab.reloadData()
            }catch{
                print(error)
            }
        }
    }
    
    func upateDataSourceForMultipleRerquest(requestedUsers:[RequestedUsers]){
        
        self.requestedUsers.removeAll()
        for user  in requestedUsers {
            for reqest in user.propertyRequest{
                var newUser =  user
                newUser.propertyRequest = [reqest]
                self.requestedUsers.append(newUser)
            }
        }
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch requestedUsers[indexPath.row].propertyRequest[0].status{
        case 1: //Accepted
            let cell = tableView.dequeueReusableCell(withIdentifier: "KXRequestPropertyStatusCell") as!KXRequestPropertyStatusCell
            cell.viewProfileBTNRef.tag = indexPath.row
            
            let hiddenNumber = requestedUsers[indexPath.row].mobileNumber//.prefix(2) + "xxxxxxx"
            cell.contactNumber.text = "\(hiddenNumber)"
            cell.name.text = "\(requestedUsers[indexPath.row].name)"
            cell.profileImage.kf.setImage(with: URL(string:"\(baseImageURL)\(requestedUsers[indexPath.row].image)"), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"),
                                          options: [.requestModifier(getImageAuthorization())],
                                          progressBlock: nil,
                                          completionHandler: nil)
            cell.statusLBL.text = "Accepted"
            cell.statusLBL.textColor = UIColor.acceptGreen
            return cell
        case 2: //Rejected
            let cell = tableView.dequeueReusableCell(withIdentifier: "KXRequestPropertyStatusCell") as! KXRequestPropertyStatusCell
            cell.viewProfileBTNRef.tag = indexPath.row
            
            let hiddenNumber = requestedUsers[indexPath.row].mobileNumber//.prefix(2) + "xxxxxxx"
            cell.contactNumber.text = "\(hiddenNumber)"
            cell.name.text = "\(requestedUsers[indexPath.row].name)"
            cell.profileImage.kf.setImage(with: URL(string:"\(baseImageURL)\(requestedUsers[indexPath.row].image)"), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"),
                                          options: [.requestModifier(getImageAuthorization())],
                                          progressBlock: nil,
                                          completionHandler: nil)
            cell.statusLBL.text = "Rejected"
            cell.statusLBL.textColor = UIColor.rejectRed
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "KXRequestPropertyTableCell") as!KXRequestPropertyTableCell
            cell.viewProfileBTNRef.tag = indexPath.row
            
            cell.acceptBTNRef.tag  = indexPath.row
            cell.ignoreBTNRef.tag  = indexPath.row
            let hiddenNumber = requestedUsers[indexPath.row].mobileNumber//.prefix(2) + "xxxxxxx"
            cell.contactNumber.text = "\(hiddenNumber)"
            cell.name.text = "\(requestedUsers[indexPath.row].name)"
            print(requestedUsers[indexPath.row])
            self.passingRequestID = requestedUsers[indexPath.row].propertyRequest[0].id
            cell.profileImage.kf.setImage(with: URL(string:"\(baseImageURL)\(requestedUsers[indexPath.row].image)"), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"),
                                          options: [.requestModifier(getImageAuthorization())],
                                          progressBlock: nil,
                                          completionHandler: nil)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
  
    /*
     propertyID:59
     userID:193
     requestID:120
     channel:”Koloxo_193_59_120"
     accept:1
 */
    
    
    @IBAction func acceptContact(_ sender: StandardButton) {
        self.requesterID = self.requestedUsers[sender.tag].id
        self.passingRequestID  = self.requestedUsers[sender.tag].propertyRequest[0].id
        self.sendAcceptData(propID: passingPropertyID, reqID: self.passingRequestID)
    }
    
    @IBAction func ignoreContact(_ sender: StandardButton) {
        self.requesterID = self.requestedUsers[sender.tag].id
        self.passingRequestID  = self.requestedUsers[sender.tag].propertyRequest[0].id
        self.sendDeclineData(propID: passingPropertyID, reqID: self.passingRequestID)
    }
    
    
    @IBAction func Accept_Button(_ sender: Any) {
//        let unixTime = SecureSocket.getTimeStamp()
//        let tokenKey = SecureSocket.getToken(with: unixTime)
//        let passingUserID = Credentials.shared.userId
//        let passingPropertyID = ""
//        let passingRequestID = ""
//        var param:[String:Any] = ["propertyID":"\(passingPropertyID)","userID":"\(passingUserID)","requestID":"\(passingRequestID)","channel":"Koloxo_\(passingUserID)_\(passingPropertyID)_\(passingRequestID)","accept":"1","tokenkey":tokenKey,"unix_time":unixTime]
//        Webservices.postMethod(url: "acceptChat", parameter: param) { (isComplete, json) in
//            print(json)
//            if isComplete{
        
        
        /*
                let vc = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "KXChatDetailVC") as! KXChatDetailVC
                self.navigationController?.pushViewController(vc, animated: true)
        
        */
        
        
//            }
//        }
        
        
        print(passingPropertyID)
        print(self.passingRequestID)
       
        self.sendAcceptData(propID: passingPropertyID, reqID: self.passingRequestID)
        
    }
    
    @IBAction func Ignore_Button(_ sender: Any) {
//        let unixTime = SecureSocket.getTimeStamp()
//        let tokenKey = SecureSocket.getToken(with: unixTime)
//        let passingUserID = Credentials.shared.userId
//        let passingPropertyID = ""
//        let passingRequestID = ""
//        var param:[String:Any] = ["propertyID":"\(passingPropertyID)","userID":"\(passingUserID)","requestID":"\(passingRequestID)","channel":"Koloxo_\(passingUserID)_\(passingPropertyID)_\(passingRequestID)","accept":"0","tokenkey":tokenKey,"unix_time":unixTime]
//        Webservices.postMethod(url: "acceptChat", parameter: param) { (isComplete, json) in
//            print(json)
//            if isComplete{
//                //Get Call
//                //Table Reload
//            }
//        }
//        let vc = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "KXDashBoardScreenViewController") as! KXDashBoardScreenViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
       // self.passingRequestID  = self.requestedUsers[sender.tag].propertyRequest[0].id

        self.sendDeclineData(propID: passingPropertyID, reqID: self.passingRequestID)
    }
    

    
    @IBAction func Back_Button(_ sender: Any) {
        if self.isFromPushNotification {
            let vc = self.storyboardMain?.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func Humberger_Menu(_ sender: Any) {
        showSideMenu()
    }
    
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }

    
    
    
    
    @IBAction func Tab_Home(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func Tab_Button(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     
     
     "id": 13,
     "first_name": "sara",
     "last_name": "k",
     "image": "images/user_image/download.jpeg",
     "mobile_number": "1234567822",
     "email": "sara@gmail.com",
     "password": "$2y$10$VQqFEtZPoPPF/Q72PvjITebJNS2pK/amzkaVHB02GMX//Q2QhcUHm",
     "country_residence": 1,
     "status": 1,
     "date": null,
     "type": 2,
     "facebook_id": null,
     "hash": "$2y$10$zRuvsA8HvnGhQJ2Rw7tO8e16Orx7gSYXs/dbXyyhQOQ/DhJdln0WW",
     "reputed_profile": "1",
     "facebook_login": null,
     "created_at": "2018-05-17 08:24:27",
     "updated_at": "2018-05-17 08:55:20",
     "deleted_at": null,
     "property_request": []
 */
    
    /*
     "id":171,
     "name":"antony",
     "image":"/storage/app/user_images/409887user_image.jpg",
     "mobile_number":"974775555",
     "email":"antony@in.appzoc.com",
     "country_residence":1,
     "country_detail":{
             "id":1,
             "name":"Afghanistan",
             "country_code":"AF",
             "flag_image":"http://koloxo-homes.dev.webcastle.in/flags/AF-32.png",
             "flag_image_128":"http://koloxo-homes.dev.webcastle.in/flags/AF-128.png",
             "telephone_code":"+93",
             "status":1,
             "created_at":"2018-05-29 11:45:39",
             "updated_at":"2018-05-29 12:00:17",
             "deleted_at":null
        },
     "property_request":[
             {
             "id":13,
             "property_id":226,
             "user_id":171,
             "status":0,
             "request_amount":null,
             "offer_amount":null,
             "created_at":"2018-06-18 09:02:38",
             "updated_at":"2018-06-18 09:02:38",
             "deleted_at":null
             }
        ]

    */
    
    struct RequestedUsers: Codable{
        let id:Int
        let name:String
        let image:String
        let mobileNumber:String
        var propertyRequest:[PropertyRequest]
        
        private enum CodingKeys: String, CodingKey{
            case id, name, image, mobileNumber = "mobile_number", propertyRequest = "property_request"
        }
        
        struct PropertyRequest: Codable {
            let propertyID:Int
            let status:Int
            let id:Int
            
            private enum CodingKeys:  String, CodingKey{
                case propertyID = "property_id",status,id = "id"
            }
        }
    }
    
}


extension KXPropertyListViewController
{
    
    func sendAcceptData(propID:Int,reqID:Int)
    {
        Webservices2.getMethod(url: "acceptContactRequest?property_id=\(propID)&request_id=\(reqID)") { (Succeeded, response) in
            guard Succeeded else { return }
            
            self.addChatRoom(propertyID: propID, requestID: reqID)
            let Message = response["Message"] as? String ?? "Error occured while fetching data"
            showBanner(message: Message, style: .success)
            self.setUpWebCall()
            // self.setUpWebCall()
        }
        
        
    }
    
    
    func sendDeclineData(propID:Int,reqID:Int)
    {
        Webservices2.getMethod(url: "declineContactRequest?property_id=\(propID)&request_id=\(reqID)") { (Succeeded, response) in
            guard Succeeded else { return }
            
            let Message = response["Message"] as? String ?? "Error occured while fetching data"
            showBanner(message: Message)
            
            self.setUpWebCall()
        }
        
        
    }
    
    func addChatRoom(propertyID:Int, requestID:Int){
        let param = ["owner_id":Credentials.shared.userId,"property_id":"\(propertyID)","request_id":"\(requestID)","channel":"Koloxo_\(Credentials.shared.userId)_\(propertyID)_\(requestID)"]
        Webservices.postMethod(url: "addChatRoom", parameter: param) { (isComplete, json) in
            if isComplete{
                print(json)
                let data = json["Data"] as? [String:Any]
                let channelID = data!["id"] as! Int
                let addingUserID = self.requesterID
                
                self.addUserToChatRoom(requester: Int(Credentials.shared.userId)!, chatID: channelID)
                self.addUserToChatRoom(requester: addingUserID, chatID: channelID)
                
            }
        }
    }
    
    func addUserToChatRoom(requester:Int, chatID:Int){
        Webservices.getMethod(url: "addUserToRoom?customer_id=\(requester)&chat_room_id=\(chatID)") { (isComplete, json) in
            print(json)
            //self.setUpWebCall()
            self.segueCounter += 1
            self.gotoChat()
        }
    }
    
    func gotoChat(){
        if segueCounter == 2{
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .chat
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
