//
//  KXRequestPropertyTableCell.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 09/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXRequestPropertyTableCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: BaseImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var contactNumber: UILabel!
    
    @IBOutlet weak var viewProfileBTNRef: StandardButton!
    
    @IBOutlet weak var acceptBTNRef: StandardButton!
    
    @IBOutlet weak var ignoreBTNRef: StandardButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


class KXRequestPropertyStatusCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: BaseImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var contactNumber: UILabel!
    
    @IBOutlet weak var viewProfileBTNRef: StandardButton!
    
    @IBOutlet var statusLBL: UILabel!
    
    
}
