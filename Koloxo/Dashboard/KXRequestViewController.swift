//
//  KXRequestViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 17/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import Kingfisher

class KXRequestViewController: UIViewController {
    
    @IBOutlet var propertyTV: UITableView!
    
    fileprivate var propertySource: [KXRequestModel] = []
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataWeb()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_menu(_ sender: Any) {
        showSideMenu()
//        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        return
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    
}

// Handling Tableview
extension KXRequestViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return propertySource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "KXRequestTableViewCell") as? KXRequestTableViewCell else { return UITableViewCell() }
        let source = propertySource[indexPath.row]
        //       let imageURL = URL(string: baseImageURL2 + (source.property?.property_images[0].image)!)
        //       cell.propertyImage.kf.setImage(with: imageURL!, placeholder: #imageLiteral(resourceName: "placeholderproperty"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.propertyTitleLBL.text = source.property?.property_name
        cell.prpertyAdressLBl.text = source.property?.city
        cell.propertyPriceLBL.text = source.property?.price.description
        cell.propertyStatusLBL.text = source.request_count > 1 ? source.request_count.description + " Requests" : source.request_count.description + " Request"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150

    }
    
}

extension KXRequestViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboardDashboard?.instantiateViewController(withIdentifier: "KXPropertyListViewController") as! KXPropertyListViewController
        //vc.propertyId = propertySource[indexPath.row].property_id
        navigationController?.pushViewController(vc, animated: true)
    }
}


// Handling Web service
extension KXRequestViewController {
    
    fileprivate func getDataWeb() {
        let url = "propertyRequestsSent"
        var params = Json()
        params.updateValue(Credentials.shared.userId, forKey: "user_id")
        Webservices2.postMethod(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? JsonArray, !data.isEmpty else { return }
            print(data)
            let values = KXRequestModel.getValue(from: data)
            print("Hai\(values[0])")
            self.propertySource = values
            BaseThread.asyncMain {
                self.propertyTV.reloadData()
            }
        }
    }
    
    
}











