//
//  FLCategorySelectionViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 18/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit


protocol CategorySelectionDelegate: class {
    func selectedValue(index:IndexPath)
}



class FLCategorySelectionViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var listingTable: UITableView!
    
    @IBOutlet var pageTitleLBL: UILabel!
    
    
    // Data receiving properties
    final var tableSource:[String] = []
    final weak var delegate:CategorySelectionDelegate?
    final var selectedIndexPath: IndexPath?
    var filteredSource:[String] = []
    
    var titleForPage:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //searchTextField.delegate = self
        filteredSource = tableSource
        listingTable.reloadData()
        setUpInterface()
    }
    
    func setTitle(title:String){
        self.titleForPage = title
    }
    
    
    func setUpInterface(){
        self.pageTitleLBL.text = titleForPage
        searchView.layer.borderWidth = 1
        searchView.layer.borderColor = UIColor(red: 56/255, green: 56/255, blue: 56/255, alpha: 0.4).cgColor
        searchView.layer.cornerRadius = 15
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func searchAction(_ sender: Any) {
        if let _ = searchTextField.text{
            var temp:[String] = []
            for item in tableSource{
                if item.contains("\(searchTextField.text!)"){
                    temp.append(item)
                }
            }
            filteredSource = temp
            listingTable.reloadData()
        }
    }
    
    
    
    
    @IBAction func clearAction(_ sender: Any) {
    }
    
    @IBAction func applyFilterAction(_ sender: Any) {
    }
    
    
    @IBAction func valueChanged(_ sender: UITextField) {
        if let _ = sender.text{
            if sender.text == ""{
                filteredSource = tableSource
            }else{
                var temp:[String] = []
                for item in tableSource{
                    if item.lowercased().contains("\(searchTextField.text!.lowercased())"){
                        temp.append(item)
                    }
                }
                filteredSource = temp
            }
            
            listingTable.reloadData()
        }
    }
    
    
}

//MARK:- Table Delegate and DataSource
extension FLCategorySelectionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FLFilterTableViewCell", for: indexPath) as? FLFilterTableViewCell else { return UITableViewCell() }
        
        if selectedIndexPath != nil {
            if selectedIndexPath == indexPath {
                cell.listingLabel.text = filteredSource[indexPath.row]
                cell.isSelected = true
            }else{
                cell.listingLabel.text = filteredSource[indexPath.row]
                cell.isSelected = false
            }
        }else{
            cell.listingLabel.text = filteredSource[indexPath.row]
            cell.isSelected = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _ = tableView.cellForRow(at: indexPath) as! FLFilterTableViewCell
        if let delegateVC = self.delegate{
            let cell = tableView.cellForRow(at: indexPath) as! FLFilterTableViewCell
            for (index,item) in tableSource.enumerated(){
                if cell.listingLabel.text! == item{
                    let indexPassing = IndexPath(row: index, section: 0)
                     delegateVC.selectedValue(index: indexPassing)
                }
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 38
    }
}


//
//
//extension FLCategorySelectionViewController: UITableViewDataSource, UITableViewDelegate{
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        switch sourceType {
//        case .country:
//            return countrySource.count
//        case .currency:
//            return currencySource.count
//        case .propertyType:
//            return propertySource.count
//
//        case .buildingAge:
//            return ageBuildingSource.count
//        case .elevators:
//            return elevatorSource.count
//        case .floorNumber:
//            return buildingFloorSource.count
//        case.buyRentType:
//            return buyRentType.count
//
//        case .number:
//            return countrySource.count
//        case .monthNumber:
//            return countrySource.count
//
//        default:
//            return countrySource.count
//
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0
//
//        UIView.animate(withDuration: 0.25) {
//            cell.alpha = 1
//
//        }
//
//
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "FLFilterTableViewCell", for: indexPath) as! FLFilterTableViewCell
//        if selectedIndexPath == indexPath {
//            cell.isSelected = true
//        }else{
//            cell.isSelected = false
//        }
//
//        switch sourceType {
//        case .country:
//            cell.listingLabel.text = countrySource[indexPath.row].country
//
//        case .currency:
//           cell.listingLabel.text = currencySource[indexPath.row].currency
//        case .propertyType:
//            cell.listingLabel.text = propertySource[indexPath.row].type
//
//        case .number:
//            cell.listingLabel.text = countrySource[indexPath.row].country
//        case .buildingAge:
//         cell.listingLabel.text = ageBuildingSource[indexPath.row].age
//        case .elevators:
//           cell.listingLabel.text = elevatorSource[indexPath.row].elevator
//        case .floorNumber:
//          cell.listingLabel.text = buildingFloorSource[indexPath.row].floor_no
//        case .buyRentType:
//           cell.listingLabel.text = buyRentType[indexPath.row].Type
//
//        case .monthNumber:
//           cell.listingLabel.text = countrySource[indexPath.row].country
//        default:
//           cell.listingLabel.text = countrySource[indexPath.row].country
//
//        }
//        return cell
//    }
//
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        switch sourceType {
//        case .country:
//             delegate?.selectedCategory(value: countrySource[indexPath.row], indexPath: indexPath)
//        case .currency:
//            delegate?.selectedCategory(value: currencySource[indexPath.row] as AnyObject, indexPath: indexPath)
//        case .propertyType:
//            delegate?.selectedCategory(value: propertySource[indexPath.row] as AnyObject, indexPath: indexPath)
//        case .buildingAge:
//            delegate?.selectedCategory(value: ageBuildingSource[indexPath.row] as AnyObject, indexPath: indexPath)
//        case .elevators:
//            delegate?.selectedCategory(value: elevatorSource[indexPath.row] as AnyObject, indexPath: indexPath)
//        case .floorNumber:
//            delegate?.selectedCategory(value: buildingFloorSource[indexPath.row] as AnyObject, indexPath: indexPath)
//        case .buyRentType:
//            delegate?.selectedCategory(value: buyRentType[indexPath.row] as AnyObject, indexPath: indexPath)
//        default:
//             delegate?.selectedCategory(value: countrySource[indexPath.row], indexPath: indexPath)
//        }
//
//        self.dismiss(animated: true, completion: nil)
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60
//
//    }
//
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        //let cell = tableView.cellForRow(at: indexPath) as! FLFilterTableViewCell
//
//        if tableView.tag == 0{
////            if self.categorySelectedIndexPaths.contains(indexPath){
////               // cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
////                self.categorySelectedIndexPaths.remove(at: self.categorySelectedIndexPaths.index(of: indexPath)!)
////            }
//        }else{
////            if self.subCategorySelectedIndexPaths.contains(indexPath){
////               // cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
////                self.subCategorySelectedIndexPaths.remove(at: self.subCategorySelectedIndexPaths.index(of: indexPath)!)
////
////            }
//        }
//
//    }
//
//
//
//}

