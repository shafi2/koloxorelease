//
//  HeaderTableViewCell.swift
//  collapasbleTbale
//
//  Created by admin on 28/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var contentOfCell: UIView!
    @IBOutlet weak var firstview: UIView!
    @IBOutlet weak var secondView: UIView!
   @IBOutlet weak var firstBtn: RoundedButton!
    @IBOutlet weak var sectionsView: UIView!
    @IBOutlet weak var mainClipView: UIView!
    
    @IBOutlet weak var secondBtn: RoundedButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     //mainClipView.roundCorners(corners: [.topLeft, .topRight], radius: 10, borderWidth: nil, borderColor: nil)
        //sectionsView.roundCorners(corners: [.topLeft, .topRight], radius: 10, borderWidth: nil, borderColor: nil)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
       
    }
}




extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat, borderWidth: CGFloat?, borderColor: UIColor?) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        
        self.layer.mask = maskLayer
        self.layer.masksToBounds = true
        
        if (borderWidth != nil && borderColor != nil) {
            
            // remove previously added border layer
            for layer in layer.sublayers! {
                if layer.name == "borderLayer" {
                    layer.removeFromSuperlayer()
                }
            }
            
            let borderLayer = CAShapeLayer()
            
            borderLayer.frame = self.bounds;
            borderLayer.path  = maskPath.cgPath;
            borderLayer.lineWidth = borderWidth ?? 0;
            borderLayer.strokeColor = borderColor?.cgColor;
            borderLayer.fillColor   = UIColor.clear.cgColor;
            borderLayer.name = "borderLayer"
            
            self.layer.addSublayer(borderLayer);
            
        }
        
    }
}



class RoundedButton1: UIButton {
    
    var corners: UIRectCorner?
    var radius = CGFloat(0.0)
    var borderWidth = CGFloat(0.0)
    var borderColor: UIColor?
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        // don't apply mask if corners is not set, or if radius is Zero
        guard let _corners = corners, radius > 0.0 else {
            return
        }
        
        roundCorners(corners: _corners, radius: radius, borderWidth: borderWidth, borderColor: borderColor)
        
    }
    
}

