//
//  KXHistoryExtendedTVC.swift
//  Koloxo
//
//  Created by Appzoc on 19/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit
import NotificationBannerSwift

class KXHistoryExtendedTVC: UITableViewCell{
    
    weak var controller:UIViewController?
    
    @IBOutlet var propertyNameLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var usdPriceLBL: UILabel!
    @IBOutlet var movingInDateLBL: UILabel!
    @IBOutlet var movingOutDateLBL: UILabel!
    @IBOutlet var propertyImageView: UIImageView!
    @IBOutlet var currencyTypeLBL: UILabel!
    @IBOutlet weak var cancelRequestBTN: UIButton!
    @IBOutlet var movingDatesView: UIView!
    @IBOutlet weak var propertyTypeView: UIView!
    @IBOutlet weak var propertyTypeLBL: UILabel!

    
    override func awakeFromNib() {
        propertyTypeView.transform = CGAffineTransform.identity
        propertyTypeView.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi * 0.25))
        
    }

    final var reviewQuestionsData:[ReviewQuestionModel] = []
    private var reviewPropertyId: Int = 0
    private var reviewUserId: Int = 0
    private var cellData: MyRentalModel?
    
    @IBAction func rateOwnerBTNTapped(_ sender: UIButton) {
        let vc = controller?.storyboardMain?.instantiateViewController(withIdentifier: "KXRateUserPopUpController") as! KXRateUserPopUpController
        vc.questionsSource = self.reviewQuestionsData.filter({$0.question_type == 1}) // rate owner 1
        vc.passingPropertyID = self.reviewPropertyId
        vc.toUserID = self.reviewUserId
        vc.ratingType = .owner
        controller?.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func ratePropertyBTNTapped(_ sender: UIButton) {
        let vc = controller?.storyboardMain?.instantiateViewController(withIdentifier: "KXRateUserPopUpController") as! KXRateUserPopUpController
        vc.questionsSource = self.reviewQuestionsData.filter({$0.question_type == 3}) // rate property 3
        vc.passingPropertyID = self.reviewPropertyId
        vc.toUserID = self.reviewUserId
        vc.ratingType = .property
        controller?.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func cancelRequestBTNTapped(_ sender: UIButton) {
        
        if let cancelDetails = cellData?.rental_details?.cancel_details {
            if cancelDetails.admin_status == 0 && cancelDetails.owner_status == 0 && cancelDetails.tenant_status == 0 {
                // not requested yet
                BaseAlert.completionAlert(withTitle: "Cancel Request?", message: "Do you wish to cancel the request?", OkTitle: "Yes", CancelTitle: "No", tintColor: .themeColor, andPresentOn: controller!, completion: { [weak self] in
                    guard let _ = self else { return }
                    self?.cancelRequestWeb()
                })
            }else if cancelDetails.admin_status == 0 && cancelDetails.tenant_status == 1 {
                // tenant requested and waiting for admin approval
                showBanner(title: "Cancel Request", message: "Already placed cancel request", style: .danger)
            }else if cancelDetails.admin_status == 2 && cancelDetails.tenant_status == 1 {
                // tenant requested and admin rejected
                showBanner(title: "Cancel Request", message: "Rejected your cancel request", style: .danger)
            }else {
                // adminstatus = 1 and tenent status = 1
                print("This will not occure - else case")
                
            }
        }else {
            // not requested yet
            BaseAlert.completionAlert(withTitle: "Cancel Request?", message: "Do you wish to cancel the request?", OkTitle: "Yes", CancelTitle: "No", tintColor: .themeColor, andPresentOn: controller!, completion: { [weak self] in
                guard let _ = self else { return }
                self?.cancelRequestWeb()
            })

        }
//        let alertVC = UIAlertController(title: "Cancel Request?", message: "Do you wish to cancel the request?", preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
//
//        }
//        let cancelAction = UIAlertAction(title: "No", style: .default) { (action) in
//            alertVC.dismiss(animated: true, completion: nil)
//        }
//        alertVC.addAction(okAction)
//        alertVC.addAction(cancelAction)
//        self.controller?.present(alertVC, animated: true, completion: nil)
    }
    
    
    func configureCellWith(data: MyRentalModel, controller:UIViewController){
        self.cellData = data
        self.controller = controller
        self.reviewPropertyId = data.propertyID
        self.reviewUserId = data.property.user_id
        self.propertyNameLBL.text = data.property.propertyName
        self.locationLBL.text = data.property.address
        self.usdPriceLBL.text = data.property.price.showPriceInDollar()//showPriceInKilo()
       
        if data.property.propertyType == 1 {
            self.propertyTypeLBL.text = "SELL"
            self.cancelRequestBTN.isHidden = true
        }else {
            
          self.propertyTypeLBL.text = "RENT"
          self.cancelRequestBTN.isHidden = false
        }
        // to check move in date with current date
//        let date = Date()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd/MM/yyyy"//"yyyy/MM/dd"
//        let Date1 = dateFormatter.string(from:date)
//        let mDate = data.movingIn
//        let inDate = mDate?.components(separatedBy: " ").first
//        print(inDate)
//
//        if inDate!.compare(Date1) == .orderedAscending {
//        self.cancelRequestBTN.setTitle("Property Expired", for: [])
//        }
        
        
        if let cancelDetails = data.rental_details?.cancel_details {
            if cancelDetails.admin_status == 0 && cancelDetails.owner_status == 0 && cancelDetails.tenant_status == 0 {
               // not requested yet
                self.cancelRequestBTN.setTitle("Cancel Request", for: [])
                self.cancelRequestBTN.isUserInteractionEnabled = true

            }else if cancelDetails.admin_status == 0 && cancelDetails.tenant_status == 1 {
                // tenant requested and waiting for admin approval
                self.cancelRequestBTN.setTitle("Cancel Requested", for: [])
                self.cancelRequestBTN.isUserInteractionEnabled = true
            }else if cancelDetails.admin_status == 2 && cancelDetails.tenant_status == 1 {
                // tenant requested and admin rejected
                self.cancelRequestBTN.setTitle("Request Rejected", for: [])
                self.cancelRequestBTN.isUserInteractionEnabled = true
            
            }else {
                // adminstatus = 1 and tenent status = 1
                print("This will not occure - else case")
                self.cancelRequestBTN.setTitle("Request Approved", for: [])
                self.cancelRequestBTN.isUserInteractionEnabled = false

            }
        }else {
            // not requested yet
            self.cancelRequestBTN.setTitle("Cancel Request", for: [])
            self.cancelRequestBTN.isUserInteractionEnabled = true

        }
        
        if let image = data.property.images.filter({$0.typeID == 6}).first{
            self.propertyImageView.kf.setImage(with: URL(string: baseImageURL + image.image)!, placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }else if let imageAlter = data.property.images.map({$0.image}).first{
            self.propertyImageView.kf.setImage(with: URL(string: baseImageURL + imageAlter)!, placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }
        
        if let inDate = data.rental_details?.movingIn, let outDate = data.rental_details?.movingOut, data.property.propertyType == 2{
            
            self.movingInDateLBL.text = Date.formatDate(date: inDate) //"12/09/2018"
            self.movingOutDateLBL.text = Date.formatDate(date: outDate) //"2/04/2018"
            
            DispatchQueue.main.async {
                self.movingDatesView.isHidden = false
            }
        }else{
            DispatchQueue.main.async {
                self.movingDatesView.isHidden = true
            }
        }
        
    }
    
    
    
    
    
    private func cancelRequestWeb() {
        guard let currentData = self.cellData else { return }
        var parameter = Json()
       // parameter["request_id"] = currentData.id
       // parameter["user_type"] = 2 //user_type =1 ------->owner, user_type=2 --------->tenant
        
        Webservices.getMethodWith(url: "cancelConfirmedDeal",parameter:["request_id":"\(currentData.id)","user_type":"2"]) { (isSucceeded, response) in
        
            
            print(isSucceeded, "Response ", response.description)
            guard isSucceeded else { return }
            guard let message = response["Message"] as? String else
          
            { return }
         
            showBanner(message: message, style: .success)
            
           // print(message)
            BaseThread.asyncMain {
               
                if let vc = self.controller as? KXHistory {
                    vc.setUpWebcall()
                }else {
                 print("History relaod Errorrrrrr")
                    self.controller?.viewDidLoad()
                }
            }
           
        }
    }
    
    
}
