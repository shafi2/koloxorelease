//
//  ViewController.swift
//  KoloxoScreen
//
//  Created by My Mac on 26/04/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import UIKit
import SideMenu
class cell1: UITableViewCell, UITextViewDelegate, UITextFieldDelegate
{
    @IBOutlet var titleTF: UITextField!
    
    @IBOutlet weak var descriptionTF: UITextView!
    @IBOutlet var submitBTN: UIButton!
    override func awakeFromNib() {
        //descriptionTF.layer.borderColor = UIColor.lightGray.cgColor
        //descriptionTF.layer.borderWidth = 1
        descriptionTF.delegate = self
        titleTF.delegate = self
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = " "
    }
    
}

class cell2: UITableViewCell
{
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    
    @IBOutlet weak var deleteBTNRef: StandardButton!
    
    @IBOutlet weak var editBTNRef: StandardButton!
    
}

class header: UITableViewCell
{
    @IBOutlet var headerLBL: UILabel!
    @IBOutlet var headerBTN: UIButton!
    @IBOutlet var headerView: StandardView!
    @IBOutlet var headerArrow: UIImageView!
    
}

class nextBTNCell: UITableViewCell{
    
    @IBOutlet weak var nextBTNRef: RoundedButton!
}

struct RoomDescription{
    let title:String
    let description:String
}


class KNRoomDescriptionController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var contentTV: UITableView!
    
    var furnitureSource = Array<[RoomDescription]>()
    var furnitureSectionCount:[Int] = []
    
    var BedroomArray:[RoomDescription] = []
    var KitchenArray:[RoomDescription] = []
    var GuestroomArray:[RoomDescription] = []
    var BalconyArray:[RoomDescription] = []
    var OtherFurnituresArray:[RoomDescription] = []
    
    var bedroomCount = 0
    var kitchenCount = 0
    var guestRoomCount = 0
    var balconyCount = 0
    var otherCount = 0
    
    var editingMode:Bool = false
    var passingPropertyID:Int = 0
    var furnitureData:[KXFurnitureData] = []
    
    var shouldSkipBuildingAddingStep: Bool = false

    //    var contentArray = [String]()
    var contentArray:[RoomDescription] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       // editingMode = true
        setUpWebCall()
    }
    
    
    
    @IBAction func headerBTNPressed(_ sender: UIButton)
    {
        for (index,item) in furnitureSectionCount.enumerated(){
            if item != 0{
                addDescription(senderTag: index)
            }
        }
        
        furnitureSectionCount.removeAll()
        for _ in furnitureSource{
            furnitureSectionCount.append(0)
        }
        
        furnitureSectionCount[sender.tag] = furnitureSource[sender.tag].count + 1
        
//        switch sender.tag
//        {
//        case 0:
//            bedroomCount == 0 ? (bedroomCount = 1+BedroomArray.count) : (bedroomCount = 0)
//            kitchenCount = 0
//            guestRoomCount = 0
//            balconyCount = 0
//            otherCount = 0
//            contentArray = BedroomArray
//        case 1:
//            bedroomCount = 0
//            kitchenCount == 0 ? (kitchenCount = 1+KitchenArray.count) : (kitchenCount = 0)
//            guestRoomCount = 0
//            balconyCount = 0
//            otherCount = 0
//            contentArray = KitchenArray
//        case 2:
//            bedroomCount = 0
//            kitchenCount = 0
//            guestRoomCount == 0 ? (guestRoomCount = 1+GuestroomArray.count) : (guestRoomCount = 0)
//            balconyCount = 0
//            otherCount = 0
//            contentArray = GuestroomArray
//        case 3:
//            bedroomCount = 0
//            kitchenCount = 0
//            guestRoomCount = 0
//            balconyCount == 0 ? (balconyCount = 1+BalconyArray.count) : (balconyCount = 0)
//            otherCount = 0
//            contentArray = BalconyArray
//        case 4:
//            bedroomCount = 0
//            kitchenCount = 0
//            guestRoomCount = 0
//            balconyCount = 0
//            otherCount == 0 ? (otherCount = 1+OtherFurnituresArray.count) : (otherCount = 0)
//            contentArray = OtherFurnituresArray
//        default:
//            print("DefaultCase")
//        }
        
        contentTV.reloadData()
    }
    
    func addDescription(senderTag:Int){
        if let cell = contentTV.cellForRow(at: IndexPath(row: 0, section: senderTag)) as? cell1{
            if /*cell.titleTF.text != "Add Title" &&*/ cell.descriptionTF.text != "Add Description"{
                
                let description = RoomDescription(title:" " /*"\(cell.titleTF.text!)"*/, description: "\(cell.descriptionTF.text!)")
                furnitureSource[senderTag].append(description)
                furnitureSectionCount[senderTag] = furnitureSource[senderTag].count + 1
            }
        }
    }
    
    @IBAction func addBTNPressed(_ sender: UIButton)
    {
//        let cell = contentTV.cellForRow(at: IndexPath(row: 0, section: sender.tag)) as! cell1
//        if cell.titleTF.text != "Add Title" && cell.descriptionTF.text != "Add Description"{
//
//        let description = RoomDescription(title: "\(cell.titleTF.text!)", description: "\(cell.descriptionTF.text!)")
//        furnitureSource[sender.tag].append(description)
//        furnitureSectionCount[sender.tag] = furnitureSource[sender.tag].count + 1
        
        //BedroomArray.append(brDesc)
        //bedroomCount = 1+BedroomArray.count
        //contentArray = BedroomArray
        
//        switch sender.tag
//        {
//        case 0:
//            let brDesc = RoomDescription(title: "\(cell.titleTF.text!)", description: "\(cell.descriptionTF.text!)")
//            BedroomArray.append(brDesc)
//            bedroomCount = 1+BedroomArray.count
//            contentArray = BedroomArray
//        case 1:
//            let kitchenDesc = RoomDescription(title: "\(cell.titleTF.text!)", description: "\(cell.descriptionTF.text!)")
//            KitchenArray.append(kitchenDesc)
//            kitchenCount = 1+KitchenArray.count
//            contentArray = KitchenArray
//        case 2:
//            let guestroomDesc = RoomDescription(title: "\(cell.titleTF.text!)", description: "\(cell.descriptionTF.text!)")
//            GuestroomArray.append(guestroomDesc)
//            guestRoomCount = 1+GuestroomArray.count
//            contentArray = GuestroomArray
//        case 3:
//            let balconyDesc = RoomDescription(title: "\(cell.titleTF.text!)", description: "\(cell.descriptionTF.text!)")
//            BalconyArray.append(balconyDesc)
//            balconyCount = 1+BalconyArray.count
//            contentArray = BalconyArray
//        case 4:
//            let otherFurnitureCount = RoomDescription(title: "\(cell.titleTF.text!)", description: "\(cell.descriptionTF.text!)")
//            OtherFurnituresArray.append(otherFurnitureCount)
//            otherCount = 1+OtherFurnituresArray.count
//            contentArray = OtherFurnituresArray
//        default:
//            print("Default")
//        }
        addDescription(senderTag: sender.tag)
        contentTV.reloadData()
    }
    
    
    @IBAction func deleteRoomDescription(_ sender: StandardButton) {
        //let cell = contentTV.cellForRow(at: sender.indexPath!)
        contentTV.beginUpdates()
        contentTV.deleteRows(at: [sender.indexPath!], with: .fade)
        furnitureSource[sender.indexPath!.section].remove(at: sender.indexPath!.row - 1)
        furnitureSectionCount[sender.indexPath!.section] = furnitureSource[sender.indexPath!.section].count + 1
//        switch sender.indexPath!.section {
//        case 0:
//            BedroomArray.remove(at: sender.indexPath!.row - 1)
//            bedroomCount = BedroomArray.count + 1
//            contentArray = BedroomArray
//        case 1:
//            KitchenArray.remove(at: sender.indexPath!.row - 1)
//            kitchenCount = KitchenArray.count + 1
//            contentArray = KitchenArray
//        case 2:
//            GuestroomArray.remove(at: sender.indexPath!.row - 1)
//            guestRoomCount = GuestroomArray.count + 1
//            contentArray = GuestroomArray
//        case 3:
//            BalconyArray.remove(at: sender.indexPath!.row - 1)
//            balconyCount = BalconyArray.count + 1
//            contentArray = BalconyArray
//        case 4:
//            OtherFurnituresArray.remove(at: sender.indexPath!.row - 1)
//            otherCount = OtherFurnituresArray.count + 1
//            contentArray = OtherFurnituresArray
//        default:
//            print("Default")
//        }
        self.contentTV.reloadData()
        contentTV.endUpdates()
    }
    
    
    @IBAction func editRoomDescription(_ sender: StandardButton) {
        
        contentTV.beginUpdates()
        _ = ""
        var editingDescription = ""
        //editingTitle =  furnitureSource[sender.indexPath!.section][sender.indexPath!.row - 1].title //contentArray[sender.indexPath!.row - 1].title
        editingDescription = furnitureSource[sender.indexPath!.section][sender.indexPath!.row - 1].description //contentArray[sender.indexPath!.row - 1].description
        contentTV.deleteRows(at: [sender.indexPath!], with: .fade)
        
        furnitureSource[sender.indexPath!.section].remove(at: sender.indexPath!.row - 1)
        furnitureSectionCount[sender.indexPath!.section] = furnitureSource[sender.indexPath!.section].count + 1
        
//        switch sender.indexPath!.section {
//        case 0:
//            BedroomArray.remove(at: sender.indexPath!.row - 1)
//            bedroomCount = BedroomArray.count + 1
//            contentArray = BedroomArray
//        case 1:
//            KitchenArray.remove(at: sender.indexPath!.row - 1)
//            kitchenCount = KitchenArray.count + 1
//            contentArray = KitchenArray
//        case 2:
//            GuestroomArray.remove(at: sender.indexPath!.row - 1)
//            guestRoomCount = GuestroomArray.count + 1
//            contentArray = GuestroomArray
//        case 3:
//            BalconyArray.remove(at: sender.indexPath!.row - 1)
//            balconyCount = BalconyArray.count + 1
//            contentArray = BalconyArray
//        case 4:
//            OtherFurnituresArray.remove(at: sender.indexPath!.row - 1)
//            otherCount = OtherFurnituresArray.count + 1
//            contentArray = OtherFurnituresArray
//        default:
//            print("Default")
//        }
        self.contentTV.reloadData()
        contentTV.endUpdates()
        let editCell = contentTV.cellForRow(at: IndexPath(row:0, section: sender.indexPath!.section)) as! cell1
        DispatchQueue.main.async {
            //editCell.titleTF.text = editingTitle
            editCell.descriptionTF.text = editingDescription
        }
        
    }
    
    
    @IBAction func closeController(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sliderTapped(_ sender: UIButton) {
        showSideMenu()
//        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)

    }
    
    func setTableToDefaultState(){
        furnitureSectionCount.removeAll()
        for _ in furnitureSource{
            furnitureSectionCount.append(0)
        }
        contentTV.reloadData()
    }
    
    func clearCellTextFields(senderTag:Int){
        if let cell = contentTV.cellForRow(at: IndexPath(row: 0, section: senderTag)) as? cell1{
            //cell.titleTF.text = "Add Title"
            cell.descriptionTF.text = "Add Description"
        }
    }
    
    //MARK:- Next BTN Tapped
    @IBAction func nextBTNTapped(_ sender: UIButton) {
        
        for (index,item) in furnitureSectionCount.enumerated(){
            if item != 0{
                addDescription(senderTag: index)
                clearCellTextFields(senderTag: index)
            }
        }
        
        guard validateData() else { return }
        setTableToDefaultState()
        postData { (isComplete) in
            print("Completed: \(isComplete)")
            if isComplete{
                let nextScreen = self.storyboardProperty!.instantiateViewController(withIdentifier: "KXAddplacemarker") as! KXAddplacemarker
                nextScreen.passingPropertyID = self.passingPropertyID
                nextScreen.editingMode = self.editingMode
                nextScreen.shouldSkipBuildingAddingStep = self.shouldSkipBuildingAddingStep

                self.navigationController?.pushViewController(nextScreen, animated: true)
            }else{
                print("Webcall has failed")
            }
        }
    }
    
    //[{"id":1,"furniture":"Bedroom"},{"id":2,"furniture":"kitchen"}]
    //MARK:- Webcall Functions
    func setUpWebCall(){
        
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
        let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime
        ]
//        let param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime
//        ]
        Webservices.getMethodWith(url: "getFurniturelists", parameter:param ) { (isComplete, json) in
            do{
                let tempData = json["Data"] as? [String:Any]
                let trueData = tempData!["furnitures"]
                let parsedData = try JSONSerialization.data(withJSONObject: trueData!, options: .prettyPrinted)
                let decoder = JSONDecoder()
                let furnitureDataReceived = try decoder.decode([KXFurnitureData].self, from: parsedData)
                self.furnitureData = furnitureDataReceived
                for _ in self.furnitureData{
                    self.furnitureSectionCount.append(0)
                    self.furnitureSource.append([RoomDescription]())
                }
                if self.editingMode{
                    self.editDataWebcall()
                }
                
                self.contentTV.reloadData()
                print(self.furnitureData)
            }catch{
                print(error)
            }
        }
    }
    
    func editDataWebcall(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
//        let param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime,"property_id":"132"]
        let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,"property_id":"\(passingPropertyID)"]
        
        Webservices.getMethodWith(url: "getPropertyFurnitureDetails", parameter: param) { (isComplete, json) in
            print(json)
            if isComplete{
                do{
                    let trueData = json["Data"]
                    let decoder = JSONDecoder()
                    let jsonData = try JSONSerialization.data(withJSONObject: trueData as Any, options: .prettyPrinted)
                    let parsedData = try decoder.decode([PropertyFurnitureDetails].self, from: jsonData)
                    self.populateWithData(data: parsedData)
                    print(parsedData)
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func populateWithData(data: [PropertyFurnitureDetails]){
        for (_,item) in data.enumerated(){
            for (subIndex,object) in furnitureData.enumerated(){
                if object.id == item.id{
                    for element in item.furniture{
                        furnitureSource[subIndex].append(RoomDescription(title: element.title, description: element.description))
                    }
                }
            }
        }
        contentTV.reloadData()
        
    }
    
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    //MARK:- Validation
    fileprivate func validateData() -> Bool {
        var isValid: Bool = false
        var missingField = ""
        
        for (index,item) in furnitureSource.enumerated() {
            if !item.isEmpty {
                isValid = true
            }else{
                //showBanner(message: "\(furnitureData[index].furniture) field is mandatory")
                missingField = furnitureData[index].furniture
                isValid = false
                break
            }
        }
        if isValid {
            return isValid
        }else{
            showBanner(message: "\(missingField) field is mandatory")
            //showBanner(message: "All fields are mandatory")
            return isValid
        }
    }

    
    func postData(completion: @escaping(Bool)->()){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
//         var param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime,"property_id":"132"]
        
        var param:[String:Any] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,
                                  "property_id":"\(passingPropertyID)"
        ]
        
        if editingMode{
            param.updateValue("1", forKey: "edit")
        }
        var sendData:[KXPropertySendData] = []
        for (index,_) in furnitureSource.enumerated(){
            for row in furnitureSource[index]{
                let a = KXPropertySendData(furniture_id: furnitureData[index].id, data: row)
                sendData.append(a)
            }
        }
        print("Send Data",sendData)
        /*
        [{"furniture_id":15,"data":{"title":"adfs","description":"bedromdescription"}},{"furniture_id":15,"data":{"title":"adfs","description":"bedromdescription"}}]
        */
        var furData:[[String:Any]] = []
       // let furData = [["furniture_id":1,"data":["title":"qwerty","description":"bedroomdesc"]],["furniture_id":2,"data":["title":"qwerty","description":"bedroomdesc"]]]
        for item in sendData{
            let a = ["furniture_id":item.furniture_id,"data":["title":" "/*"\(item.data.title)"*/,"description":"\(item.data.description)"]] as [String : Any]
            furData.append(a)
        }
        do{
            let dataSend = try JSONSerialization.data(withJSONObject: furData, options: .prettyPrinted)
            let jsonString = String(data: dataSend, encoding: .utf8)
            param.updateValue(jsonString!, forKey: "furniture_data")
        }catch{
            print("Failed Serialisation")
            print(error)
        }
        
        Webservices.postMethod(url: "postFurnitureData", parameter: param) { (isComplete, json) in
            if isComplete{
                print(json)
                completion(true)
            }else{
                print("Webcall failed")
                completion(false)
            }
        }
    }
    
    //MARK:- Table View Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return furnitureData.count + 1
    }
    
    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section >= furnitureData.count{
            return 1
        }else{
            return furnitureSectionCount[section]
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section >= furnitureData.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "nextBTNCell") as! nextBTNCell
            return cell
        } else if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! cell1
            //cell.titleTF.text = "Add Title"
            cell.descriptionTF.text = "Add Description"
            //cell.descriptionTF.layer.borderColor = UIColor.lightGray.cgColor
            cell.submitBTN.tag = indexPath.section
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! cell2
           // cell.titleLBL.text = furnitureSource[indexPath.section][indexPath.row-1].title //contentArray[indexPath.row-1].title
            cell.descriptionLBL.text = furnitureSource[indexPath.section][indexPath.row-1].description  //contentArray[indexPath.row-1].description
            cell.deleteBTNRef.indexPath = indexPath
            cell.editBTNRef.indexPath = indexPath
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section >= furnitureData.count{
            return 15
        }else{ return 55 }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! header
        
        if furnitureSource.count > section {
            if tableView.numberOfRows(inSection: section) == 0 {
                cell.headerView.cornerRadius = 4
                cell.headerArrow.image = UIImage(named: "angle-arrow-down")
            }else{
                cell.headerView.setSpecificRoundCorners([.topLeft,.topRight], radius: 4)
                cell.headerArrow.image = UIImage(named: "up-arrow")
            }
        }
        
        cell.headerBTN.tag = section
        if section >= furnitureData.count{
            return UIView()
        }else{
            cell.headerLBL.text = furnitureData[section].furniture
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "footer")
        return cell
    }
    
    
    
}

