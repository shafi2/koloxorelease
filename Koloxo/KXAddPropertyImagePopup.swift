//
//  KXAddPropertyImagePopup.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 02/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import OpalImagePicker

protocol KXAddPropertyImageDelegate: class {
    func selectedBuilding(name: String?, image: UIImage?)
}

class KXAddPropertyImagePopup: UIViewController, OpalImagePickerControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet var buildingNameTF: UITextField!
    
    final var delegate: KXAddPropertyImageDelegate?

    fileprivate var buildingTitle: String = ""
    fileprivate var buildingImage: UIImage?
    
    @IBOutlet weak var imageUploadedStatus: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func closePopup(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Submit_button(_ sender: Any) {
        
        guard validateData() else { return }
        delegate?.selectedBuilding(name: buildingNameTF.text, image: buildingImage)
        self.dismiss(animated: true, completion: nil)
    }

    fileprivate func validateData() -> Bool{
        if BaseValidator.isNotEmpty(string: buildingNameTF.text) && buildingImage != nil {
            return true
        }else {
            
            if !BaseValidator.isNotEmpty(string: buildingNameTF.text) {
                showBanner(message: "Please enter building name")
                return false
            }
            
            if buildingImage == nil {
                showBanner(message: "Please choose building image")
                return false
            }
            
            return false
        }
    }
    
    @IBAction func browseTapped(_ sender: UIButton) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 1
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        if !images.isEmpty {
            buildingImage = images[0]
        }
        DispatchQueue.main.async {
            self.imageUploadedStatus.text = "Image-1"
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    

}
