//

//  KXAddRoomImagesController.swift

//  Koloxo

//

//  Created by Appzoc on 28/05/18.

//  Copyright © 2018 Appzoc-Macmini. All rights reserved.

//



import UIKit

import OpalImagePicker

import Photos

import SideMenu

import Kingfisher



class KXPostPropertyBTNCell:UITableViewCell{
    
    @IBOutlet var postPropertyBTNRef: RoundedButton!
    
    
    
}



class KXAddRoomImagesController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableRef: UITableView!
    
    fileprivate var imageData:[Array<UIImage>] = []
    
    fileprivate var attributeData:[KXPropertyImageAttributes] = []
    
    fileprivate var selectedSection: Int?
    
    var passingPropertyID:Int = 0
    var editingMode:Bool = false
    
    
    var imageIdData:[Array<Int>] = []
    
    var downloadedData:[UIImage] = []
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //editingMode = true
        
        setUpWebCall()
        
        // Do any additional setup after loading the view.
        
    }
    
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
        
        
    }
    
    
    
    @IBAction func sliderTapped(_ sender: UIButton) {
        
        
        
        showSideMenu()
        
    }
    
    
    
    
    
    @IBAction func addImageToSection(_ sender: StandardButton) {
        
        let imagePicker = OpalImagePickerController()
        
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        
        presentOpalImagePickerController(imagePicker, animated: true,
                                         
                                         select: { (assets) in
                                            
                                            for item in assets{
                                                
                                                let OPImage = self.getAssetThumbnail(asset: item)
                                                
                                                print(OPImage,"image")
                                                
                                                self.imageData[sender.indexPath!.section].append(OPImage)
                                                
                                                self.imageIdData[sender.indexPath!.section].append(0)
                                                
                                            }
                                            
                                            self.dismiss(animated: true, completion: {
                                                
                                                self.tableRef.reloadData()
                                                
                                            })
                                            
        }, cancel: {
            
            //self.dismiss(animated: true, completion: nil)
            
        })
        
        
        
    }
    
    
    
    
    
    @IBAction func removeImageFromSection(_ sender: StandardButton) {
        
        imageData[sender.indexPath!.section].remove(at: sender.indexPath!.row)
        
        tableRef.reloadData()
        
        removeImageBackend(index: sender.indexPath!)
        
    }
    
    
    
    func removeImageBackend(index:IndexPath){
        
        let id = self.imageIdData[index.section][index.row]
        
        print("ImageID: ",id)
        
        Webservices.postMethodWithoutAlert(url: "deleteImage/\(id)", parameter: ["slug":"propertyImage","user_id":"\(Credentials.shared.userId)"]) { (isCompleted, json) in
            self.imageIdData[index.section].remove(at: index.row)
            print(json)
            
        }
        
        
        
    }
    
    
    
    //MARK:- Validation
    
    func validateData() -> Bool{
        
        for (index,item) in imageData.enumerated(){
            
            if attributeData[index].id == 6,item.count > 0{
                
                return true
                
            }
            
        }
        
        return false
        
    }
    
    
    
    //MARK:- Post Property Tapped Final
    
    @IBAction func postPropertyTapped(_ sender: RoundedButton) {
        
        guard validateData() else{
            showBanner(message: "Add atleast one Exterior Property Image")
            return
        }
        
        postData { (isComplete) in
                if isComplete{
                
                print("Completed")
                
                Webservices.postMethod(url: "Postproperty", parameter: ["user_id":"\(Credentials.shared.userId)","propertyid":"\(self.passingPropertyID)"], CompletionHandler: { (isComplete, json) in
                    print(json)
                    if isComplete{
                        BaseThread.asyncMain {
                            
                            if self.editingMode {
                                print("editingModeeeeeee")
                                showBanner(title: "", message: "Property Updated Successfully", style: .success)

                            }else {
                                print("Adddddddddddddd-EditMode False")
                                showBanner(title: "", message: "Property Added Successfully", style: .success)

                            }
                            
                            
                            //showBanner(message: "Property Added Successfully")
                            //                            let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
                            
                            let vc = self.storyboardDashboard?.instantiateViewController(withIdentifier: "KXMyPropertiesVC") as! KXMyPropertiesVC
                            vc.isFromAddProperty = true
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else {
                        
                    }
                    
                })
                
            }else{
                
                print("Final Failed")
                
            }
            
        }
        
        
        
    }
    
    
    @IBAction func sectionBTNTapped(_ sender: UIButton) {
        
        print(sender.tag)
        
        selectedSection = sender.tag
        
        tableRef.reloadData()
        
    }
    
    
    
    
    
    
    
    //MARK:- Table Delegate Methods
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int
        
    {
        
        return imageData.count + 1
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        
    {
        
        print(section,"khdhf",imageData.count)
        
        if section >= imageData.count{
            
            print("Button Reached")
            
            return 1
            
        }else if section == selectedSection {
            
            return 1
            
        }else{
            
            return 0
            
        }
        
        
        
        //        if section >= imageData.count{
        
        //            return 1
        
        //        }else{
        
        //            return imageData[section].count
        
        //        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        
    {
        
        if indexPath.section >= imageData.count{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "KXPostPropertyBTNCell") as! KXPostPropertyBTNCell
            
            cell.postPropertyBTNRef.index = indexPath
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "KXAddPropertyImagesCell") as! KXAddPropertyImagesCell
            
            cell.imageData = imageData.count > indexPath.section ? imageData[indexPath.section] : nil
            
            cell.addImagesBTNRef.indexPath = indexPath
            
            cell.collectionRef.reloadData()
            
            return cell
            
        }
        
        //        if indexPath.section == 6{
        
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "nextBTNCell") as! nextBTNCell
        
        //            cell.nextBTNRef.setTitle("Post Property", for: [])
        
        //            return cell
        
        //        }else{
        
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "KXAddPropertyImagesCell") as! KXAddPropertyImagesCell
        
        //            cell.imageData = imageData.count > indexPath.section ? imageData[indexPath.section] : nil
        
        //            cell.addImagesBTNRef.indexPath = indexPath
        
        //            return cell
        
        //        }
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
        
    {
        
        if section < attributeData.count{
            
            return 55
            
        }else{
            
            return 10
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
        
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! header
        
        cell.headerBTN.tag = section
        
        if section < attributeData.count{
            
            if tableView.numberOfRows(inSection: section) == 0 {
                cell.headerArrow.image = UIImage(named: "angle-arrow-down")
            }else{
                cell.headerArrow.image = UIImage(named: "up-arrow")
            }

            
            cell.headerLBL.text = attributeData[section].name
            
            return cell
            
        }else{
            
            let a = UIView()
            
            //a.backgroundColor = .wh
            
            return  a
            
        }
        
        //        switch section
        
        //        {
        
        //        case 0:
        
        //            cell.headerLBL.text = "Balconies"
        
        //        case 1:
        
        //            cell.headerLBL.text = "Bedrooms"
        
        //        case 2:
        
        //            cell.headerLBL.text = "Bathroom"
        
        //        case 3:
        
        //            cell.headerLBL.text = "Gardens"
        
        //        case 4:
        
        //            cell.headerLBL.text = "Other Photos"
        
        //        case 5:
        
        //            cell.headerLBL.text = "Upload Exterior Image"
        
        //        case 6:
        
        //            cell.headerLBL.text = ""
        
        //        default:
        
        //            print("Out of swith")
        
        //        }
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "footer")
        
        return cell
        
    }
    
    
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        
        let manager = PHImageManager.default()
        
        let option = PHImageRequestOptions()
        
        var thumbnail = UIImage()
        
        option.isSynchronous = true
        
        manager.requestImage(for: asset, targetSize: CGSize(width: 400, height: 400), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            
            thumbnail = result!
            
        })
        
        return thumbnail
        
    }
    
    
    
    //MARK:- SetUpWebCall
    
    func setUpWebCall(){
        
        let unixTime = SecureSocket.getTimeStamp()
        
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
        // "\(Credentials.shared.userId)"
        
        let param:[String:String] = ["user_id": "\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime
            
        ]
        
        
        
        Webservices.getMethodWith(url: "getPropertyImageAttributes", parameter:param ) { (isComplete, json) in
            
            do{
                
                let tempData = json["Data"] as? [String:Any] ?? [:]
                
                let trueData = tempData["propertyimageAttributes"] ?? [:]
                
                let parsedData = try JSONSerialization.data(withJSONObject: trueData, options: .prettyPrinted)
                
                let decoder = JSONDecoder()
                
                let imageDataReceived = try decoder.decode([KXPropertyImageAttributes].self, from: parsedData)
                
                for _ in imageDataReceived{
                    
                    self.imageData.append(Array<UIImage>())
                    
                    self.imageIdData.append(Array<Int>())
                    
                }
                
                print("Number of sections: ",imageDataReceived.count)
                
                self.attributeData = imageDataReceived
                
                self.tableRef.reloadData()
                
                //                self.furnitureData = furnitureDataReceived
                
                //                for item in self.furnitureData{
                
                //                    self.furnitureSectionCount.append(0)
                
                //                    self.furnitureSource.append([RoomDescription]())
                
                //                }
                
                //
                
                //                self.contentTV.reloadData()
                
                //                print(self.furnitureData)
                
            }catch{
                
                print(error)
                
            }
            
            if self.editingMode{
                
                self.editWebCall()
                
            }
            
        }
        
    }
    
    
    
    
    
    func editWebCall(){
        
        let unixTime = SecureSocket.getTimeStamp()
        
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
        //        let param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"132"]
        
        let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"\(passingPropertyID)"]
        
        
        
        Webservices.getMethodWith(url: "getAllPropertyImages", parameter: param) { (isComplete, json) in
            
            print(json)
            
            if isComplete{
                
                do{
                    
                    let trueData = json["Data"] as? [String:Any]
                    
                    let decoder = JSONDecoder()
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: trueData as Any, options: .prettyPrinted)
                    
                    let parsedData = try decoder.decode(PropertyImagesModel.self, from: jsonData)
                    
                    self.populateWithData(data: parsedData)
                    
                    print(parsedData)
                    
                }catch{
                    
                    print(error)
                    
                }
                
            }
            
        }
        
    }
    
    
    
    func populateWithData(data: PropertyImagesModel){
        
        for (index,item) in attributeData.enumerated(){
            
            for object in data.propertyImages{
                
                if object.type_id == item.id{
                    //http://koloxo-homes.dev.webcastle.in
                    if let url = URL(string: "\(baseImageURL)\(object.image)"){
                        
                        print(url)
                        
                        let indicatorA = ActivityIndicator()
                        
                        indicatorA.start()
                        
                        let imageView = UIImageView()
                        
                        ImageCache.default.retrieveImage(forKey: url.absoluteString, options: nil) {
                            image, cacheType in
                            if let _ = image {
                                print("Get image \(url.absoluteString), cacheType: \(cacheType).")
                                //In this code snippet, the `cacheType` is .disk
                            } else {
                                print("Not exist in cache.")
                            }
                        }
                        
                        imageView.kf.setImage(with: url, options: [.requestModifier(getImageAuthorization())]) { (image, error, type, url) in
                            
                            if error == nil && image != nil {
                                
                                if let appendingImage = imageView.image{
                                    
                                    self.imageData[index].append(appendingImage)
                                    
                                    self.imageIdData[index].append(object.id)
                                    
                                    self.downloadedData.append(appendingImage)
                                    
                                }
                                
                                DispatchQueue.main.async {
                                    
                                    self.tableRef.reloadData()
                                    
                                }
                                
                            } else {
                                
                                print(error as Any)
                                
                            }
                            
                            indicatorA.stop()
                            
                        }
                        
                        
                        
                        
                        
                    }
                    
                }
                
            }
            
        }
        
        print(imageData)
        
        
        
    }
    
    
    
    //MARK:- Post Data
    
    func postData(completion:@escaping(Bool)->()){
        
        let unixTime = SecureSocket.getTimeStamp()
        
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
        
        
        var param:[String:Any] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,
                                  
                                  "property_id":"\(passingPropertyID)"
            
        ]
        //        var param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime,"property_id":"132"]
        
        var dataParam:[String:Data] = [:]
        
        var indexCount = 0
        for (index,section) in imageData.enumerated(){
            for (_,item) in section.enumerated(){
                if downloadedData.contains(item){
                    
                }else{
                    
//                    print("ImageOrginalsize",UIImageJPEGRepresentation(item, 1))
//                    print("ImageResized",UIImageJPEGRepresentation(item.scaleToSize(aSize: CGSize(width: 400, height: 400)).compressToBelow1MB(sizeInKB: 1000), 1))
                    
                    
                    dataParam.updateValue(UIImageJPEGRepresentation(item.scaleToSize(aSize: CGSize(width: 400, height: 400)).compressToBelow1MB(sizeInKB: 800), 1)!, forKey: "propertImage[\(indexCount)][1]")
                    param.updateValue("\(attributeData[index].id)", forKey: "propertImage[\(indexCount)][0]")
                    indexCount += 1
                }
            }
        }
        
        
//        print("DataParam",dataParam)
//        print("TitleParam",param)
        Webservices.postMethodMultiPartImage(url: "addPropertyImage",
                                             
                                             parameter:param,
                                             
                                             dataParameter:dataParam, timeOutSeconds: 360 ) { (isCompleted, json) in
                                                
                                                print(json)
                                                
                                                if isCompleted {
                                                    
                                                    completion(true)
                                                    
                                                }else{
                                                    
                                                    
                                                    
                                                    print("Call Failed")
                                                    
                                                    completion(false)
                                                    
                                                }
                                                
        }
        
    }
    
    
    
}

































/*
 
 
 
 
 
 
 
 //
 
 //  KXAddRoomImagesController.swift
 
 //  Koloxo
 
 //
 
 //  Created by Appzoc on 28/05/18.
 
 //  Copyright © 2018 Appzoc-Macmini. All rights reserved.
 
 //
 
 
 
 import UIKit
 
 import OpalImagePicker
 
 import Photos
 
 
 
 class KXAddRoomImagesController: UIViewController, UITableViewDelegate, UITableViewDataSource {
 
 
 
 
 
 @IBOutlet weak var tableRef: UITableView!
 
 fileprivate var imageData:[Array<UIImage>] = []
 
 
 
 override func viewDidLoad() {
 
 super.viewDidLoad()
 
 setUpWebCall()
 
 // Do any additional setup after loading the view.
 
 }
 
 
 
 
 
 
 
 
 
 @IBAction func addImageToSection(_ sender: StandardButton) {
 
 let imagePicker = OpalImagePickerController()
 
 imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
 
 presentOpalImagePickerController(imagePicker, animated: true,
 
 select: { (assets) in
 
 for item in assets{
 
 let OPImage = self.getAssetThumbnail(asset: item)
 
 self.imageData[sender.indexPath!.section].append(OPImage)
 
 }
 
 self.dismiss(animated: true, completion: {
 
 self.tableRef.reloadData()
 
 })
 
 }, cancel: {
 
 //self.dismiss(animated: true, completion: nil)
 
 })
 
 
 
 }
 
 
 
 
 
 @IBAction func removeImageFromSection(_ sender: UIButton) {
 
 
 
 }
 
 
 
 func numberOfSections(in tableView: UITableView) -> Int
 
 {
 
 return imageData.count + 1
 
 }
 
 
 
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
 
 {
 
 if section >= imageData.count{
 
 return 1
 
 }else{
 
 return imageData[section].count
 
 }
 
 
 
 //        switch section
 
 //        {
 
 ////        case 0:
 
 ////            return imageData[0].count
 
 ////        case 1:
 
 ////            return imageData[1].count
 
 ////        case 2:
 
 ////            return imageData[2].count
 
 ////        case 3:
 
 ////            return imageData[3].count
 
 ////        case 4:
 
 ////            return imageData[4].count
 
 ////        case 5:
 
 ////            return imageData[5].count
 
 ////        case 6:
 
 ////            return 1
 
 //        default:
 
 //            return 1
 
 //        }
 
 }
 
 
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
 
 {
 
 if indexPath.section == 6{
 
 let cell = tableView.dequeueReusableCell(withIdentifier: "nextBTNCell") as! nextBTNCell
 
 cell.nextBTNRef.setTitle("Post Property", for: [])
 
 return cell
 
 }else{
 
 let cell = tableView.dequeueReusableCell(withIdentifier: "KXAddPropertyImagesCell") as! KXAddPropertyImagesCell
 
 cell.imageData = imageData.count > indexPath.section ? imageData[indexPath.section] : nil
 
 cell.addImagesBTNRef.indexPath = indexPath
 
 return cell
 
 }
 
 }
 
 
 
 
 
 func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
 
 {
 
 return 55
 
 }
 
 
 
 
 
 func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
 
 {
 
 let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! header
 
 cell.headerBTN.tag = section
 
 
 
 switch section
 
 {
 
 case 0:
 
 cell.headerLBL.text = "Balconies"
 
 case 1:
 
 cell.headerLBL.text = "Bedrooms"
 
 case 2:
 
 cell.headerLBL.text = "Bathroom"
 
 case 3:
 
 cell.headerLBL.text = "Gardens"
 
 case 4:
 
 cell.headerLBL.text = "Other Photos"
 
 case 5:
 
 cell.headerLBL.text = "Upload Exterior Image"
 
 case 6:
 
 cell.headerLBL.text = ""
 
 default:
 
 print("Out of swith")
 
 }
 
 
 
 return cell
 
 }
 
 
 
 func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
 
 let cell = tableView.dequeueReusableCell(withIdentifier: "footer")
 
 return cell
 
 }
 
 
 
 func getAssetThumbnail(asset: PHAsset) -> UIImage {
 
 let manager = PHImageManager.default()
 
 let option = PHImageRequestOptions()
 
 var thumbnail = UIImage()
 
 option.isSynchronous = true
 
 manager.requestImage(for: asset, targetSize: CGSize(width: 400, height: 400), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
 
 thumbnail = result!
 
 })
 
 return thumbnail
 
 }
 
 
 
 func setUpWebCall(){
 
 let unixTime = SecureSocket.getTimeStamp()
 
 let tokenKey = SecureSocket.getToken(with: unixTime)
 
 let param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime]
 
 
 
 Webservices.getMethodWith(url: "getPropertyImageAttributes", parameter:param ) { (isComplete, json) in
 
 do{
 
 let tempData = json["Data"] as? [String:Any]
 
 let trueData = tempData!["propertyimageAttributes"]
 
 let parsedData = try JSONSerialization.data(withJSONObject: trueData!, options: .prettyPrinted)
 
 let decoder = JSONDecoder()
 
 let imageDataReceived = try decoder.decode([KXPropertyImageAttributes].self, from: parsedData)
 
 for item in imageDataReceived{
 
 self.imageData.append(Array<UIImage>())
 
 }
 
 
 
 //                self.furnitureData = furnitureDataReceived
 
 //                for item in self.furnitureData{
 
 //                    self.furnitureSectionCount.append(0)
 
 //                    self.furnitureSource.append([RoomDescription]())
 
 //                }
 
 //
 
 //                self.contentTV.reloadData()
 
 //                print(self.furnitureData)
 
 }catch{
 
 print(error)
 
 }
 
 }
 
 }
 
 
 
 func postData(){
 
 
 
 }
 
 
 
 }
 
 */
