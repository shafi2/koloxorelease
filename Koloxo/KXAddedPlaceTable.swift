//
//  KXAddedPlaceTable.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 07/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class KXPlaceMarkTVC: UITableViewCell {
    @IBOutlet var propertyIMG: UIImageView!
    @IBOutlet var proprtynameLBL: UILabel!
    @IBOutlet var propertyDescLBL: UILabel!
    @IBOutlet var placeLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}

class KXAddedPlaceTable: UIViewController, KXAddPropertyImageDelegate {

    @IBOutlet var Addedplace_tab: UITableView!
    
    final var params: Json?
    final var lat: Double = 0
    final var lon: Double = 0
    final var loca: String = ""
    
    fileprivate var buildingId: Int?
    fileprivate var dataSource: [KXPlaceMarkerModel] = []
    fileprivate var isSuccessfullyPosted: Bool = false
    
    var passingPropertyID:Int = 0
    var editingMode:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSourceData()
        
    }
    
    func selectedBuilding(name: String?, image: UIImage?) {
        postAddbuildingCreated(name: name!, image: image!)
    }
    
    
    @IBAction func Humberger_Menu(_ sender: Any) {
        showSideMenu()

        
    }
    
    @IBAction func Back_button(_ sender: Any) {
         navigationController?.popViewController(animated: true)
        
    }

    
    @IBAction func Next_Button(_ sender: Any) {
        
        if isSuccessfullyPosted {
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXAddRoomImagesController") as! KXAddRoomImagesController
            vc.passingPropertyID = self.passingPropertyID
            vc.editingMode = self.editingMode
            self.navigationController?.pushViewController(vc, animated: true)

        }else{
            showBanner(message: "Please enter or select a building")

        }
        
    }
   
    @IBAction func PopUP_Button(_ sender: Any) {

        let popvc = storyboardProperty!.instantiateViewController(withIdentifier: "KXAddPropertyImagePopup") as! KXAddPropertyImagePopup
        popvc.delegate = self
        navigationController?.present(popvc, animated: true, completion: nil)
    }
    
    
    
}

extension KXAddedPlaceTable: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        if dataSource.count > 0
        {
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records in our database."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.font = UIFont(name: "Montserrat-Regular", size: 15)
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
        }
        return numOfSections
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXPlaceMarkTVC") as! KXPlaceMarkTVC
        let source = dataSource[indexPath.row]
        cell.propertyIMG.kf.setImage(with: URL(string: baseImageURL2 + source.file_upload), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        cell.proprtynameLBL.text = source.name
        cell.propertyDescLBL.text = source.description
        cell.placeLBL.text = source.location
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let source = dataSource[indexPath.row]
        if source.id == -1 {
            // google data
            postAddBuilding(sourceIndex: indexPath)
        }else{
            // back end data
            buildingId = source.id
            postDataPlaceMarkWeb()
        }
    }

}


extension KXAddedPlaceTable {
    
    fileprivate func postAddbuildingCreated(name: String, image: UIImage) {
        let url = "addBuildingList"
        var parameter = Json()
        
        parameter.updateValue(name, forKey: "name")
        parameter.updateValue("\(self.lat)", forKey: "latitude")
        parameter.updateValue("\(self.lon)", forKey: "longitude")
        parameter.updateValue("\(self.loca)", forKey: "location")
        parameter.updateValue(name, forKey: "description")
        parameter.updateValue(2.description, forKey: "building_type") // this for excluding building in the the list if appartment is selected(2)

        let dataParam:[String:Data] = ["building":UIImageJPEGRepresentation(image, 0.8)!]
        
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_tocken")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        
        let unixTime = Date().millisecondsSince1970
        print("UnixTime",unixTime.description)
        let hashKeyString = "appzoc-koloxohomes-appwebcastle" + unixTime.description
        print(hashKeyString)
        let tockenKey = generateMD5(from: hashKeyString)
        
        parameter.updateValue(tockenKey, forKey: "tokenkey")
        parameter.updateValue("\(unixTime)", forKey: "unix_time")
        
        Webservices.postMethodMultiPartImage(url: url, parameter: parameter, dataParameter: dataParam) { (isSucceeded, response) in
            print(response)
            guard isSucceeded else { return }

            guard let data = response["Data"] as? Int else { return }
            self.buildingId = data
            self.postDataPlaceMarkWeb()
        }
        

    }
    
    fileprivate func postAddBuilding(sourceIndex: IndexPath){
        let url = "addBuildingList"
        var parameter = Json()
        
        let source = dataSource[sourceIndex.row]
        
        parameter.updateValue("\(source.name)", forKey: "name")
        parameter.updateValue("\(source.latitude)", forKey: "latitude")
        parameter.updateValue("\(source.longitude)", forKey: "longitude")
        parameter.updateValue("\(source.location)", forKey: "location")
        parameter.updateValue("\(source.description)", forKey: "description")
        var dataParam:[String:Data] = [:]
    
        DispatchQueue.global().async {
            let imageURL = source.file_upload
            let data = try? Data(contentsOf: URL(string:imageURL)!)
            DispatchQueue.main.async {
                let buildingImage = UIImage(data: data!)
                dataParam.updateValue(UIImageJPEGRepresentation(buildingImage!, 0.8)!, forKey: "building")
                //parameter.updateValue(buildingImage as Any, forKey: "building")
            }
        }
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_tocken")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        let unixTime = Date().millisecondsSince1970
        print("UnixTime",unixTime.description)
        let hashKeyString = "appzoc-koloxohomes-appwebcastle" + unixTime.description
        print(hashKeyString)
        let tockenKey = generateMD5(from: hashKeyString)
        parameter.updateValue(tockenKey, forKey: "tokenkey")
        parameter.updateValue("\(unixTime)", forKey: "unix_time")
        
        Webservices.postMethodMultiPartImage(url: url, parameter: parameter, dataParameter: dataParam) { (isSucceeded, response) in

            guard isSucceeded else { return }

            guard let data = response["Data"] as? Json, !data.isEmpty else { return }

            print("id olddddd",self.dataSource[sourceIndex.row].id)
            
            self.dataSource[sourceIndex.row].id = data["building_id"] as? Int ?? 0
            
            self.buildingId = self.dataSource[sourceIndex.row].id
            
            print("Idchangeddd",self.dataSource[sourceIndex.row].id)
            
            self.postDataPlaceMarkWeb()


        }

//        Webservices2.postMethod(url: url, parameter: parameter) { (isSucceeded, response) in
//
//            guard isSucceeded else { return }
//
//            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
//
//            print("id olddddd",self.dataSource[sourceIndex.row].id)
//            self.dataSource[sourceIndex.row].id = data["building_id"] as? Int ?? 0
//            self.buildingId = self.dataSource[sourceIndex.row].id
//            print("Idchangeddd",self.dataSource[sourceIndex.row].id)
//            self.postDataPlaceMarkWeb()
//
//
//        }

    }
    
    fileprivate func getSourceData(){
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            return
        }
        
        let activity = ActivityIndicator()
        activity.start()
        
        self.getGoogleBuildingList { (isSucceeded,results) in
            var modelJson = Json()
            for item in results {
                if let GGgeometry = item["geometry"] as? Json, let GGLocation = GGgeometry["location"] as? Json, let GGicon = item["icon"] as? String, let GGname = item["name"] as? String {
                    if let GGlat = GGLocation["lat"] as? Double, let GGlon = GGLocation["lon"] as? Double {
                        modelJson.updateValue(GGname, forKey: "name")
                        modelJson.updateValue(GGlat, forKey: "latitude")
                        modelJson.updateValue(GGlon, forKey: "longitude")
                        modelJson.updateValue("", forKey: "description")
                        modelJson.updateValue(GGicon, forKey: "file_upload")
                        modelJson.updateValue("", forKey: "location")
                        modelJson.updateValue(-1, forKey: "id") // for detecting data from google
                        self.dataSource.append(KXPlaceMarkerModel.getValue(fromObject: modelJson))
                        print("Fully parsed")
                        
                    }
                }
            } // closed loop
            BaseThread.asyncMain {
                activity.stop()
                self.getBuildingListWeb()
            }
        }
        
    }
    
    
    fileprivate func getGoogleBuildingList(completion: @escaping ((Bool, JsonArray)->())) {
        
        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=9.999695,76.296954&radius=100&type=premise&keyword=&key=AIzaSyA-9e56nVZ1buxzU6KXTfeO2zelNE6c5BE
        
        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=9.98756990986855,9.98756990986855&radius=100&type=premise&keyword=&key=AIzaSyDYSe0A9O1BBINQQ6kA5vVCgUf5GzRbmxA
        
        let url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
        let apiKey = "AIzaSyDYSe0A9O1BBINQQ6kA5vVCgUf5GzRbmxA"
        let radius = 50
        let callingURL = url + "?location=\(lat),\(lon)&radius=\(radius)&type=premise&keyword=&key=\(apiKey)"
        print("Calling URL: ",callingURL)
        let request = URLRequest(url: URL(string: callingURL)!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            if let trueData = data{
                print("Response \n:",data as Any)
                do {
                    if let jsonData = try! JSONSerialization.jsonObject(with: trueData, options: []) as? Json {
                        if let results = jsonData["results"] as? JsonArray {
                            completion(true, results)
                        }else{
                            completion(false, [])
                        }
                    }else{
                        completion(false, [])
                    }
                } catch {
                    print("GoogleBuildingApi",err as Any)
                    completion(false, [])
                }
            }
        }
        task.resume()

    }
    
    fileprivate func getBuildingListWeb(){
        let url = "getBuildingList"
        var parameter = Json()
        
        parameter.updateValue(lat, forKey: "latitude")
        parameter.updateValue(lon, forKey: "longitude")
        
        Webservices2.getMethodWith(url: url, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else {
                if !self.dataSource.isEmpty {
                    BaseThread.asyncMain {
                        self.Addedplace_tab.reloadData()
                    }
                }
                return
            }
            guard let dataArray = response["Data"] as? JsonArray, !dataArray.isEmpty else {
                if !self.dataSource.isEmpty {
                    BaseThread.asyncMain {
                        self.Addedplace_tab.reloadData()
                    }
                }
                return
            }
            
            BaseThread.asyncMain {
                
                let modelData = KXPlaceMarkerModel.getValue(fromArray: dataArray)
                for (index,item) in modelData.map({$0.name}).enumerated(){
                    if self.dataSource.map({$0.name}).contains(item){
                        
                    }else{
                        self.dataSource.append(modelData[index])
                    }
                }

                
                
//                self.dataSource.append(contentsOf: KXPlaceMarkerModel.getValue(fromArray: dataArray))
                
                self.Addedplace_tab.reloadData()
                self.editDataWebcall()

            }
        }
        
    }
    
    fileprivate func editDataWebcall(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
//        var param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"132"]
        var param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"\(passingPropertyID)"]
        
        if editingMode{
            param.updateValue("1", forKey: "edit")
        }
        Webservices.getMethodWith(url: "getPropertyDetail", parameter: param) { (isComplete, json) in
            print(json)
            if isComplete{
                do{
                    let trueData = json["Data"] as? [String:Any]
                    let tempData = trueData!["property"]
                    let decoder = JSONDecoder()
                    let jsonData = try JSONSerialization.data(withJSONObject: tempData as Any, options: .prettyPrinted)
                    let parsedData = try decoder.decode(PropertyPlaceModel.self, from: jsonData)
                    if let a = parsedData.building_id{
                        self.buildingId = a
                        for (index,item) in self.dataSource.enumerated(){
                            if item.id == self.buildingId!{
                                DispatchQueue.main.async {
                                    self.Addedplace_tab.selectRow(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .top)
                                    if self.editingMode {
                                        self.isSuccessfullyPosted = true
                                    }
                                }
                            }
                        }
                    }
                    print(parsedData)
                }catch{
                    print(error)
                }
            }
        }
    }

    
    fileprivate func postDataPlaceMarkWeb(){
        guard params != nil else { return }
        let url = "AddplaceMark"
        var parameter = params
        parameter?.updateValue(buildingId!, forKey: "building_id")
        parameter?.updateValue("\(passingPropertyID)", forKey: "property_id")
        parameter?.updateValue("\(Credentials.shared.userId)", forKey: "user_id")
        parameter?.updateValue("1", forKey: "edit")
        
        Webservices2.postMethod(url: url, parameter: parameter!) { (isSucceeded, response) in
            guard isSucceeded else { return }
            
            BaseThread.asyncMain {
                self.isSuccessfullyPosted = true
                //self.Addedplace_tab.reloadData()
                self.getBuildingListWeb()
                //showBanner(message: "Placemark added successfully")
            }
        }
        
        
    }
    
}







