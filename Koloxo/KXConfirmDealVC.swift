//
//  KXConfirmDealVC.swift
//  Koloxo
//
//  Created by Appzoc on 18/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit
import FloatRatingView
import DatePickerDialog

class KXConfirmDealVC: UIViewController, InterfaceSettable, UITextFieldDelegate {
    
    @IBOutlet weak var propertyImgView: StandardImageView!
    @IBOutlet weak var propertyNameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var currencyTypeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var availabilityStatusLbl: UILabel!
    @IBOutlet var availabilityIndicatorView: StandardView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var bedroomsCountLbl: UILabel!
    @IBOutlet weak var bathroomsCountLbl: UILabel!
    @IBOutlet weak var parkingStatusLbl: UILabel!
    @IBOutlet weak var offerAmountTF: UITextField!
    @IBOutlet weak var editOfferAmountBtn: UIButton!
    @IBOutlet var movingInDateLBL: UILabel!
    @IBOutlet var movingOutDateLBL: UILabel!
    @IBOutlet var termsImage: UIImageView!
    @IBOutlet var dateSelectionView: UIView!
    @IBOutlet var nonNegotiableLBL: UILabel!
    
    var propertyData: Json = Json()
    var propertyId: Int?
    var requestId: Int = 0
    var isRentProperty: Bool = true
    var isFromPropertyDetails: Bool = true
    
    fileprivate var isPriceNegotiable:Int = 1 // price negotiable -> 1, not negotiable - 0
    fileprivate var isReputedRequired:Int = 0 // the property needed reputed profile //// 0-> not reputed
    
    fileprivate var movingInDate: Date? = Date()
    fileprivate var movingOutDate: Date? = Date()
    fileprivate var movingOutDateOrginal: Date? = Date()

    fileprivate var isCheckedTerms: Bool = false
    fileprivate var resultRequestStatus: KXDetailsRequestStatusModel?
    fileprivate var isThreadRequestFinished: Bool = false
    fileprivate var isThreadDetailsFinished: Bool = false
    
    private var movingInDateSelected = Date()
    private lazy var negotiableText = "*Price negotiable"
    private lazy var notNegotiableText = "*Price not negotiable"
    private var propertyDataGet: Json? = nil
    private var currentReputedStatus: String = 0.description
    
//    private var movingInDate: Date = Date()
//    private var movingOutDate: Date = Date()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromPropertyDetails {
            //getPropertyStatus()
            getDataWeb()
        }else {
            
            self.navigationController?.popViewController(animated: true)
            return
            getDataWeb()
            getRequestStatus()
        }
        getReputedWeb()
    }
    
    func getReputedWeb() {
//        http://koloxo-homes.dev.webcastle.in/api/checkReputedUser?user_id=349
        
        var param = Json()
        param["user_id"] = Credentials.shared.userId
        Webservices.getMethodWith(url: "checkReputedUser", parameter: param) { (isCompleted, response) in
            if isCompleted {
                if let data = response["Data"] as? Json {
                    if let reputed = data["reputed_profile"] as? String {
                        self.currentReputedStatus = reputed
                    }else {
                        
                    }
                }else {
                    
                    
                    
                }
                
            }
            
            
        }
        
    }
    
    func setUpInterface() {
        let imageURL =  propertyData["image"] as! String
        movingInDateLBL.text = getDateAsString(date: movingInDate)
        movingOutDateLBL.text = getDateAsString(date: movingOutDateOrginal)
        propertyImgView.kf.setImage(with: URL(string: baseImageURL + imageURL), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        
        propertyNameLbl.text = (propertyData["name"] as! String)
        locationLbl.text = (propertyData["location"] as! String)
        if let price = propertyData["price"] as? Double{
            priceLbl.text = price.showPriceInDollar()
        }else{
            priceLbl.text = "\(propertyData["price"] ?? "0")"
        }
        
        if (propertyData["bedrooms"] as! String) == "1" {
            bedroomsCountLbl.text = (propertyData["bedrooms"] as! String) + "Bedroom"

        }else {
            bedroomsCountLbl.text = (propertyData["bedrooms"] as! String) + "Bedrooms"

        }
        if (propertyData["bathrooms"] as! String) == "1" {
            bathroomsCountLbl.text = (propertyData["bathrooms"] as! String) + "Bathroom"
        }else {
            bathroomsCountLbl.text = (propertyData["bathrooms"] as! String) + "Bathrooms"

        }

        
        
        
        //parkingStatusLbl.text = propertyData["parking"]
        availabilityStatusLbl.text = (propertyData["available"] as! String)
        if availabilityStatusLbl.text == "Available" {
            availabilityIndicatorView.backgroundColor = UIColor.availableGreen
            
        }else if availabilityStatusLbl.text == "Almost Available" {
            availabilityIndicatorView.backgroundColor = UIColor.almostAvailableYellow

        }else {
            availabilityIndicatorView.backgroundColor = UIColor.notAvailableRed

        }
        
        if let rating = propertyData["rating"] as? Double{
            ratingView.rating =  rating
        }
        if let ratingCount = propertyData["ratingCount"] as? String{
            ratingLbl.text = ratingCount
        }
        
        if isPriceNegotiable == 0 {
            let priceValue = "\(propertyData["price"] ?? "0")" 
            
            print("Price : ",priceValue)
            
            offerAmountTF.text = priceValue.replacingOccurrences(of: ",", with: "").trimmingCharacters(in: .whitespacesAndNewlines).doubleValue.showPriceInDollar() //StringManipulation.shared.revertFromK(word: priceValue)
            offerAmountTF.isUserInteractionEnabled = false
            nonNegotiableLBL.isHidden = false
            nonNegotiableLBL.text = notNegotiableText
        }else {
            //offerAmountTF.isUserInteractionEnabled = true
            offerAmountTF.isUserInteractionEnabled = true
            nonNegotiableLBL.isHidden = false
            nonNegotiableLBL.text = negotiableText

        }
        
        if self.isRentProperty {
            
            dateSelectionView.isHidden = false
        }else {
            dateSelectionView.isHidden = true
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text as NSString?
        let newText = text?.replacingCharacters(in: range, with: string).replacingOccurrences(of: ",", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        print("New Text :",newText as Any)
        if let _ = newText, let numericValue = Double(newText!){
            updateText(newText: newText!, numericValue: numericValue, textField: offerAmountTF)
        }

        return true
    }
    
    func updateText(newText:String,numericValue:Double,textField:UITextField){
        let currencyFormattedValue = numericValue.showPriceInDollar()
        print("Currency Formatted Value : ",currencyFormattedValue)
        //textField.text = nil
        DispatchQueue.main.async{
            textField.text = currencyFormattedValue
        }
    }

    
    func getPropertyStatus(){
        let url = "getDealData"
        var parameter = Json()
        parameter["user_id"] = Credentials.shared.userId
        parameter["propertyid"] = propertyId!

        Webservices2.getMethodWith(url: url, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let result = response["Data"] as? Json, !result.isEmpty else { return }
            
            if let negotiableStatus = result["pirce_negotiable"] as? Int{
                self.isPriceNegotiable = negotiableStatus
            }
            
            if let reputedStatus = result["reputed_profile"] as? Int{
                self.isReputedRequired = reputedStatus
            }
            
            self.setUpInterface()
        }
    }
    
    fileprivate func getDataWeb(){
        ///http://koloxo-homes.dev.webcastle.in/api/getDealData?propertyid=515
        let url = "getDealData"
        var parameter = Json()
        parameter["user_id"] = Credentials.shared.userId
        parameter["propertyid"] = propertyId!
        
        Webservices2.getMethodWith(url: url, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let propertyResult = response["Data"] as? Json, !propertyResult.isEmpty else { return }
            guard let result = propertyResult["property_data"] as? Json, !result.isEmpty else { return }
            
            if let imageArray = result["property_images"] as? JsonArray {
                self.propertyData["image"] = imageArray[0]["image"] as? String ?? ""
            }
            self.propertyData["name"] = result["property_name"] as? String ?? ""
            self.propertyData["location"] = result["city"] as? String ?? ""
            self.propertyData["price"] = result["usd_price"] as? Double ?? 0.0
            
            if let bedroomData = result["property_bedrooms"] as? Json {
                self.propertyData["bedrooms"] = bedroomData["data"] as? String ?? ""
            }
            
            if let bathroomData = result["property_bathrooms"] as? Json {
                self.propertyData["bathrooms"] = bathroomData["data"] as? String ?? ""
            }
            self.propertyData["available"] = "Available"
            
            if let ratingTotal = result["property_ratings_total"] as? JsonArray, !ratingTotal.isEmpty {
                let ratingValue = ratingTotal[0]["aggregate"] as? String ?? "0"
                self.self.propertyData["rating"] = Double(ratingValue)
                self.propertyData["ratingCount"] = ratingValue
            }
            
            self.propertyData["ownerId"] = result["user_id"]
            let property_type = result["property_type"] as? Int ?? 1
            self.isRentProperty = property_type == 1 ? false : true
            
            if let negotiableStatus = result["pirce_negotiable"] as? Int{
                self.isPriceNegotiable = negotiableStatus
            }
            
            if let reputedStatus = result["reputed_profile"] as? Int{
                self.isReputedRequired = reputedStatus
            }
            
            if let requestDataReceived = propertyResult["request_data"] as? JsonArray, !requestDataReceived.isEmpty {
                var requestData = requestDataReceived[0]
                print("## Request ID : ",self.requestId.description," ",requestDataReceived.description)
                for item in requestDataReceived{
                    if let _id = item["id"] as? Int, _id == self.requestId {
                        requestData = item
                    }
                }
                if let movingIn = requestData["moving_in"] as? String, let movingOut = requestData["moving_out"] as? String {
                    if let inDate = movingIn.components(separatedBy: " ").first, let outDate = movingOut.components(separatedBy: " ").first {
                        self.movingInDate = inDate.convertToADate(inputFormat: "yyyy-MM-dd")
                        self.movingOutDateOrginal = outDate.convertToADate(inputFormat: "yyyy-MM-dd")
                        self.movingOutDate = self.movingOutDateOrginal
                        print("vc.moving_inMyrequest",inDate)
                        print("vc.durationMyRequest",outDate)
                        
                    }
                    
                }
                
            }
            
            self.isThreadDetailsFinished = true
            
            if self.isFromPropertyDetails {
                if self.isThreadDetailsFinished {
                    BaseThread.asyncMain {
                       self.setUpInterface()

                    }
                }

            }else {
                if self.isThreadDetailsFinished {
                    BaseThread.asyncMain {
                        self.getRequestStatus()

                        //self.setUpInterface()
                    }
                }

            }
            
            
        }
        
    }
    
    
    fileprivate func getRequestStatus(){
        let url = "getRrequestStatus"
        var parameter = Json()
        parameter.updateValue(propertyId!, forKey: "property_id")
        parameter.updateValue(Credentials.shared.userId, forKey: "user_id")
        
        
        Webservices2.getMethodWith(url: url, parameter: parameter) { (isSucceeded, responseRrequest) in
            guard isSucceeded else { return }
            guard let responseDataRrequest = responseRrequest["Data"] as? Json, !responseDataRrequest.isEmpty else {
                let message = responseRrequest["Message"] as? String ?? "Error"
                showBanner(message: message)
                return
            }
            
            BaseThread.asyncMain {
                self.resultRequestStatus = KXDetailsRequestStatusModel.getValue(fromObject: responseDataRrequest)
                guard let requestStatus = self.resultRequestStatus else { return }
                self.requestId = requestStatus.response!.request_id
                self.isThreadRequestFinished = true
                if self.isThreadDetailsFinished && self.isThreadRequestFinished {
                    BaseThread.asyncMain {
                        self.setUpInterface()
                    }
                }
            }
            
        }
        
    }
    
    
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func movingInBTNTapped(_ sender: UIButton) {
       return
        
//        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: Date(), datePickerMode: .date) {
//            (date) -> Void in
//            if let dt = date {
//                let formatter = DateFormatter()
//                formatter.dateFormat = "MM/dd/yyyy"
//                self.movingInDateLBL.text = formatter.string(from: dt)
//                self.movingInDate = dt
//                print("MovingInDateas Date",self.movingInDate as Any)
//            }
//        }
        
    }
    
    @IBAction func movingOutBtnTapped(_ sender: Any) {
        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: self.movingInDate,maximumDate: self.movingOutDateOrginal, datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                self.movingOutDateLBL.text = formatter.string(from: dt)
                self.movingOutDate = dt
                print("MovingIOutDateas Date",self.movingOutDate as Any)

            }
        }
        
    }
    
    @IBAction func termsBtnTapped(_ sender: Any) {
        
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXTCViewController") as! KXTCViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func checkTapped(_ sender: UIButton) {
        if isCheckedTerms {
            termsImage.image = #imageLiteral(resourceName: "tick_grey")
            isCheckedTerms = false
        }else {
            termsImage.image = #imageLiteral(resourceName: "tick-sign")
            isCheckedTerms = true
        }

    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func slideMenuTapped(_ sender: Any) {
        
    }
    
    @IBAction func editofferAmountTapped(_ sender: Any) {
        offerAmountTF.isUserInteractionEnabled = true
    }
    
    @IBAction func submitBtnTapped(_ sender: Any) {
        
        if isReputedRequired == 0{
            guard isCheckedTerms else {
                showBanner(message: "Please accept terms and conditions")
                return
            }
            
            print("IsReptuted-False")

            postData()
        }else{
            print("IsReptuted-True",Credentials.shared.reputed)
            if currentReputedStatus == "1"{
                print("MyProfile-Reputed")
                guard isCheckedTerms else {
                    showBanner(message: "Please accept terms and conditions")
                    return
                }
                postData()
            }else{
                showBanner(message: "Your profile is not Reputed")
            }
        }
    }
    
    fileprivate func isValidDates() -> Bool {
        if movingInDate != nil && movingOutDate != nil {
            switch movingInDate!.compare(movingOutDate!) {
            case .orderedAscending     :
                print("movingInDate is earlier than movingOutDate")
                return true
            case .orderedDescending    :
                //showBanner(message: "Moving In date is later than Moving Out date")
                print("movingInDate is later than movingOutDate")
                return false
            case .orderedSame          :
                print("movingInDate is earlier than movingOutDate")
                return true
            }
            
        }else {
            showBanner(message: "Please select both Moving In and Moving out date")
            return false
        }
        
    }
    
    fileprivate func validateData() -> Bool {
        
        if isRentProperty {
            if movingInDate == nil || movingOutDate == nil {
                showBanner(message: "Please select both Moving In and Moving out date")
                return false
            }else{
                if propertyId != nil && BaseValidator.isNotEmpty(string: offerAmountTF.text) && isValidDates(){
                    return true
                }else {
                    if propertyId == nil {
                        showBanner(message: "Property Id Missing")
                        return false
                    }
                    if !BaseValidator.isNotEmpty(string: offerAmountTF.text) {
                        showBanner(message: "Please enter offer amount")
                        return false
                    }
                    if !isCheckedTerms {
                        showBanner(message: "Please check terms and condition")
                        return false
                    }
                    if !isValidDates() {
                        showBanner(message: "Moving In date is later than Moving Out date")
                        return false
                    }
                    
                    return false
                }
                
            }
        }else {
            if propertyId != nil && BaseValidator.isNotEmpty(string: offerAmountTF.text) {
                return true
            }else {
                if propertyId == nil {
                    showBanner(message: "Property Id Missing")
                    return false
                }
                if !BaseValidator.isNotEmpty(string: offerAmountTF.text) {
                    showBanner(message: "Please enter offer amount")
                    return false
                }
                if !isCheckedTerms {
                    showBanner(message: "Please check terms and condition")
                    return false
                }
                
                return false
            }
            
        }
        
        
        
    }
    
    
    fileprivate func postData(){
        
        guard validateData() else { return }
        let url = "requestOffer"
        var params = Json()
        
        params.updateValue(Credentials.shared.userId, forKey: "user_id")
        params.updateValue(self.propertyId!, forKey: "property_id")
        params.updateValue(self.requestId, forKey: "request_id")
        let offerAmount = self.offerAmountTF.text?.replacingOccurrences(of: ",", with: "") .trimmingCharacters(in: .whitespacesAndNewlines)
        params.updateValue(offerAmount!, forKey: "offer_amount")
        
        if isRentProperty {
            if let movingIn = movingInDate, let movingOut = movingOutDate {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let inString = formatter.string(from: movingIn)
                let outString = formatter.string(from: movingOut)

                params.updateValue(inString, forKey: "moving_in")
                params.updateValue(outString, forKey: "moving_out")
                
            }
            
        }
        
        Webservices2.getMethodWithMultipleErrorCode(url: url, parameter: params) {(isSucceeded, response) in
            guard isSucceeded else { return }
            //guard let data = response["Data"] as? Json else { return }
            let Errorcode = response["ErrorCode"] as? String ?? "1"
            let Message = response["Message"] as? String ?? "Error occured while fetching data"
            
            if Errorcode == "0" {
                showBanner(title: Message, message: Message, style: .success)
                // showBanner(message: Message)
            }else if Errorcode == "4" {
                showBanner(message: Message)
            }else if Errorcode == "6" {
                showBanner(message: Message)
            }else {
                //showBanner(message: Message)
                if let messageJson = response["Message"] as? Json {
                    let allkeys = Array(messageJson.keys)
                    for item in allkeys {
                        let messageArray = messageJson[item]  as! [String]
                        showBanner(message: messageArray[0])
                    }
                }
            }
            BaseThread.asyncMain {
                let vc = self.storyboardDashboard!.instantiateViewController(withIdentifier: "KXMyRequestVC") as! KXMyRequestVC
                self.navigationController?.pushViewController(vc, animated: true)

//                let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHistory") as! KXHistory
//                self.navigationController?.pushViewController(vc, animated: true)
            }

            
        }
        
    }
    
}

