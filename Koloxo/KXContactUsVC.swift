//
//  KXContactUsVC.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 28/12/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import WebKit

class KXContactUsVC: UIViewController, UIWebViewDelegate  {

    
    @IBOutlet var contactWebView: UIWebView!
    
    private let URLString = "https://koloxo.com/about-us/"

    private let activity = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: URLString)!
        contactWebView.loadRequest(URLRequest(url: url))
        contactWebView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {

        BaseThread.asyncMain {
            
            self.activity.start()

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        BaseThread.asyncMain {
            self.activity.stop()
        }

    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        showBanner(message: "Unable show Contact Information")
        BaseThread.asyncMain {
            self.activity.stop()
        }

    }
}
