//
//  KXCustomMarker.swift
//  Koloxo
//
//  Created by Appzoc on 21/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXCustomMarker: UIView {

    @IBOutlet var markerImage: UIImageView!
    @IBOutlet var markerBgImage: UIImageView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var bgView: UIView!
    
    func setLabelCornerRadius() {
        self.priceLabel.layer.cornerRadius = 8
//        self.priceLabel.layer.borderWidth = 1.0
//        self.priceLabel.layer.borderColor = UIColor.black.cgColor
//        self.priceLabel.layer.masksToBounds = true
        
//        self.priceLabel.layer.shadowColor = UIColor.black.cgColor
//        self.priceLabel.layer.shadowOffset = CGSize.init(width: 5, height: 5)
//        self.priceLabel.layer.opacity = 1.0
//        self.priceLabel.layer.shadowRadius = 5
        
        //self.priceLabel.layer.masksToBounds = true

    }

    func setSelectedBgColor() {
        self.priceLabel.backgroundColor = UIColor.init(red: 87/255, green: 86/255, blue: 214/255, alpha: 1.0)
        self.priceLabel.textColor = .white
        
    }
    
    func setDeselectedBgColor() {
        self.priceLabel.backgroundColor = .white
        self.priceLabel.textColor = .black

    }
    
}
