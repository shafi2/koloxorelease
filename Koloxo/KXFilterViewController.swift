//
//  KXFilterViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 18/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import RangeSeekSlider
import DropDown
import SideMenu

class KXFilterCommonTypeModel{
    var id: Int = 0
    var data: String = ""
    // for data source handling
    var isSelected: Bool = false
    
    init(){}
    
    init(object: Json){
        id = object["id"] as? Int ?? 0
        data = object["data"] as? String ?? ""
    }
    
    class func getValue(fromObject: Json?) -> KXFilterCommonTypeModel {
        guard let fromObject = fromObject else { return KXFilterCommonTypeModel()}
        return KXFilterCommonTypeModel(object: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXFilterCommonTypeModel] {
        var modelArray: [KXFilterCommonTypeModel] = []
        for item in fromArray {
            modelArray.append(KXFilterCommonTypeModel(object: item))
        }
        return modelArray
    }
}

class KXDistanceModel{
    var id:Int = 1
    var value:Int = 1
    var display_name:String = ""
    var display_order:Int = 0
    
    init(){}
    init(object: Json) {
        id = object["id"] as? Int ?? 0
        value = object["value"] as? Int ?? 1
        display_name = object["display_name"] as? String ?? ""
        display_order = object["display_order"] as? Int ?? 0
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDistanceModel] {
        
        var modelArray: [KXDistanceModel] = []
        for item in fromArray {
            modelArray.append(KXDistanceModel.init(object: item))
        }
        return modelArray
    }

}

class KXFilterCurrencyModel
{
    var id:Int = 0
    var currency:String = ""
    var currency_code:String = ""
    var country_id:Int = 0
    var symbol_left:String = ""
    var symbol_right:String = ""
    var decimal_places:Int = 0
    var value:Double = 0
    var status:Int = 0
    var last_updated:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    
    init(){}
    
    init(dictionary: Json) {
        
        id = dictionary["id"] as? Int ?? 0
        currency = dictionary["currency"] as? String ?? ""
        currency_code = dictionary["currency_code"] as? String ?? ""
        country_id = dictionary["country_id"] as? Int ?? 0
        symbol_left = dictionary["symbol_left"] as? String ?? ""
        symbol_right = dictionary["symbol_right"] as? String ?? ""
        decimal_places = dictionary["decimal_places"] as? Int ?? 0
        value = dictionary["value"] as? Double ?? 0
        status = dictionary["status"] as? Int ?? 0
        last_updated = dictionary["last_updated"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
    }
    
    class func getValue(fromArray: JsonArray) -> [KXFilterCurrencyModel] {
        var modelArray: [KXFilterCurrencyModel] = []
        for item in fromArray {
            modelArray.append(KXFilterCurrencyModel(dictionary: item))
        }
        return modelArray
    }
    
}


class KXBedroomCVC: UICollectionViewCell {
    @IBOutlet var selectionView: BaseGradientView!
    @IBOutlet var titleLBL: UILabel!
}

class KXBathroomCVC: UICollectionViewCell {
    @IBOutlet var selectionView: BaseGradientView!
    @IBOutlet var titleLBL: UILabel!
}

class KXPropertyTypeCVC: UICollectionViewCell {
    
    @IBOutlet var selectedTickMark: BaseImageView!
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var selectionView: BaseGradientView!
}

class KXFilterViewController: UIViewController, InterfaceSettable, CategorySelectionDelegate, RangeSeekSliderDelegate {

    @IBOutlet var placeTF: UITextField!
    @IBOutlet var DoubleSlider: RangeSeekSlider!
    @IBOutlet var buySelect: StandardButton!
    @IBOutlet var rentSelect: StandardButton!
    
    @IBOutlet var allBTNSelectedTick: BaseImageView!
    @IBOutlet var availableBTNTickMarkImage: BaseImageView!
    @IBOutlet var almostAvailableBTNTickMark: BaseImageView!
    @IBOutlet var notAvailableBTNSelectedTickMark: BaseImageView!
    
    
    @IBOutlet var all_Button: StandardButton!
    @IBOutlet var available_Button: StandardButton!
    @IBOutlet var notAvailable_Button: StandardButton!
    @IBOutlet var almost_Available: StandardButton!
    @IBOutlet var anyBedRoom_Btn: StandardButton!
    @IBOutlet var anybathRoom_Btn: StandardButton!
    @IBOutlet var Reset_Btn: StandardButton!
    @IBOutlet var heightIncreaseSearchAreaValueView: NSLayoutConstraint!
    @IBOutlet var heightPropertyTypeCV: NSLayoutConstraint!
    @IBOutlet var increaseAreaSwitch: UISwitch!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var view2: StandardView!
    @IBOutlet var propertyTypeCV: UICollectionView!
    @IBOutlet var minPriceLBL: UILabel!
    @IBOutlet var maxPriceLBL: UILabel!
    @IBOutlet var bedroomCV: UICollectionView!
    @IBOutlet var bathroomCV: UICollectionView!
    
    final var selectedLocation: String = ""
    final var isMapFilter: Bool = true
    final var parameterReceived: Json = Json()
    final var isRentalProperty: Bool = false
    final var citySelected: String = ""
    
    
    fileprivate var distanceSource: [KXDistanceModel] = []
    fileprivate var bedroomSource: [KXFilterCommonTypeModel] = []
    fileprivate var bathroomSource: [KXFilterCommonTypeModel] = []
    fileprivate var buildingTypeSource: [KXFilterCommonTypeModel] = []
    fileprivate var currencySource: [KXFilterCurrencyModel] = []
    fileprivate var minPrice: Int64 = 0
    fileprivate var maxPrice: Int64 = 1
    fileprivate var indexpathDistance: IndexPath = IndexPath(row: 0, section: 0)
    fileprivate var indexpathCurrency: IndexPath = IndexPath(row: 0, section: 0)
    
    fileprivate var isDistanceDropDown: Bool = true
    fileprivate var isAnyBedroom: Bool = false
    fileprivate var isAnyBathroom: Bool = false
    
    fileprivate var minPriceUpdated: Int64 = 0
    fileprivate var maxPriceUpdated: Int64 = 1
    fileprivate var selectedCurrency: String = ""
    fileprivate var selectedCurrencyId: Int = 0
    fileprivate var selectedCurrencyValue: Double = 1
    fileprivate var selectedPropertyType: Int = 1 // 1 for sell and 2 for rent
    fileprivate var selectedDistance: String = ""
    fileprivate var selectedAvailability: String = ""
    fileprivate var selectedMinPrice: String = ""
    fileprivate var selectedMaxPrice: String = ""
    fileprivate var selectedBedroomId: [Int] = []
    fileprivate var selectedBathroomId: [Int] = []
    fileprivate var selectedBuildingTypes: [Int] = []
    fileprivate var isDistanceOn: Bool = true
    
    fileprivate var availableSource: [KXSearchAvailableModel] = []
    fileprivate var notAvailableSource: [KXSearchAvailableModel] = []
    fileprivate var almostAvailableSource: [KXSearchAvailableModel] = []
    fileprivate var builidngSource: [KXSearchBuildingModel] = []
    fileprivate var availableBuilding:[KXSearchBuildingModel] = []
    fileprivate var notAvailableBuilding:[KXSearchBuildingModel] = []
    fileprivate var almostAvailableBuilding:[KXSearchBuildingModel] = []
    fileprivate var multiBuildingPropertySource: [KXSearchAvailableModel] = []
    fileprivate var multiBuildingAvailability: BuildingAvailability = .available
    fileprivate var buildingListViewAllSource: [KXSearchBuildingModel] = []
    
    fileprivate var isAvailable: Bool = false
    fileprivate var isAlmostAvailable: Bool = false
    fileprivate var isNotAvailable: Bool = false
    
    
    let selectedColor:UIColor = UIColor(red: 116/255, green: 107/255, blue: 247/255, alpha: 1)
    //old color - UIColor(red:0.46, green:0.49, blue:1.00, alpha:1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DoubleSlider.delegate = self
        increaseAreaSwitch.isOn = false
        turnOffSwitch()
        Reset_Btn.addTarget(self, action: #selector(holdRelease(sender:)), for: UIControlEvents.touchUpInside);
        Reset_Btn.addTarget(self, action: #selector(HoldDown(sender:)), for: UIControlEvents.touchDown)
        placeTF.text = citySelected
        getFilterAttributesWeb()
    }
    
    func setUpInterface() {
        selectedPropertyType = isRentalProperty ? 2 : 1
        selectedDistance = distanceSource[0].display_name
        self.distanceLabel.text = selectedDistance
        
        let firstCurrency = self.currencySource[0]
        let firstValue = firstCurrency.value
        let firstCurrencyCode = firstCurrency.currency_code
        let firstCurrencyId = firstCurrency.id
        self.minPriceUpdated = self.minPrice * Int64(firstValue)
        self.maxPriceUpdated = self.maxPrice * Int64(firstValue)
        self.selectedCurrency = firstCurrencyCode
        self.selectedCurrencyId = firstCurrencyId
        
        
        self.DoubleSlider.minValue = CGFloat(self.minPriceUpdated)
        self.DoubleSlider.maxValue = CGFloat(self.maxPriceUpdated)
        self.DoubleSlider.selectedMinValue = CGFloat(self.minPriceUpdated)
        self.DoubleSlider.selectedMaxValue = CGFloat(self.maxPriceUpdated)
        
        selectedMinPrice = self.DoubleSlider.selectedMinValue.description
        selectedMaxPrice = self.DoubleSlider.selectedMaxValue.description

        //self.DoubleSlider.minDistance = 10
        
        self.DoubleSlider.layoutSubviews()
        
        self.minPriceLBL.text = self.DoubleSlider.selectedMinValue.description
        self.maxPriceLBL.text = self.DoubleSlider.selectedMaxValue.description
        self.currencyLabel.text = firstCurrencyCode
        AnyBathroom(Any.self)
        Any_Bedroom(Any.self)
        
//        anyBedRoom_Btn.backgroundColor = selectedColor
//        anybathRoom_Btn.backgroundColor = selectedColor
//
//        isAnyBedroom = true
//        isAnyBathroom = true

        // select available property intially
        selectedAvailability = ""
        all_Button.backgroundColor = selectedColor
        all_Button.setTitleColor(.white, for: .normal)
        allBTNSelectedTick.isHidden = false
        
        available_Button.backgroundColor = UIColor.clear
        available_Button.setTitleColor(.black, for: .normal)
        availableBTNTickMarkImage.isHidden = true
        

        notAvailable_Button.backgroundColor = UIColor.clear
        notAvailable_Button.setTitleColor(.black, for: .normal)
        notAvailableBTNSelectedTickMark.isHidden = true
        
        almost_Available.backgroundColor = UIColor.clear
        almost_Available.setTitleColor(.black, for: .normal)
        almostAvailableBTNTickMark.isHidden = true

        if let cell = propertyTypeCV.cellForItem(at: IndexPath(item: 0, section: 0)) as? KXPropertyTypeCVC {
            cell.selectionView.isHidden = false
            cell.titleLBL.textColor = .white
            buildingTypeSource[0].isSelected = true
            selectedBuildingTypes.append(cell.titleLBL.tag)

        }

        bedroomCV.reloadData()
        bathroomCV.reloadData()
        propertyTypeCV.reloadData()
        
    }
    
    
    // Delegate from category selection
    func selectedValue(index: IndexPath) {
        if isDistanceDropDown {
            selectedDistance = distanceSource[index.row].display_name
            self.distanceLabel.text = selectedDistance
            self.indexpathDistance = index
        }else {
            selectedCurrency = currencySource[index.row].currency
            selectedCurrencyValue = currencySource[index.row].value
            selectedCurrencyId = currencySource[index.row].id

            self.currencyLabel.text = currencySource[index.row].currency_code
            
            self.minPriceUpdated = (self.minPrice.double * selectedCurrencyValue).int64
            self.maxPriceUpdated = (self.maxPrice.double * selectedCurrencyValue).int64//.rounded(.up).int64
            

            self.minPriceLBL.text = self.minPriceUpdated.description
            self.maxPriceLBL.text = self.maxPriceUpdated.description
            
            self.DoubleSlider.minValue = self.minPriceUpdated.cgFloat
            self.DoubleSlider.maxValue = self.maxPriceUpdated.cgFloat
            
            self.DoubleSlider.selectedMinValue = self.minPriceUpdated.cgFloat
            self.DoubleSlider.selectedMaxValue = self.maxPriceUpdated.cgFloat

            selectedMinPrice = self.DoubleSlider.selectedMinValue.description
            selectedMaxPrice = self.DoubleSlider.selectedMaxValue.description
            
//            print((self.minPrice.double * selectedCurrencyValue).int64)
//            print((self.maxPrice.double * selectedCurrencyValue).rounded(.up).int64)
//
            print("==========")
            print("selectedCurrencyValue:",selectedCurrencyValue)
            print("self.minPrice",self.minPrice)
            print("self.maxPrice",self.maxPrice)
            
            print("self.minPrice.double",self.minPrice.double)
            print("self.maxPrice.double",self.maxPrice.double)

            print("minPriceUpdated:",minPriceUpdated)
            print("maxPriceUpdated:",maxPriceUpdated)
            
            print("self.minPriceUpdated.cgFloat:",self.minPriceUpdated.cgFloat)
            print("self.maxPriceUpdated.cgFloat:",self.maxPriceUpdated.cgFloat)

            
            print("self.DoubleSlider.minValue:",self.DoubleSlider.minValue)
            print("self.DoubleSlider.maxValue:",self.DoubleSlider.maxValue)
            
//            print("self.DoubleSlider.selectedMinValue",self.DoubleSlider.selectedMinValue)
//            print("self.DoubleSlider.selectedMaxValue",self.DoubleSlider.selectedMaxValue)
            
            self.view.layoutIfNeeded()
            self.DoubleSlider.layoutIfNeeded()

        }
        
        
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        self.selectedMinPrice = minValue.int64.description
        self.selectedMaxPrice = maxValue.int64.description
//        print("rangeSeekSlider_Min:",self.selectedMinPrice,"Max:",selectedMaxPrice)
//        print("rangeSeekSlider",self.DoubleSlider.minValue)
//        print(self.DoubleSlider.maxValue)
//
//        print("rangeSeekSlider",self.DoubleSlider.selectedMinValue)
//        print(self.DoubleSlider.selectedMaxValue)

        
    }

    @IBAction func back_Button(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger(_ sender: Any) {
        showSideMenu()
    }

    @IBAction func Reset_Button(_ sender: Any) {
        getFilterAttributesWeb()
    }
    
    @IBAction func increaseAreaSwitchChanged(_ sender: UISwitch) {
        if sender.isOn {
            heightIncreaseSearchAreaValueView.constant = 50
            self.view.layoutIfNeeded()
            selectedDistance = distanceLabel.text ?? ""
            isDistanceOn = true
        }else{
            heightIncreaseSearchAreaValueView.constant = 0
            self.view.layoutIfNeeded()
            selectedDistance = ""
            isDistanceOn = false

        }
        
    }
    
    
    @IBAction func distanceDrop(_ sender: Any) {
        isDistanceDropDown = true
        let categorySelectionViewController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        categorySelectionViewController.setTitle(title: "Select Distance")
        categorySelectionViewController.tableSource = distanceSource.map{$0.display_name}
        categorySelectionViewController.delegate = self
        categorySelectionViewController.selectedIndexPath = indexpathDistance
        
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)

    }
    
    
    @IBAction func allPropertyTapped(_ sender: StandardButton) {
        
        all_Button.backgroundColor = selectedColor
        all_Button.setTitleColor(.white, for: .normal)
        allBTNSelectedTick.isHidden = false
        
        available_Button.backgroundColor = UIColor.clear
        available_Button.setTitleColor(.black, for: .normal)
        availableBTNTickMarkImage.isHidden = true
        
        notAvailable_Button.backgroundColor = UIColor.clear
        notAvailable_Button.setTitleColor(.black, for: .normal)
        notAvailableBTNSelectedTickMark.isHidden = true
        
        almost_Available.backgroundColor = UIColor.clear
        almost_Available.setTitleColor(.black, for: .normal)
        almostAvailableBTNTickMark.isHidden = true
        
        selectedAvailability = ""
        isAvailable = false
        isAlmostAvailable = false
        isNotAvailable = false

    }
    
    
    @IBAction func Available_Property(_ sender: Any) {
        if isAvailable
        {
//            isAvailable = false
//            selectedAvailability = ""
//            available_Button.backgroundColor = UIColor.white
//            available_Button.setTitleColor(.black, for: .normal)
//
//            all_Button.backgroundColor = selectedColor
//            all_Button.setTitleColor(.white, for: .normal)
            allPropertyTapped(StandardButton())
        }
        else
        {
            isAvailable = true
            isAlmostAvailable = false
            isNotAvailable = false

            selectedAvailability = "available"
            available_Button.backgroundColor = selectedColor
            available_Button.setTitleColor(.white, for: .normal)
            availableBTNTickMarkImage.isHidden = false
            
            notAvailable_Button.backgroundColor = UIColor.clear
            notAvailable_Button.setTitleColor(.black, for: .normal)
            notAvailableBTNSelectedTickMark.isHidden = true
            
            almost_Available.backgroundColor = UIColor.clear
            almost_Available.setTitleColor(.black, for: .normal)
            almostAvailableBTNTickMark.isHidden = true
            
            all_Button.backgroundColor = UIColor.clear
            all_Button.setTitleColor(.black, for: .normal)
            allBTNSelectedTick.isHidden = true

        }
    }
    
    @IBAction func Almost_Available(_ sender: Any) {
        if isAlmostAvailable
        {
            allPropertyTapped(StandardButton())

//            selectedAvailability = ""
//            almost_Available.backgroundColor = UIColor.white
//            almost_Available.setTitleColor(.black, for: .normal)
//            isAlmostAvailable = false
//
//            all_Button.backgroundColor = selectedColor
//            all_Button.setTitleColor(.white, for: .normal)

        }
        else
        {
            isAlmostAvailable = true
            isAvailable = false
            isNotAvailable = false
            
            selectedAvailability = "almost_available"
            
            available_Button.backgroundColor = UIColor.clear
            available_Button.setTitleColor(.black, for: .normal)
            availableBTNTickMarkImage.isHidden = true
            
            notAvailable_Button.backgroundColor = UIColor.clear
            notAvailable_Button.setTitleColor(.black, for: .normal)
            notAvailableBTNSelectedTickMark.isHidden = true
            
            almost_Available.backgroundColor = selectedColor
            almost_Available.setTitleColor(.white, for: .normal)
            almostAvailableBTNTickMark.isHidden = false

            all_Button.backgroundColor = UIColor.clear
            all_Button.setTitleColor(.black, for: .normal)
            allBTNSelectedTick.isHidden = true

        }
    }
    
    @IBAction func NotAvailable_Property(_ sender: Any) {
        if isNotAvailable
        {
            allPropertyTapped(StandardButton())

//            selectedAvailability = ""
//            notAvailable_Button.backgroundColor = UIColor.white
//            notAvailable_Button.setTitleColor(.black, for: .normal)
//            isNotAvailable = false
//
//            all_Button.backgroundColor = selectedColor
//            all_Button.setTitleColor(.white, for: .normal)
//            isAll = true

        }
        else
        {
            isNotAvailable = true
            isAlmostAvailable = false
            isAvailable = false

            selectedAvailability = "not_available"
            available_Button.backgroundColor = UIColor.clear
            available_Button.setTitleColor(.black, for: .normal)
            availableBTNTickMarkImage.isHidden = true
            
            notAvailable_Button.backgroundColor = selectedColor
            notAvailable_Button.setTitleColor(.white, for: .normal)
            notAvailableBTNSelectedTickMark.isHidden = false
            
            almost_Available.backgroundColor = UIColor.clear
            almost_Available.setTitleColor(.black, for: .normal)
            almostAvailableBTNTickMark.isHidden = true
            
            all_Button.backgroundColor = UIColor.clear
            all_Button.setTitleColor(.black, for: .normal)
            allBTNSelectedTick.isHidden = true
        }
    }

    
    @IBAction func currencyTapped(_ sender: UIButton) {
        isDistanceDropDown = false
        let categorySelectionViewController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        categorySelectionViewController.setTitle(title: "Select Currency")
        categorySelectionViewController.tableSource = currencySource.map{$0.currency}
        categorySelectionViewController.delegate = self
        categorySelectionViewController.selectedIndexPath = indexpathCurrency
        
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)

    }
    
    
    //target functions
    @objc func HoldDown(sender:UIButton)
    {
        Reset_Btn.backgroundColor = selectedColor
        Reset_Btn.setTitleColor(.white, for: [])
    }
    
    @objc func holdRelease(sender:UIButton)
    {
        Reset_Btn.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        Reset_Btn.setTitleColor(.black, for: [])
    }


    @IBAction func Any_Bedroom(_ sender: Any) {
        if isAnyBedroom {
            anyBedRoom_Btn.backgroundColor = .white
            anyBedRoom_Btn.setTitleColor(.black, for: .normal)
            isAnyBedroom = false
            selectedBedroomId.removeAll()
        }else {
            anyBedRoom_Btn.backgroundColor = selectedColor
            anyBedRoom_Btn.setTitleColor(.white, for: .normal)
            isAnyBedroom = true
        }
        bedroomCV.reloadData()

    }
    
    @IBAction func AnyBathroom(_ sender: Any) {
        if isAnyBathroom {
            anybathRoom_Btn.backgroundColor = .white
            anybathRoom_Btn.setTitleColor(.black, for: .normal)
            isAnyBathroom = false
            selectedBathroomId.removeAll()

        }else{
            anybathRoom_Btn.backgroundColor = selectedColor
            anybathRoom_Btn.setTitleColor(.white, for: .normal)
            isAnyBathroom = true
        }
        bathroomCV.reloadData()
    }


    
    
    ///Button Actions
    @IBAction func SubMit_Button(_ sender: Any) {
        self.generateFilterParams { (filterParameter) in
            BaseThread.asyncMain {
                if self.isMapFilter {
                    //self.back(number: 1, from: self)

                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                    if let vc = viewControllers[viewControllers.count - 2] as? KXSearchResultViewController {
                        print("back to searchResultVC")
                        vc.citySelected = self.citySelected
                        vc.isRentalProperty = self.isRentalProperty
                        vc.parameterReceived = filterParameter
                        vc.isFromFilter = true
                        self.navigationController?.popToViewController(vc, animated: true)
                    }else {
                        print("created new search Result vc")
                        let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXSearchResultViewController") as! KXSearchResultViewController
                         vc.citySelected = self.citySelected
                         vc.isRentalProperty = self.isRentalProperty
                         vc.parameterReceived = filterParameter
                         
                         self.navigationController?.pushViewController(vc, animated: true)

                    }

                }else {
                    
//                    self.back(number: 2, from: self)
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                    if let vc = viewControllers[viewControllers.count - 3] as? KXSearchResultViewController {
                        print("back to searchResultVC Else Case")
                        vc.citySelected = self.citySelected
                        vc.isRentalProperty = self.isRentalProperty
                        vc.parameterReceived = filterParameter
                        vc.isFromFilter = true
                        self.navigationController?.popToViewController(vc, animated: true)
                    }else {
                        print("created new search Result vc Else case")

                        let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXSearchResultViewController") as! KXSearchResultViewController
                        vc.citySelected = self.citySelected
                        vc.isRentalProperty = self.isRentalProperty
                        vc.parameterReceived = filterParameter
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                        /*  let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXSearchResultListingBuildingVC") as! KXSearchResultListingBuildingVC
                         
                         vc.citySelected = self.citySelected
                         vc.isRentalProperty = self.isRentalProperty
                         vc.parameterReceived = filterParameter
                         vc.citySelected = self.citySelected
                         vc.isFromMap = false
                         
                         self.navigationController?.pushViewController(vc, animated: true)*/

                    }


                    
                }
                
                
            }
        }
        
        //navigationController?.popViewController(animated: true)
    }
    
    func turnOffSwitch(){
        heightIncreaseSearchAreaValueView.constant = 0
        self.view.layoutIfNeeded()
        selectedDistance = ""
        isDistanceOn = false
    }
    
    
}



// Handling API
extension KXFilterViewController {
    fileprivate func getFilterAttributesWeb() {
        let url = "getFilterAttributes"
        Webservices2.getMethod(url: url) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataObject = response["Data"] as? Json, !dataObject.isEmpty else { return }
            if let distances = dataObject["distance"] as? JsonArray, !distances.isEmpty {
                self.distanceSource = KXDistanceModel.getValue(fromArray: distances)
            }
            if let building_type = dataObject["building_type"] as? JsonArray, !building_type.isEmpty {
                self.buildingTypeSource = KXFilterCommonTypeModel.getValue(fromArray: building_type)
                //debugPrint("buildingTypeSource",self.buildingTypeSource)

            }
            if let bedrooms = dataObject["bedroom"] as? JsonArray, !bedrooms.isEmpty {
                self.bedroomSource = KXFilterCommonTypeModel.getValue(fromArray: bedrooms)
                //debugPrint("bedroomSource",self.bedroomSource)

            }
            if let bathrooms = dataObject["bathrooms"] as? JsonArray, !bathrooms.isEmpty {
                self.bathroomSource = KXFilterCommonTypeModel.getValue(fromArray: bathrooms)
                //debugPrint("bathroomSource",self.bathroomSource)
            }
            
            if let min = dataObject["minprice"] as? Int64 {
                self.minPrice = min
                print("min",min)
            }
            if let max = dataObject["maxprice"] as? Int64 {
                self.maxPrice = max
                print("max0",max)
            }

            if let currencies = dataObject["currency"] as? JsonArray, !currencies.isEmpty {
                
                self.currencySource = KXFilterCurrencyModel.getValue(fromArray: currencies)
                //debugPrint(self.currencySource as AnyObject)
            }

            BaseThread.asyncMain {
                self.setUpInterface()
            }
            
        }
        
    }
    
    /*
     http://koloxo-homes.dev.webcastle.in/api/searchProperty?latitude=10.5284793&longitude=76.2681871&saletype=1&take=4&skip=0&bathrooms[0]=1&bathrooms[1]=3&bathrooms[2]=2&floor[0]=1&building_type[0]=1&building_type[1]=2&currency=7&maxprice=101
     */
    
    
    fileprivate func generateFilterParams(completed: @escaping ((Json)->())){
//        let url = "searchProperty"
        var filterParams = parameterReceived
        //print("Filterparams",filterParams as AnyObject)
        if isDistanceOn {
            filterParams.updateValue(distanceLabel.text ?? "", forKey: "distance")
        }else {
            if let _ = filterParams["distance"] {
                filterParams.removeValue(forKey: "distance")
            }
        }
        filterParams.updateValue(selectedAvailability, forKey: "availability")
        filterParams.updateValue(selectedCurrencyId, forKey: "currency")
        filterParams.updateValue(selectedMaxPrice, forKey: "maxprice")
        filterParams.updateValue(selectedMinPrice, forKey: "minprice")
        
        if !isAnyBedroom {
            var i = 0
            for item in selectedBedroomId {
                filterParams.updateValue(item, forKey: "bedroom" + "[" + i.description + "]")
                i += 1
            }
        }
        
        if !isAnyBathroom {
            var i = 0
            for item in selectedBathroomId {
                filterParams.updateValue(item, forKey: "bathrooms" + "[" + i.description + "]")
                i += 1
            }
        }
        
        if !selectedBuildingTypes.isEmpty {
            var i = 0
            for item in selectedBuildingTypes {
                filterParams.updateValue(item, forKey: "building_type" + "[" + i.description + "]")
                i += 1
            }
        }
        
        completed(filterParams)
/*
         http://koloxo-homes.dev.webcastle.in/api/searchProperty?latitude=10.5284793&longitude=76.2681871&saletype=1&take=4&skip=0&bathrooms[0]=1&bathrooms[1]=3&bathrooms[2]=2&floor[0]=1&building_type[0]=1&building_type[1]=2&currency=7&maxprice=101
         */

        
    }
    
}

extension KXFilterViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == propertyTypeCV {
            //let size: CGSize = buildingTypeSource[indexPath.item].data.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Montserrat", size: 14.0)!])
            //return CGSize(width: size.width + 19, height: 40)
            return CGSize(width: collectionView.frame.width/2 - 5 , height: 40)
        }else{
            return CGSize(width: 32, height: 32)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == propertyTypeCV {
            return buildingTypeSource.count
        }else if collectionView == bedroomCV {
            return bedroomSource.count
        }else {
            return bathroomSource.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == propertyTypeCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyTypeCVC", for: indexPath) as! KXPropertyTypeCVC
            if buildingTypeSource[indexPath.item].isSelected {
                cell.selectionView.isHidden = false
                cell.titleLBL.textColor = .white
                cell.titleLBL.text = buildingTypeSource[indexPath.item].data
                cell.titleLBL.tag = buildingTypeSource[indexPath.item].id
            }else{
                cell.selectionView.isHidden = true
                cell.titleLBL.textColor = .black
                cell.titleLBL.text = buildingTypeSource[indexPath.item].data
                cell.titleLBL.tag = buildingTypeSource[indexPath.item].id
            }
            return cell
        }else if collectionView == bedroomCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXBedroomCVC", for: indexPath) as! KXBedroomCVC
           // print("Cell-KXBedroomCVC",cell.selectionView.frame.size,"Label",cell.titleLBL.frame.size)

            cell.selectionView.layer.cornerRadius = cell.selectionView.frame.width / 2
            if isAnyBedroom {
                cell.selectionView.isHidden = true
                cell.titleLBL.textColor = .black
                cell.titleLBL.text = bedroomSource[indexPath.item].data
                cell.titleLBL.tag = bedroomSource[indexPath.item].id
                bedroomSource[indexPath.item].isSelected = false
            }else{
                if bedroomSource[indexPath.item].isSelected {
                    cell.selectionView.isHidden = false
                    cell.titleLBL.textColor = .white
                    cell.titleLBL.text = bedroomSource[indexPath.item].data
                    cell.titleLBL.tag = bedroomSource[indexPath.item].id
                    //bedroomSource[indexPath.item].isSelected = true
                }else{
                    cell.selectionView.isHidden = true
                    cell.titleLBL.textColor = .black
                    cell.titleLBL.text = bedroomSource[indexPath.item].data
                    cell.titleLBL.tag = bedroomSource[indexPath.item].id
                    bedroomSource[indexPath.item].isSelected = false
                    
                }
            }
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXBathroomCVC", for: indexPath) as! KXBathroomCVC
            //print("Cell-BathroomCVC",cell.selectionView.frame.size,"Label",cell.titleLBL.frame.size)
            cell.selectionView.layer.cornerRadius = cell.selectionView.frame.width / 2

            if isAnyBathroom {
                cell.selectionView.isHidden = true
                cell.titleLBL.textColor = .black
                cell.titleLBL.text = bathroomSource[indexPath.item].data
                cell.titleLBL.tag = bathroomSource[indexPath.item].id
                bathroomSource[indexPath.item].isSelected = false
            }else{
                if bathroomSource[indexPath.item].isSelected {
                    cell.selectionView.isHidden = false
                    cell.titleLBL.textColor = .white
                    cell.titleLBL.text = bathroomSource[indexPath.item].data
                    cell.titleLBL.tag = bathroomSource[indexPath.item].id
                    bathroomSource[indexPath.item].isSelected = true
                }else{
                    cell.selectionView.isHidden = true
                    cell.titleLBL.textColor = .black
                    cell.titleLBL.text = bathroomSource[indexPath.item].data
                    cell.titleLBL.tag = bathroomSource[indexPath.item].id
                    bathroomSource[indexPath.item].isSelected = false
                }
            }
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == propertyTypeCV {
            
            let cell = collectionView.cellForItem(at: indexPath) as! KXPropertyTypeCVC
            cell.isSelected = false
            if buildingTypeSource[indexPath.item].isSelected {
                cell.selectionView.isHidden = true
                cell.selectedTickMark.isHidden = true
                cell.titleLBL.textColor = .black
                buildingTypeSource[indexPath.item].isSelected = false
                if selectedBuildingTypes.contains(cell.titleLBL.tag) {
                    if let index = selectedBuildingTypes.index(of: cell.titleLBL.tag) {
                        selectedBuildingTypes.remove(at: index)
                    }
                }
            }else {
                cell.selectionView.isHidden = false
                cell.selectedTickMark.isHidden = false
                cell.titleLBL.textColor = .white
                buildingTypeSource[indexPath.item].isSelected = true
                selectedBuildingTypes.append(cell.titleLBL.tag)
            }
        }else if collectionView == bedroomCV {
            isAnyBedroom = false
            anyBedRoom_Btn.backgroundColor = .white
            anyBedRoom_Btn.setTitleColor(.black, for: .normal)

            let cell = collectionView.cellForItem(at: indexPath) as! KXBedroomCVC
            cell.isSelected = false
            
            if bedroomSource[indexPath.item].isSelected {
                cell.selectionView.isHidden = true
                cell.titleLBL.textColor = .black
                if selectedBedroomId.contains(cell.titleLBL.tag) {
                    if let index = selectedBedroomId.index(of: cell.titleLBL.tag) {
                        selectedBedroomId.remove(at: index)
                    }
                }
                bedroomSource[indexPath.item].isSelected = false
                if selectedBedroomId.isEmpty {
                    Any_Bedroom((Any).self)
                }
            }else {
                cell.selectionView.isHidden = false
                cell.titleLBL.textColor = .white
                bedroomSource[indexPath.item].isSelected = true
                selectedBedroomId.append(cell.titleLBL.tag)
            }
            //print("didSelect_Bedroom_SelectionView",cell.selectionView.frame.size,"Label:",cell.titleLBL.frame.size)
        }else {
            isAnyBathroom = false
            anybathRoom_Btn.backgroundColor = .white
            anybathRoom_Btn.setTitleColor(.black, for: .normal)

            let cell = collectionView.cellForItem(at: indexPath) as! KXBathroomCVC
            cell.isSelected = false
            
            if bathroomSource[indexPath.item].isSelected {
                cell.selectionView.isHidden = true
                cell.titleLBL.textColor = .black
                if selectedBathroomId.contains(cell.titleLBL.tag) {
                    if let index = selectedBathroomId.index(of: cell.titleLBL.tag) {
                        selectedBathroomId.remove(at: index)
                    }
                }
                bathroomSource[indexPath.item].isSelected = false
                if selectedBathroomId.isEmpty {
                    AnyBathroom(Any.self)
                }
            }else {
                cell.selectionView.isHidden = false
                cell.titleLBL.textColor = .white
                bathroomSource[indexPath.item].isSelected = true
                selectedBathroomId.append(cell.titleLBL.tag)
            }

        }

    }
    
}


