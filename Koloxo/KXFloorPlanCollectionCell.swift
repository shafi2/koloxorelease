//
//  KXFloorPlanCollectionCell.swift
//  Koloxo
//
//  Created by Appzoc on 23/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXFloorPlanCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var floorPlanImage: UIImageView!
    
    @IBOutlet weak var removeImageButtonRef: UIButton!
    
    
}
