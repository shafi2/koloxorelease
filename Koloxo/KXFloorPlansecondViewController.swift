//
//  KXFloorPlansecondViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 21/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu

class KXFloorPlansecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Humberger_Menu(_ sender: Any) {
        showSideMenu()
//        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Next_Button(_ sender: Any) {
        let homeScreen = storyboardProperty!.instantiateViewController(withIdentifier: "KXAddplacemarker") as! KXAddplacemarker
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
