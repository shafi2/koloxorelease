    //
//  KXHomeViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 13/03/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu

class KXHomeViewController: UIViewController, InterfaceSettable
{

    override func viewDidLoad()
    {
        super.viewDidLoad()
        checkNotificationCount()
        setUpInterface()
        checkChatCount()
//        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: KXGuestSideMenuViewController)
//        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
//        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
    }
    
    func setUpInterface() {
        if let firstName = BaseMemmory.read(forKey: "firstName") as? String {
            Credentials.shared.firstName = firstName
        }
        if let lastName = BaseMemmory.read(forKey: "lastName") as? String {
            Credentials.shared.lastName = lastName

        }
        if let location = BaseMemmory.read(forKey: "location") as? String {
            Credentials.shared.country = location

        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Humberger_Menu(_ sender: Any)
    {
//        let sideMenuVC =  storyboardMain!.instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
        showSideMenu()
    }
    
    func checkNotificationCount(){
        NotificationManager.shared.getNotificationCount()
    }
    
    func checkChatCount(){
        Chat.getTotalUnreadCount()
    }
    
    
    @IBAction func SearchButton(_ sender: Any)
    {
        let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func AddPropertyButton(_ sender: Any)
    {
        ////if  logged in
        
        
        if isLoggedIn() {
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)

        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        //if logged in
//        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXAddPropertyViewController") as! KXAddPropertyViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    

//    @IBAction func Humberger_Menu(_ sender: Any) {
//        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXGuestSideMenuViewController") as! KXGuestSideMenuViewController
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
