//
//  KXInviteChatViewController.swift
//  Koloxo
//
//  Created by Appzoc on 31/07/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXInviteChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var passingPropertyID:Int = 0
    var propertyOwnerID:Int = 0
    var channelID:Int = 0
    
    @IBOutlet var tableRef: UITableView!
    fileprivate var invitedUsers:[InvitedUserData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        passingPropertyID = 724
        setUpWebCall()
    }
    
    func setUpWebCall(){
        Webservices.getMethodWith(url: "inviteUser", parameter: ["property_id":"\(passingPropertyID)"]) { (isComplete, json) in
            if isComplete{
                do{
                    let jsonData = json["Data"] as? [[String:Any]]
                    let decoder = JSONDecoder()
                    let trueData = try JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
                    let parsedData = try decoder.decode([InvitedUserData].self, from: trueData)
                    self.invitedUsers = parsedData
                    self.tableRef.reloadData()
                }catch{
                    print(error)
                }
            }
        }
    }
    
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invitedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXInviteUserCell") as! KXInviteUserCell
        cell.profileName.text = invitedUsers[indexPath.row].users.firstName
        let imgURL = URL(string: baseImageURL + invitedUsers[indexPath.row].users.image)
        cell.profileView.kf.setImage(with: imgURL, placeholder: #imageLiteral(resourceName: "userPlaceholder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       addUserToChannel(user: invitedUsers[indexPath.row].userID)
    }
    
    func addUserToChannel(user:Int){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        Webservices.postMethod(url: "addUserToChannel", parameter: ["channelID":"\(channelID)","userID":"\(user)","tokenkey":tokenKey,"unix_time":unixTime]) { (isComplete, json) in
            print(json)
            if isComplete{
                NotificationCenter.default.post(name: .userInvitedToChat, object: nil)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

}


extension KXInviteChatViewController {
    
    struct InvitedUserData: Codable{
        let id:Int
        let userID:Int
        let users:Users
        
        private enum CodingKeys:String, CodingKey{
            case  id , userID = "user_id", users
        }
        
        struct Users: Codable{
            let firstName:String
            let id:Int
            let image:String
            
            private enum CodingKeys:String, CodingKey{
                case firstName = "first_name", id , image
            }
        }
    }
    
}

class KXInviteUserCell: UITableViewCell {
    
    @IBOutlet var profileView: StandardImageView!
    
    @IBOutlet var profileName: UILabel!
    
}
