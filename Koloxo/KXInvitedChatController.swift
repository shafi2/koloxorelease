//
//  KXInvitedChatController.swift
//  Koloxo
//
//  Created by Appzoc on 26/07/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXInvitedChatController: UIViewController {

    @IBOutlet var tableRef: UITableView!
    var dataSource:KXChatMyGeneralModel?
    
    var accessNav:UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableRef.tableFooterView = UIView()
        guard let _ = dataSource else{return}
    }

    @IBAction func touchedOutside(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension KXInvitedChatController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource!.invitedChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXInvitedChatCell") as! KXInvitedChatCell
        if let source = dataSource?.invitedChats{
            cell.chatUserImage.kf.setImage(with: URL(string: "\(baseImageURL)\(source[indexPath.row].image)"), placeholder: UIImage(named: "userPlaceholder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            cell.chatUserName.text = source[indexPath.row].name
            let ownName = Credentials.shared.firstName + " " + Credentials.shared.lastName
            if source[indexPath.row].name == ownName {
                cell.invitedView.isHidden = true
            }else{
                cell.invitedView.isHidden = false
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = dataSource{
             let vc = self.storyboardChat!.instantiateViewController(withIdentifier: "KXChatDetailVC") as! KXChatDetailVC
            vc.channelName = dataSource!.invitedChats[indexPath.row].channel //myGeneralSource[indexPath.row].chat_owner_ship!.channel
            vc.channelID = dataSource!.invitedChats[indexPath.row].id //myGeneralSource[indexPath.row].chat_owner_ship!.id
            vc.propertyID = dataSource!.propertyId //myGeneralSource[indexPath.row].propertyId
            vc.propertyOwnerId = dataSource!.propertyOwnerId //myGeneralSource[indexPath.row].propertyOwnerId
            vc.propertyName = dataSource!.propertyName //myGeneralSource[indexPath.row].propertyName
            self.dismiss(animated: true, completion: nil)
            self.accessNav?.pushViewController(vc, animated: true)
        }
    }
}

class KXInvitedChatCell: UITableViewCell{
    
    @IBOutlet var chatUserImage: UIImageView!
    @IBOutlet var chatUserName: UILabel!
    @IBOutlet var invitedView: StandardView!
    
}
