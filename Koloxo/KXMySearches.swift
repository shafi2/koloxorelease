//
//  KXMySearches.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 19/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu

class KXMySearches: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var Recently_Viewed_tab: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func Humberger(_ sender: Any) {
        // UISideMenuNavigationController
        // sideMenuNavigation
        showSideMenu()
//        let sideMenuVC =  storyboardMain!.instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! KXMySearchCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    @IBAction func Tab_Home(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Tab_Button(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

  

}
