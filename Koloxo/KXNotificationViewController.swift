//
//  KXNotificationViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 09/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu

class KXNotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var Notification_tab: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! KXNotificationTableCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXPropertyListViewController") as! KXPropertyListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Back_Button(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_Menu(_ sender: Any) {
        showSideMenu()
//        let sideMenuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
}
