//
//  KXPlacemarkCell.swift
//  Koloxo
//
//  Created by Appzoc on 28/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXPlacemarkCell: UITableViewCell {
    
    
    
    @IBOutlet weak var buildingName: UILabel!
    
    @IBOutlet weak var propertyImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
