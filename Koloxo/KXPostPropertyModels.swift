//
//  KXPostPropertyModels.swift
//  Koloxo
//
//  Created by Appzoc on 21/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

struct PropertyDetailOne:Codable {
    let currency:[KXCurrency]
    let building_type:[KXPropertyType]
    let ages:[KXAgeBuilding]
    let elevators:[KXElevator]
    let floor:[KXBuildingFloor]
    let saletype:[KXBuyRentType]
    let Bedroom_counts:[KXPostPropertyGeneral]
    let Bathroom_count:[KXPostPropertyGeneral]
}

struct KXCurrency:Codable{
    let id:Int
    let currency:String
    let currency_code:String
    
//    init(json:Json) {
//        self.id = json[""] as! String
//        self.country = json[""] as! String
//        self.currency = json[""] as! String
//        self.name = json[""] as! String
//        self.country_code = json[""] as! String
//    }
//
//    init(toSend id:String ) {
//        self.id = id
//        self.country = ""
//        self.currency = ""
//        self.name = ""
//        self.country_code = ""
//    }
}

struct KXPropertyType:Codable{
    let id:Int
    let data:String
    let type: Int
//    init(json:Json) {
//        self.id = json[""] as! String
//        self.type = json[""] as! String
//    }
//
//    init(toSend type:String) {
//        self.id = ""
//        self.type = type
//    }
}

struct KXAgeBuilding:Codable {
    let id:Int
    let data:String
}

struct KXElevator:Codable {
    let id:Int
    let data:String
}

struct KXBuildingFloor:Codable {
    let id:Int
    let data:String
}

struct KXBuyRentType:Codable {
    let id:Int
    let type:String
}

struct KXFacilities:Codable {
    let id:Int
    let facility:String
}
//[{"id":1,"furniture":"Bedroom"},{"id":2,"furniture":"kitchen"}]
struct KXFurnitureData:Codable{
    let id:Int
    let furniture:String
}

/*
 "POST
 
 userid,
 property_id,
 furniture_data:[
 {""furniture_id"":15,
 [{""title"":""bedroomtitle1"",""description"":""bedromdescription""}]
 },
 {""furniture_id"":15,
 [{""title"":""bedroomtitle1"",""description"":""bedromdescription""}]
 }
 ]"
 
 
 [
     {"furniture_id":15,
     "data":{"title":"adfs","description":"bedromdescription"}
     },
     {"furniture_id":15,
     "data":{"title":"adfs","description":"bedromdescription"}
     }
 ]
 
 */
struct KXPropertySendData{
    let furniture_id:Int
    let data: RoomDescription
    
}

struct KXPropertyImageAttributes:Codable{
    let id:Int
    let name:String
}
