//
//  KXPreviousBookingsTVC.swift
//  Koloxo
//
//  Created by Appzoc on 16/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXPreviousBookingsTVC: UITableViewCell {

    
    @IBOutlet var moveInDateLBL: UILabel!
    @IBOutlet var moveOutDateLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureWith(data:KXPreviousBookingData){
        self.moveInDateLBL.text = data.movingIn
        self.moveOutDateLBL.text = data.movingOut
        //movingIn
        //movingOut
    }

}
