//
//  KXPreviousBookingsVC.swift
//  Koloxo
//
//  Created by Appzoc on 16/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXPreviousBookingsVC: UIViewController {

    
    @IBOutlet var tableRef: UITableView!
    @IBOutlet var emptyIndicatorLBL: UILabel!
    
    var dataSource:[KXPreviousBookingData] = []{
        didSet{
            if dataSource.count > 0{
                self.emptyIndicatorLBL.isHidden = true
            }else{
                self.emptyIndicatorLBL.isHidden = false
            }
        }
    }
    var propertyID:String = ""
    var availableDate:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Get deal on property : \(propertyID)")
        tableRef.tableFooterView = UIView()
        setupWebcall()
    }
    
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func setupWebcall(){
        Webservices.getMethodWith(url: "getPropertyDealDates", parameter: ["propertyid":"\(propertyID)"]) { (isCompleted, json) in
            if isCompleted{
                if let trueData = json["Data"] as? [String:Any], let dateData = trueData["property_dates"] as? [[String:Any]] {
                    if let availableDateData = trueData["Available_Date"] as? [[String:Any]], availableDateData.count > 0{
                        print("#Min avaialable : ",availableDateData.description)
                        if let availableReportedDate = availableDateData.first!["date_moving"] as? String{
                            self.availableDate = availableReportedDate
                        }
                    }
                    self.dataSource.removeAll()
                    for item in dateData{
                        let obj = KXPreviousBookingData(data: item)
                        
                        self.dataSource.append(obj)
                    }
                    print(self.dataSource)
                    self.dataSource = self.showDateGap(data: self.dataSource)
                    print(self.dataSource)
                    self.dataSource = self.getAvailableDatesForDisplay(data: self.dataSource)
                    print(self.dataSource)
                    if self.dataSource.count == 0 {
                        
                        let dateformater = DateFormatter()
                        dateformater.dateFormat = "dd/mm/yyyy"
                        dateformater.locale = NSLocale(localeIdentifier: "en_US") as Locale!

                        if  let date =   self.availableDate.date{
                           let year =  Calendar.current.component(Calendar.Component.year, from: date)
                            self.dataSource.append(KXPreviousBookingData(startDate: self.availableDate, endDate: "12/31/\(year)"))
                        }
                    }
                    print("Datasource $$",self.dataSource.description)
                    self.tableRef.reloadData()
                }
            }
        }
    }
    
    func showDateGap(data:[KXPreviousBookingData]) -> [KXPreviousBookingData]{
        let newSource = data.map({getDateRange(dateStart: $0.movingIn, dateEnd: $0.movingOut)})
        var newSourceArray:[KXPreviousBookingData] = []
        let _ = newSource.map({if let tuple = $0{
                let item = KXPreviousBookingData(startDate: tuple.0,endDate:tuple.1)
                newSourceArray.append(item)
            }})
        return newSourceArray
    }
    
    func getAvailableDatesForDisplay(data:[KXPreviousBookingData]) -> [KXPreviousBookingData]{
        guard data.count > 0 else{return []}
        let dateFormatter = DateFormatter()
        var returnData:[KXPreviousBookingData] = []
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let today = compareAvailableDate() //dateFormatter.string(from: Date())
        
        print("# Finalised today : ",today)
        
        let firstDate = data[0].movingIn
        let lastDate = data[data.count - 1].movingOut
        var insertionFirstDate = ""
        var insertionLastDate = ""
        
        if firstDate != today{
            insertionFirstDate = today
            returnData.append(KXPreviousBookingData(startDate: insertionFirstDate, endDate: firstDate))
        }
            
        else{
            insertionFirstDate = firstDate
            returnData.append(KXPreviousBookingData(startDate: insertionFirstDate, endDate: insertionFirstDate))
        }
        
      
        
        
        
        for item in 0...(data.count-1){
            if item + 1 < data.count{
                returnData.append(KXPreviousBookingData(startDate: data[item].movingOut, endDate: data[item + 1].movingIn))
                print(data)
                //movingIn
                //movingOut
            }
        }
       
       
        if isLastDayOfYear(date: lastDate){
            insertionLastDate = lastDate
        }else{
            insertionLastDate = "31/12/\(lastDate.suffix(4))"
            returnData.append(KXPreviousBookingData(startDate: lastDate, endDate: insertionLastDate))
           
        }
        return returnData
    }
    
    
    func isLastDayOfYear(date:String) -> Bool{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if let dateObj = dateFormatter.date(from: date){
            let day = Calendar.current.component(.day, from: dateObj)
            let month = Calendar.current.component(.month, from: dateObj)
            if day == 31, month == 12{
                return true
            }
        }
        return false
    }
    
    func compareAvailableDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"//"yyyy/MM/dd"
        if let reportedAvailableDate = dateFormatter.date(from: availableDate){
            if reportedAvailableDate.compare(Date()) == .orderedDescending{
                dateFormatter.dateFormat = "dd/MM/yyyy"
                return dateFormatter.string(from: reportedAvailableDate)
            }
                
                
//         // to set available date change done by shyam 20/2/19
//            else if reportedAvailableDate.compare(Date()) == .orderedAscending{
//                dateFormatter.dateFormat = "dd/MM/yyyy"
//                 let lastday = dateFormatter.date(from: dataSource[0].movingOut)
//                return dateFormatter.string(from:lastday!)
//
//           }
//                
                
            else{
                dateFormatter.dateFormat = "dd/MM/yyyy"
                return dateFormatter.string(from: Date())
            }
        }else{
            return ""
        }
    }
    
    func getAvailableDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"//"yyyy/MM/dd"
        if let reportedAvailableDate = dateFormatter.date(from: availableDate){
         return reportedAvailableDate
        }else{
           return Date()
        }
    }
    
//    func getGapDate(date:String, day:Date.DaySimplified) -> String{
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd/MM/yyyy"
//        let today = Date()
//        if let dateObj = dateFormatter.date(from: date){
//            let todayDate = dateFormatter.string(from: today)
//            if todayDate == date{
//                return date
//            }else{
//                if day == .yesterday{
//                    let newDate = Calendar.current.date(byAdding: .day, value: -1, to: dateObj) ?? dateObj
//                    return dateFormatter.string(from: newDate)
//                }else{
//                    let newDate = Calendar.current.date(byAdding: .day, value: 1, to: dateObj) ?? dateObj
//                    return dateFormatter.string(from: newDate)
//                }
//            }
//        }else{
//            return date
//        }
//    }
    
    func getDateRange(dateStart:String, dateEnd:String) -> (String,String)?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let reportedAvailableDate = getAvailableDate()
        var firstRange = ""
        var lastRange = ""
        if let startDate = dateFormatter.date(from: dateStart), let endDate = dateFormatter.date(from: dateEnd){
            if startDate.compare(reportedAvailableDate) == .orderedSame{
                firstRange = dateFormatter.string(from: reportedAvailableDate)
                let newEndDate = Calendar.current.date(byAdding: .day, value: 1, to: endDate) ?? endDate
                lastRange = dateFormatter.string(from: newEndDate)
                return (firstRange,lastRange)
            }
            else if startDate.compare(Date()) == .orderedDescending{
                let newstartDate = Calendar.current.date(byAdding: .day, value: -1, to: startDate) ?? startDate
                firstRange = dateFormatter.string(from: newstartDate)
                let newEndDate = Calendar.current.date(byAdding: .day, value: 1, to: endDate) ?? endDate
                lastRange = dateFormatter.string(from: newEndDate)
                return (firstRange,lastRange)
                
                
                
            }else{
                firstRange = dateFormatter.string(from:Date()) //"-"
                let newEndDate = Calendar.current.date(byAdding: .day, value: 1, to: endDate) ?? endDate
                lastRange = dateFormatter.string(from: newEndDate)
                return (firstRange,lastRange)
            }
        }
        return nil
    }
    
}

extension KXPreviousBookingsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXPreviousBookingsTVC") as! KXPreviousBookingsTVC
        cell.configureWith(data: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
}

extension String {
    var date : Date? {
       
        let dateformater = DateFormatter()
        dateformater.dateFormat = "dd/mm/yyyy"
        dateformater.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        return dateformater.date(from: self)
    }
}

