//
//  KXProfileEditVC.swift
//  ProfileKoloxo
//
//  Created by Appzoc on 03/05/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import SideMenu
import FloatRatingView
import OpalImagePicker
import Photos
import MessageUI
import MobileCoreServices

class KXProfileEditDocumentCVC: UICollectionViewCell {
    
    @IBOutlet var documentLabel: UILabel!
    
    @IBOutlet var removeBTNRef: StandardButton!
    
    
}

class KXPropertyAddSectionCell:UITableViewCell{
    @IBOutlet var addSectionBTNRef: StandardButton!
    
}

class KXProfileEditDocumentAddCVC:UICollectionViewCell{
    
    @IBOutlet var addBTNRef: StandardButton!
}

class KXProfileEditTVC: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var openCloseCellBTN: BaseButton!
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var documentCV: UICollectionView!
    @IBOutlet var removeBTNRef: UIButton!
    @IBOutlet var collectionRef: UICollectionView!
    
    var navigationRef:UINavigationController?
    var documentData:[KXProfileEditVC.DocData.FieldData] = []{didSet{self.collectionRef.reloadData()}}
    var documentDataLocal:[Data] = []//{didSet{self.collectionRef.reloadData()}}cellforrow
    var localDocumentLink:[URL?] = []
    var isEditingMode:Bool = false
    var indexInTable:IndexPath?
    
    override func awakeFromNib() {
        collectionRef.delegate = self
        collectionRef.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isEditingMode{
            return documentData.count + documentDataLocal.count + 1
        }else{
            return documentData.count + documentDataLocal.count
        }
    }
    
    //"/storage/app/reputed_docs/794632reputed.pdf
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellDocument = collectionView.dequeueReusableCell(withReuseIdentifier: "KXProfileEditDocumentCVC", for: indexPath) as! KXProfileEditDocumentCVC
        let cellAdd = collectionView.dequeueReusableCell(withReuseIdentifier: "KXProfileEditDocumentAddCVC", for: indexPath) as! KXProfileEditDocumentAddCVC
        cellAdd.addBTNRef.indexPath = IndexPath(row: indexPath.row, section: indexInTable!.row)
        cellDocument.removeBTNRef.indexPath = IndexPath(row: indexPath.row, section: indexInTable!.row)
        cellDocument.documentLabel.text = "Document - \(indexPath.row+1)"
        //Actual Document = "Document - \(documentData[indexPath.row].upload.components(separatedBy: "reputed_docs/")[1])"
        if isEditingMode{
            cellDocument.removeBTNRef.isHidden = false
        }else{
            cellDocument.removeBTNRef.isHidden = true
        }
        
        if indexPath.row >= documentData.count + documentDataLocal.count{
            return cellAdd
        }else{
            return cellDocument
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var storyBoard = UIStoryboard(name: "Property", bundle: nil)
        if SessionStore.current.currentDevice == .iPad {
            storyBoard = UIStoryboard(name: "Property-iPad", bundle: nil)
        }
            let vc = storyBoard.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
            if indexPath.row < documentData.count{
                vc.isLocal = false
                vc.link = documentData[indexPath.row].upload
                self.navigationRef?.pushViewController(vc, animated: true)

            }else{
                vc.isLocal = true
                
                if indexPath.row < localDocumentLink.count {
                    if let link = localDocumentLink[indexPath.row]{
                        vc.link = link.absoluteString
                        self.navigationRef?.pushViewController(vc, animated: true)

                    }
                }
            }
    }
    
}

class KXProfileEditVC: UIViewController, KXAddTitlePopupDelegate, CategorySelectionDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate, UIDocumentMenuDelegate {
   
    
    
    @IBOutlet var profileImage: BaseImageView!
    @IBOutlet var profileProgress: UIProgressView!
    @IBOutlet var progressLBL: UILabel!
    @IBOutlet var firstNameTF: UITextField!
    @IBOutlet var lastNameTF: UITextField!
    @IBOutlet var countryTF: UITextField!
    @IBOutlet var emailTF: UITextField!
    @IBOutlet var mobileTF: UITextField!
    @IBOutlet var countryCodeTF: UILabel!
    
    @IBOutlet var documentTypeTV: UITableView!
    @IBOutlet var heightTableView: NSLayoutConstraint!
    @IBOutlet var heightTopView: NSLayoutConstraint!
    @IBOutlet var heightSimpleTopView: NSLayoutConstraint!
    
    
    @IBOutlet var but_Edit_Tittle: UIButton!
    
    @IBOutlet weak var countryFlagImage: UIImageView!
    @IBOutlet weak var countrySelectionMenu: UIButton!
    
    var profileReviewType:ProfileReviewSectionType = .owner
    var profileImageHasChanged:Bool = false
    
    
    @IBOutlet var editProfilePenVIew: UIView!
    
    @IBOutlet var simpleProfileImage: BaseImageView!
    
    @IBOutlet var simpleProfileName: UILabel!
    
    @IBOutlet var simpleLocationLabel: UILabel!
    
    @IBOutlet var simpleStarRating: FloatRatingView!
    
    @IBOutlet var simpleRatingNumber: UILabel!
    
    @IBOutlet var simpleProfileEmail: UILabel!
    
    @IBOutlet var simpleMobileNumber: UILabel!
    
    @IBOutlet weak var editProfileImageBTNRef: UIButton!
    
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    
    @IBOutlet var notificationCountLBL: UILabel!
    
    //MARK: Rating View Outlets
    @IBOutlet var ratingViewHeight: NSLayoutConstraint!
    
    @IBOutlet var ratingMainImage: BaseImageView!
    @IBOutlet var ratingFirstImage: UIImageView!
    @IBOutlet var ratingSecondImage: UIImageView!
    @IBOutlet var ratingAdditionalNumbersContainer: BaseView!
    @IBOutlet var ratingUsername: UILabel!
    @IBOutlet var ratingUserLocation: UILabel!
    @IBOutlet var ratingStarVIew: FloatRatingView!
    @IBOutlet var ratingStarCount: UILabel!
    @IBOutlet var ratingDescription: UILabel!
    @IBOutlet var ratingDateLBL: UILabel!
    @IBOutlet var ratingSeeAllBTNRef: UIButton!
    @IBOutlet var reviewerStatusLBL: UILabel!
    
    
    
    fileprivate var documentTypeSource: [String] = []
    fileprivate var documentsSource: [String] = []
    fileprivate var isRotatedArrowIMG: Bool = false
    fileprivate var selectedIndexPath: IndexPath?
    fileprivate var isExpandedCell: Bool = false
    fileprivate var deselectedCell:KXProfileEditTVC?
    fileprivate var telephone_code: String = ""
    
    var passingUserID:String = "0"
    var privateUser:Bool{
        if passingUserID == Credentials.shared.userId{
            print("Private User Loading")
            return true
        }else{
            print("Public User Loading")
            return false
        }
    }
    
    fileprivate var dataFrom:[String:Any]?
    
    var localPDFLink:[Array<URL?>] = []
    
    var insertIndex:IndexPath?
    var isEditingMode:Bool = false
    var ProfileDataFrom:ProfileData? = nil
    var docData:[DocData.TopLevel] = []
    //Local Data Store
    var documentArray:[Array<Data>] = []
    var sectionTitleArray:[String] = []
    
    var displayRating:Double = 0
    
    var currentCell:IndexPath?{didSet{print("Current Cell",currentCell as Any,"Previous Cell",previousCell as Any)}}
    var previousCell:IndexPath?
    
    fileprivate var countrySource :[KXCountryListModel] = []
    fileprivate var countryIndexPath:IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPageAccordingToUser(privateViewer: self.privateUser)
        profileProgress.transform = profileProgress.transform.scaledBy(x: 1, y: 3)
        profileProgress.layer.cornerRadius = 5
        self.editProfileImageBTNRef.isUserInteractionEnabled = false
        
        if privateUser {
            self.firstNameTF.text = BaseMemmory.read(forKey: "firstName") as? String
            self.lastNameTF.text = BaseMemmory.read(forKey: "lastName") as? String
            self.countryTF.text = BaseMemmory.read(forKey: "location") as? String
            self.mobileTF.text = BaseMemmory.read(forKey: "mobile_number") as? String
            self.countryCodeTF.text = BaseMemmory.read(forKey: "tel_Code") as? String
            self.emailTF.text = BaseMemmory.read(forKey: "email") as? String

        }
        
        
        getCountryWeb()
        refreshPage()
    }
    
    
    //MARK: Change
    //MARK: Change User ID
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    //MARK: Rating See All
    @IBAction func ratingSeeAllTapped(_ sender: UIButton) {
        
        let vc = storyboardBooking?.instantiateViewController(withIdentifier: "KXRatingAndReviewsVC") as! KXRatingAndReviewsVC
        vc.passingUserID = self.passingUserID
        vc.sourceType = self.profileReviewType
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    //MARK: Set Page according to user
    func setPageAccordingToUser(privateViewer:Bool){
        if privateViewer{
            self.heightTopView.constant = 505
            self.heightSimpleTopView.constant = 0
            self.ratingViewHeight.constant = 0
        }else{
            self.heightTopView.constant = 0
            self.heightSimpleTopView.constant = 200
            self.ratingViewHeight.constant = 300
        }
    }
    
    //MARK: Refresh Page
    func refreshPage(){
        print("Refreshing Page")
        self.documentArray.removeAll()
        self.sectionTitleArray.removeAll()
        self.localPDFLink.removeAll()
        
        Webservices2.getMethod(url: "getmy_profile?user_id=\(passingUserID)&review_type=\(profileReviewType.rawValue)") { (isSuccess, Respo) in
            guard isSuccess else { return }
            do{
                let trueData = Respo["Data"] as? [String:Any]
                let decoder = JSONDecoder()
                let jsonData = try JSONSerialization.data(withJSONObject: trueData as Any, options: .prettyPrinted)
                let parsedData = try decoder.decode(DocData.self, from: jsonData)
                self.docData = parsedData.docs
                DispatchQueue.main.async{
                     self.setReviewData(data: parsedData)
                }
                
                if let testRating = parsedData.data.rating, testRating.count > 0 {
                    if let rating = Double(parsedData.data.rating![0].total_rating[0].aggregate){
                        self.displayRating = rating
                    }
                }
                let downloadedDocs = self.docData.map({$0.docs.map({$0.upload})})
                print("Downloaded docs link",downloadedDocs)
                for (indexDoc,item) in downloadedDocs.enumerated(){
                    self.documentArray.append(Array<Data>())
                    self.sectionTitleArray.append("")
                    self.localPDFLink.append(Array<URL?>())
                    for (_,_) in item.enumerated(){
                        self.localPDFLink[indexDoc].append(nil)
                    }
                }
                print("LocalPDFCount",self.localPDFLink)
                self.documentTypeTV.reloadWithCompletion {
                    DispatchQueue.main.async {
                        self.heightTableView.constant = self.documentTypeTV.contentSize.height
                    }
                }
            }catch{
                print(error)
            }
            
            self.ProfileDataFrom = ProfileData().getData(dataDict: Respo)
            self.setdata()
            
        }
    }
    
    
    func setReviewData(data:DocData){
        if data.reviews.count > 0{
            ratingMainImage.kf.setImage(with: URL(string: "\(baseImageURL + (data.reviews[0].users.image ) )"), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            ratingUsername.text = data.reviews[0].users.firstName + " " + data.reviews[0].users.lastName
            ratingUserLocation.text = data.reviews[0].users.location.name
            ratingDescription.text = data.reviews[0].comment
            ratingDateLBL.text = Date.formatDate(date: data.reviews[0].users.date)
            if data.reviews[0].ratingAverage.count > 0 {
                let rating = data.reviews[0].ratingAverage[0].aggregate
                ratingStarCount.text = rating.formatRatingToDisplay()
                ratingStarVIew.rating = (rating.getRatingInDouble() ?? 0)
                simpleStarRating.rating = rating.getRatingInDouble() ?? 0
                simpleRatingNumber.text = rating.formatRatingToDisplay()
            }
            
            if profileReviewType == .owner{
                reviewerStatusLBL.text = "Previous Tenant"
            }else{
                reviewerStatusLBL.text = "Property Owner"
            }
            
            if data.reviews.count > 3{
                ratingAdditionalNumbersContainer.isHidden = false
            }else{
                ratingAdditionalNumbersContainer.isHidden = true
            }
            if data.reviews.count > 2{
                ratingSecondImage.isHidden = false
                ratingSecondImage.kf.setImage(with: URL(string: "\(baseImageURL + (data.reviews[2].users.image) )"), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            }else{
                ratingSecondImage.isHidden = true
            }
            if data.reviews.count > 1{
                ratingFirstImage.isHidden = false
                 ratingFirstImage.kf.setImage(with: URL(string: "\(baseImageURL + (data.reviews[1].users.image) )"), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            }else{
                ratingFirstImage.isHidden = true
                ratingSeeAllBTNRef.isHidden = true
            }
        }
    }
    
    //http://koloxo-homes.dev.webcastle.in/api/deleteImage/172?slug=user_doccuments
    //MARK: Delegate Method
    func setTitle(name: String, documentData: Data, docURL:URL) {
        if let index = insertIndex{
          documentArray[index.section].append(documentData)
            localPDFLink[index.section].append(docURL)
            self.documentTypeTV.reloadWithCompletion {
                DispatchQueue.main.async {
                    self.heightTableView.constant = self.documentTypeTV.contentSize.height
                }
            }
            print("Inserting Insideeee Cell")
        }else{
            sectionTitleArray.append(name)
            localPDFLink.append([docURL])
            documentArray.append([documentData])
            self.documentTypeTV.reloadWithCompletion {
                DispatchQueue.main.async {
                    self.heightTableView.constant = self.documentTypeTV.contentSize.height
                }
            }
            print("Inserting New Section")

        }
        
        
    }
    
    
    @IBAction func emailUserBTN(_ sender: UIButton) {
        if let email = simpleProfileEmail.text{
            let emailVC = MFMailComposeViewController()
            emailVC.mailComposeDelegate = self
            print("Sending email : \(email.components(separatedBy: " ")[1])")
            emailVC.setToRecipients(["\(email.components(separatedBy: " ")[1])"])
            emailVC.setMessageBody("Hello", isHTML: false)
            if MFMailComposeViewController.canSendMail(){
                present(emailVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func callUserBTN(_ sender: UIButton) {
        if let ownerPhoneNumber = simpleMobileNumber.text{
            let alertController = UIAlertController(title: "Koloxo", message: "Do you want to call ? \n\(ownerPhoneNumber)", preferredStyle: .alert)
            let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                ownerPhoneNumber.makeACall()
                alertController.dismiss(animated: true, completion: nil)
            })
            let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
                alertController.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(yesPressed)
            alertController.addAction(noPressed)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .sent:
            print("Sent successfully")
        default:
            print("Default")
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func removeDocumentTapped(_ sender: StandardButton) {
        let downloadedCount = docData[sender.indexPath!.section].docs.count  //Check Section
        
        if sender.indexPath!.row < downloadedCount{
            //Delete document with ID at indexpath.row
            Webservices.getMethod(url: "deleteImage/\(docData[sender.indexPath!.section].docs[sender.indexPath!.row].id)?slug=user_doccuments", CompletionHandler: { (isComplete, json) in
                print(json)
                self.docData[sender.indexPath!.section].docs.remove(at: sender.indexPath!.row)
                self.localPDFLink[sender.indexPath!.section].remove(at:sender.indexPath!.row)
                self.documentTypeTV.reloadWithCompletion {
                    DispatchQueue.main.async {
                        self.heightTableView.constant = self.documentTypeTV.contentSize.height + 5
                    }
                }
            })
            debugPrint("ArrayCount",documentArray.count,":araadd:",documentArray,":ADADAD:",docData[sender.indexPath!.section].docs,"fdfd:",docData[sender.indexPath!.section].docs.count)
            print("remove from back end",docData[sender.indexPath!.section].docs.count)
            print("remove from back end-Row",docData[sender.indexPath!.row].docs.count)

        }else{
            let newIndex = sender.indexPath!.row - downloadedCount
            documentArray[sender.indexPath!.section].remove(at:newIndex)
            self.localPDFLink[sender.indexPath!.section].remove(at:sender.indexPath!.row)
            self.documentTypeTV.reloadData()
            debugPrint("ArrayCount",documentArray.count,":araadd:",documentArray,":ADADAD:",documentArray[sender.indexPath!.section],"fdfd:",documentArray[sender.indexPath!.section].count)
            print("removingCurrent-Row",documentArray[sender.indexPath!.row].count)

        }

    }
    
    //MARK: Add Document to section
    @IBAction func addDocumentToSection(_ sender: StandardButton) {
        if isEditingMode{
            insertIndex = sender.indexPath
            
//            let popVC = storyboardSearch!.instantiateViewController(withIdentifier: "KXAddTitilePopupViewController") as! KXAddTitilePopupViewController
//            popVC.delegate = self
//            self.present(popVC, animated: true, completion: nil)
            getDataFromICloud()
        }
    }
    
    func getDataFromICloud(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        present(importMenu, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        do{
            let data = try Data(contentsOf: url)
            if let index = insertIndex{
                documentArray[index.section].append(data)
                localPDFLink[index.section].append(url)
                self.documentTypeTV.reloadWithCompletion {
                    DispatchQueue.main.async {
                        self.heightTableView.constant = self.documentTypeTV.contentSize.height
                    }
                }
                print("Inserting Insideeee without delegate- Cell")
            }
        }catch{
            print(error)
        }
    }

    @IBAction func removeEntireSection(_ sender: UIButton) {
        
        if isEditingMode{
//            docData.remove(at: sender.tag)
//            documentTypeTV.reloadWithCompletion {
//                 self.heightTableView.constant = self.documentTypeTV.contentSize.height
//            }
        }
    }
    
    @IBAction func addNewSection(_ sender: StandardButton) {
        if isEditingMode{
            insertIndex = nil
            let popVC = storyboardSearch!.instantiateViewController(withIdentifier: "KXAddTitilePopupViewController") as! KXAddTitilePopupViewController
            popVC.delegate = self
            self.present(popVC, animated: true, completion: nil)
        }
    }
    
    //MARK: Get Country Data
    
    private func getCountryWeb(){
        getCountryListWeb { (list) in
            self.countrySource = list
            if self.ProfileDataFrom != nil{
                self.updateCountryReceivedData()
            }
        }
    }

    
    
    func selectedValue(index: IndexPath) {
        let country = countrySource[index.row]
        countryTF.text = country.name
        countryCodeTF.text = country.telephone_code
        countryIndexPath = index
        if let imageURL = URL(string: country.flag_image_128){
            countryFlagImage.kf.setImage(with: imageURL, placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }
    }
    
    @IBAction func countryListDropDown(_ sender: UIButton){
        let categorySelectionViewController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        categorySelectionViewController.setTitle(title: "Select Country")
        categorySelectionViewController.tableSource = countrySource.map({$0.name})
        categorySelectionViewController.delegate = self
        categorySelectionViewController.selectedIndexPath = countryIndexPath
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    //MARK: Set Data After Call
    func setdata()
    {
        if let ProfileDataUnwrapped = ProfileDataFrom{
            print(ProfileDataUnwrapped.firstName)
            self.firstNameTF.text = ProfileDataUnwrapped.firstName
            self.lastNameTF.text = ProfileDataUnwrapped.lastName
            self.mobileTF.text = ProfileDataUnwrapped.mobileNumber
            self.mobileTF.text = ProfileDataUnwrapped.mobileNumber
            
            if privateUser {
                Credentials.shared.firstName = ProfileDataUnwrapped.firstName
                Credentials.shared.lastName = ProfileDataUnwrapped.lastName
                Credentials.shared.mobile = ProfileDataUnwrapped.mobileNumber
                Credentials.shared.emailId = ProfileDataUnwrapped.email
                Credentials.shared.profileImage = ProfileDataUnwrapped.image

            }
            
            
            if !countrySource.isEmpty {
                let flagLink = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].flag_image_128
                self.countryFlagImage.kf.setImage(with: URL(string: "\(flagLink)"), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
                self.countryTF.text = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].name//BaseMemmory.read(forKey: "location") as? String
                self.countryCodeTF.text = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].telephone_code //BaseMemmory.read(forKey: "tel_Code") as? String
                self.simpleLocationLabel.text = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].name
                self.telephone_code = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].telephone_code
                var countryRow = 0
                for (index,item) in countrySource.enumerated(){
                    if item.id == ProfileDataUnwrapped.countryResidence{
                        countryRow = index
                    }
                }
                
                self.countryIndexPath = IndexPath(row: countryRow, section: 0)
                
                if privateUser {
                    BaseMemmory.write(object: self.countryTF.text ?? "", forKey: "location")
                }
                
             //   Credentials.shared.country = self.countryTF.text ?? ""
            }
            self.emailTF.text = ProfileDataUnwrapped.email
            self.documentTypeTV.reloadData()
            self.but_Edit_Tittle.setTitle("Edit", for: .normal)
            
            self.profileImage.kf.setImage(with: URL(string:"\(baseImageURL)\(ProfileDataUnwrapped.image)"), placeholder: #imageLiteral(resourceName: "userPlaceholder"),
                                          options: [.requestModifier(getImageAuthorization())],
                                          progressBlock: nil,
                                          completionHandler: nil)
            
            if ProfileDataUnwrapped.docList.isEmpty {
                self.progressLBL.text = "75% Complete"
                self.profileProgress.progress = 0.75
            }else{
                self.progressLBL.text = "100% Complete"
                self.profileProgress.progress = 1
            }
            
            
            
            let keyAttribute : [NSAttributedStringKey:Any] = [NSAttributedStringKey.font : UIFont(name: "Montserrat", size: 10) as Any, NSAttributedStringKey.foregroundColor : UIColor.black]
            
            let valueAttribute : [NSAttributedStringKey:Any] = [NSAttributedStringKey.font : UIFont(name: "Montserrat", size: 11) as Any, NSAttributedStringKey.foregroundColor : UIColor.blue]
            
            
            let attributDescription = NSMutableAttributedString()
            attributDescription.append(NSAttributedString(string: "Email: ", attributes: keyAttribute))
            attributDescription.append(NSAttributedString(string: ProfileDataUnwrapped.email, attributes: valueAttribute))
            
            let attributDescription2 = NSMutableAttributedString()
            attributDescription2.append(NSAttributedString(string: "Mobile: ", attributes: keyAttribute))
            
            if self.telephone_code != "" {
                attributDescription2.append(NSAttributedString(string: self.telephone_code + ProfileDataUnwrapped.mobileNumber, attributes: valueAttribute))

            }else {
                attributDescription2.append(NSAttributedString(string: ProfileDataUnwrapped.mobileNumber, attributes: valueAttribute))

            }
            
            
            self.simpleProfileEmail.attributedText = attributDescription
            self.simpleMobileNumber.attributedText = attributDescription2
            
            
            //Simple Profile
            self.simpleProfileName.text = "\(ProfileDataUnwrapped.firstName) \(ProfileDataUnwrapped.lastName)"
            self.simpleProfileImage.kf.setImage(with: URL(string:"\(baseImageURL)\(ProfileDataUnwrapped.image)"), placeholder: #imageLiteral(resourceName: "userPlaceholder"),
                                                options: [.requestModifier(getImageAuthorization())],
                                                progressBlock: nil,
                                                completionHandler: nil)
            self.simpleRatingNumber.text = "\(displayRating)"
            self.simpleStarRating.rating = displayRating
            
        }
         heightTableView.constant = documentTypeTV.contentSize.height
    }
    
    func updateCountryReceivedData(){
        if !countrySource.isEmpty, let ProfileDataUnwrapped = ProfileDataFrom{
            let flagLink = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].flag_image_128
            self.countryFlagImage.kf.setImage(with: URL(string: "\(flagLink)"), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            self.countryTF.text = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].name//BaseMemmory.read(forKey: "location") as? String
            self.countryCodeTF.text = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].telephone_code //BaseMemmory.read(forKey: "tel_Code") as? String
            self.simpleLocationLabel.text = self.countrySource.filter({$0.id == ProfileDataUnwrapped.countryResidence})[0].name
            var countryRow = 0
            for (index,item) in countrySource.enumerated(){
                if item.id == ProfileDataUnwrapped.countryResidence{
                    countryRow = index
                }
            }
            self.countryIndexPath = IndexPath(row: countryRow, section: 0)
        }
    }
    
    //MARK: Edit - Update Button
    @IBAction func but_edit_profile(_ sender: UIButton) {
        
        if sender.titleLabel!.text == "Edit"{
            firstNameTF.isUserInteractionEnabled = true
            lastNameTF.isUserInteractionEnabled = true
            countryTF.isUserInteractionEnabled = true
           // emailTF.isUserInteractionEnabled = true
            emailTF.isEnabled = false
            mobileTF.isUserInteractionEnabled = true
            self.editProfileImageBTNRef.isUserInteractionEnabled = true
            countrySelectionMenu.isUserInteractionEnabled = true
            self.isEditingMode = true
            self.editProfilePenVIew.isHidden = false
            self.documentTypeTV.reloadWithCompletion {
                DispatchQueue.main.async {
                    self.heightTableView.constant = self.documentTypeTV.contentSize.height + 5
                    self.view.setNeedsLayout()
                }
            }
            self.but_Edit_Tittle.layer.borderWidth = 2
            self.but_Edit_Tittle.layer.borderColor = UIColor.availableGreen.cgColor
            self.but_Edit_Tittle.setTitle("Update", for: .normal)
        }else{
            firstNameTF.isUserInteractionEnabled = false
            lastNameTF.isUserInteractionEnabled = false
            countryTF.isUserInteractionEnabled = false
            self.editProfilePenVIew.isHidden = true
           // emailTF.isUserInteractionEnabled = false
            emailTF.isEnabled = true
            mobileTF.isUserInteractionEnabled = false
            countrySelectionMenu.isUserInteractionEnabled = false
            self.editProfileImageBTNRef.isUserInteractionEnabled = false
            if profileImageHasChanged{
                uploadUserProfileImage()
            }
            postProfileData()
            self.isEditingMode = false
            self.documentTypeTV.reloadWithCompletion {
                DispatchQueue.main.async {
                    self.heightTableView.constant = self.documentTypeTV.contentSize.height
                    self.view.setNeedsLayout()
                }
            }
            self.but_Edit_Tittle.layer.borderWidth = 0
            self.but_Edit_Tittle.layer.borderColor = UIColor.lightGray.cgColor
            self.but_Edit_Tittle.setTitle("Edit", for: .normal)
        }
        
    }
    
    @IBAction func editProfileImage(_ sender: UIButton) {
        
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        presentOpalImagePickerController(imagePicker, animated: true,
                                         select: { (assets) in
                                            for item in assets{
                                                let OPImage = self.getAssetThumbnail(asset: item)
                                                print(OPImage,"image")
                                                self.profileImageHasChanged = true
                                                DispatchQueue.main.async {
                                                    self.profileImage.image = OPImage
                                                }
                                            }
                                            self.dismiss(animated: true, completion: {
                                                
                                            })
        }, cancel: {
            //self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    func uploadUserProfileImage(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
//        var param:[String:Any] = ["id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime
//        ]
        let param:[String:Any] = ["id":"\(passingUserID)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime
        ]
        
        var dataParam:[String:Data] = [:]
        if let proImage = self.profileImage.image{
            dataParam.updateValue(UIImageJPEGRepresentation(proImage, 1)!, forKey: "image")
        }
        print("Data Param",dataParam)
        Webservices.postMethodMultiPartImage(url: "updateUserimage",
                                             parameter:param,
                                             dataParameter:dataParam ) { (isCompleted, json) in
                                                print(json)
                                                if isCompleted {
                                                    self.profileImageHasChanged = false
                                                    let data = json["Data"] as? [String:Any]
                                                    if let image = data!["image"] as? String{
                                                        Credentials.shared.profileImage = image
                                                    }
                                                }else{
                                                    print("Call Failed")
                                                }
        }
    }
    
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 500, height: 500), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger(_ sender: Any) {
        showSideMenu()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        heightTableView.constant = documentTypeTV.contentSize.height
    }
    
    //MARK: Tab Bar Actions
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    //MARK: Open Close Header Cell
    @IBAction func openCloseCellTapped(_ sender: BaseButton) {
        let indexPath = sender.indexPath!
        
        if let current = currentCell{
            previousCell = current
        }
        currentCell = sender.indexPath
        
        self.changeCellPlusIcon()
        guard let selectedCell = documentTypeTV.cellForRow(at: indexPath) as? KXProfileEditTVC else { return }
        
        if selectedIndexPath == indexPath {
            isExpandedCell = isExpandedCell ? false : true
        }else if selectedIndexPath == nil {
            isExpandedCell = true
        }
        selectedIndexPath = indexPath
        documentTypeTV.beginUpdates()
        changeCellImage(of: selectedCell)
        documentTypeTV.endUpdates()
        heightTableView.constant = documentTypeTV.contentSize.height
    }
    
    //MARK: Validation
    func validate()->Bool{
        if let fName = firstNameTF.text, fName.isNotEmpty, let lName = lastNameTF.text, lName.isNotEmpty, let country = countryTF.text, country.isNotEmpty, let email = emailTF.text, email.isNotEmpty, let mobile = mobileTF.text, mobile.isNotEmpty {

            if let email = emailTF.text, BaseValidator.isValid(email: email){
                if let mobile = mobileTF.text, BaseValidator.isValid(digits: mobile), mobile.count >= mobileLimit{
                    return true
                    
                    /*
                    if documentArray.count > 0{
                        return true
                    }else{
                        showBanner(message: "Atleast one verification document is mandatory.")
                        return false
                    }
                    */
                    
                }else{
                    showBanner(message: "Enter a valid Mobile Number")
                    return false
                }
            }else{
                showBanner(message: "Enter a valid E-mail ID")
                return false
            }
            
        }else{
//            showBanner(message: "All fields are mandatory")
            if !BaseValidator.isNotEmpty(string: firstNameTF.text) {
                showBanner(message: "Please enter First Name")
                return false

            }
            
            if !BaseValidator.isNotEmpty(string: lastNameTF.text) {
                showBanner(message: "Please enter Last Name")
                return false

            }
            
            if !BaseValidator.isNotEmpty(string: countryTF.text) {
                showBanner(message: "Please choose a Country")
                return false

            }

            if !BaseValidator.isNotEmpty(string: emailTF.text) {
                showBanner(message: "Please enter E-mail ID")
                return false

            }
            
            if !BaseValidator.isNotEmpty(string: mobileTF.text) {
                showBanner(message: "Please enter Mobile Number")
                return false

            }
            
            return false
        }
        
    }
    
    //MARK:- Post Edit Data
    func postProfileData() {
        
        guard validate() else { return }
        
        var postDataComplete:Bool = false
        var postComplete:Bool = false
        
        var param:[String:String] = ["user_id":"\(passingUserID)"]
        var dataParam:[String:Data] = [:]
        var insertPoint = 0
        var docCount = 0
        for (index,item) in documentArray.enumerated(){
            if item.count > 0 {
                docCount += 1
                if index < docData.count{
                    param.updateValue("\(docData[index].title)", forKey: "document[\(insertPoint)][0]")
                    for (subIndex,doc) in item.enumerated(){
                        dataParam.updateValue(doc, forKey: "document[\(insertPoint)][\(subIndex+1)]")
                        
                    }
                    insertPoint += 1
                }
                else if index-docData.count < sectionTitleArray.count{
                    param.updateValue("\(sectionTitleArray[index])", forKey: "document[\(insertPoint)][0]")
                    for (subIndex,doc) in item.enumerated(){
                        dataParam.updateValue(doc, forKey: "document[\(insertPoint)][\(subIndex+1)]")
                    }
                    insertPoint += 1
                }
            }
            
        }
        
        print("Title Param",param)
        print("Data Param",dataParam)
        var isReputedCall: Bool = false
        if docCount > 0 {
            isReputedCall = true
            print("Reputed Calllllllllllllllll")
            Webservices.postMethodMultiPartData(url: "add_reputed_profile", parameter: param, dataParameter: dataParam) { (isComplete, json) in
                print(json)
                if isComplete{
                    postDataComplete = true
                    if postComplete{
                        self.refreshPage()
                    }
                }
            }
        }else {
            print("notttttttttt reputeddddddddddd")
            isReputedCall = false
        }
        
        /*
         updateUserprofile:
         'id','first_name','last_name' ,'country_residence','email' ,'mobile_number' ,
         METHOD : POST */
        let detailsParam = ["id":"\(passingUserID)","email":"\(emailTF.text!)",
            "first_name":"\(firstNameTF.text!)",
            "last_name":"\(lastNameTF.text!)",
            "mobile_number":"\(mobileTF.text!)",
            "country_residence":"\(countrySource[countryIndexPath!.row].id)"]
        
        Webservices.postMethod(url: "updateUserprofile", parameter: detailsParam) { (isCompleted, json) in
            if isCompleted{
                postComplete = true
                if isReputedCall {
                    if postDataComplete{
                        self.refreshPage()
                    }
                }else {
                    self.refreshPage()

                }
            }
        }
        
    }
    
    
    //MARK: Change Plus Sign
    private func changeCellPlusIcon(){
        if let current = self.currentCell{
            DispatchQueue.main.async {
                
                if let cell = self.documentTypeTV.cellForRow(at: current) as? KXProfileEditTVC{
                    if cell.removeBTNRef.image(for: UIControlState.normal) == UIImage(named: "minus-circular-button"){
                        cell.removeBTNRef.setImage(UIImage(named: "add-circular-outlined-button"), for: [])
                    }else{
                        cell.removeBTNRef.setImage(UIImage(named: "minus-circular-button"), for: [])
                    }
                
                    if let previous = self.previousCell{
                        
                        if let prevCell = self.documentTypeTV.cellForRow(at: previous) as? KXProfileEditTVC{
                            
                            if previous.row == current.row{
                                
                            }else{
                                if prevCell.removeBTNRef.image(for: UIControlState.normal) == UIImage(named: "minus-circular-button"){
                                    prevCell.removeBTNRef.setImage(UIImage(named: "add-circular-outlined-button"), for: [])
                                }else{
                                    //prevCell.removeBTNRef.setImage(UIImage(named: "minus-circular-button"), for: [])
                                }
                            }
                        }
                    }
                }
            }
        }
    }
        

    
    private func changeCellImage(of selectedCell: KXProfileEditTVC){
        //minus (1), add-circular-outlined-button
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                if self.deselectedCell == nil {
                    /// tapping on the cell first time
                    self.isRotatedArrowIMG = true
                }else if self.deselectedCell == selectedCell {
                    /// tapping on already selected cell
                    if self.isRotatedArrowIMG {
                        self.isRotatedArrowIMG = false
                    }else{
                        self.isRotatedArrowIMG = true
                    }
                }else{
                    /// tapping on other cell not already selected cell
                    guard let _ = self.deselectedCell else { self.isRotatedArrowIMG = true; return }
                    self.isRotatedArrowIMG = true
                }
                self.deselectedCell = selectedCell
            })
        }
        
    }
    
    @IBAction func Home_Tab(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func Tab(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension KXProfileEditVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isEditingMode{
            return documentArray.count + 1
        }else{
            return documentArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXProfileEditTVC") as! KXProfileEditTVC
        let addCell = tableView.dequeueReusableCell(withIdentifier: "KXPropertyAddSectionCell") as! KXPropertyAddSectionCell
       // cell.titleLBL.text = self.ProfileDataFrom.docList[indexPath.row].title
        cell.isEditingMode = self.isEditingMode
        cell.indexInTable = indexPath
        cell.navigationRef = self.navigationController
        if indexPath.row < documentArray.count{
            cell.localDocumentLink = self.localPDFLink[indexPath.row]
        }
        print("Passed Editing mode",self.isEditingMode)
        if !isEditingMode{
            
                if indexPath.row < docData.count{
                    cell.openCloseCellBTN.indexPath = indexPath
                    cell.titleLBL.text = docData[indexPath.row].title
                    cell.documentData = docData[indexPath.row].docs
                    cell.documentDataLocal = self.documentArray[indexPath.row]
                    cell.removeBTNRef.tag = indexPath.section
                    cell.collectionRef.reloadData()
                    return cell
                
                }else{
                    cell.openCloseCellBTN.indexPath = indexPath
                    cell.titleLBL.text = self.sectionTitleArray[indexPath.row]
                    cell.documentDataLocal = self.documentArray[indexPath.row]
                    cell.removeBTNRef.tag = indexPath.section
                    cell.collectionRef.reloadData()
                    return cell
                }

            
        }else{
            if indexPath.row >= documentArray.count{
                    return addCell
            }else{
                if indexPath.row < docData.count{
                    print("frontENd")
                    cell.openCloseCellBTN.indexPath = indexPath
                    cell.titleLBL.text = docData[indexPath.row].title
                    cell.documentData = docData[indexPath.row].docs
                    cell.documentDataLocal = self.documentArray[indexPath.row]
                    cell.removeBTNRef.tag = indexPath.section
                    cell.collectionRef.reloadData()
                    return cell
                }else{
                    print("backend")
                    cell.openCloseCellBTN.indexPath = indexPath
                    cell.titleLBL.text = self.sectionTitleArray[indexPath.row]
                    cell.documentDataLocal = self.documentArray[indexPath.row]
                    cell.removeBTNRef.tag = indexPath.section
                    cell.collectionRef.reloadData()
                    return cell
                }
            }
        }
        
    }
    
}

extension KXProfileEditVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < documentArray.count{
            if selectedIndexPath == indexPath && !isExpandedCell {selectedIndexPath = nil}
            return selectedIndexPath == indexPath ? (isExpandedCell ? 225 :  45) :  45 //45
        }else{
            return 52
        }
    }
}






extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

/*
 docs =     (
     {
     docs =             (
         {
         "created_at" = "2018-06-20 07:19:48";
         "document_title" = "Bank Statement";
         id = 228;
         upload = "/storage/app/reputed_docs/794632reputed.pdf";
         "user_id" = 193;
         }
     );
     title = "Bank Statement";
     }
 );

 
 */

extension KXProfileEditVC{
    struct DocData:Codable{
        let docs:[TopLevel]
        let data:PersonalData
        let reviews:[ReviewData]
        let total_average:[DataAverage]
        
        
        struct PersonalData: Codable{
            let rating:[RatingData]?
            
            struct RatingData:Codable{
                let total_rating:[Rating]
                
                struct Rating: Codable {
                    let aggregate: String
                    let rating_id: Int
                }
            }
        }
        
        struct TopLevel:Codable{
            let title:String
            var docs:[FieldData]
        }
        
        struct FieldData:Codable {
            let id:Int
            let documentTitle:String
            let upload:String
            
            private enum CodingKeys: String, CodingKey{
                case id, documentTitle = "document_title", upload
            }
        }
        
        
        struct ReviewData:Codable{
            let comment:String
            let users:ReviewUser
            let ratingAverage:[UserRating]
            let date:String
            
            private enum CodingKeys: String, CodingKey{
                case comment = "comment", users = "users", ratingAverage = "rating_average", date = "created_at"
            }
            
            struct ReviewUser:Codable{
                let firstName:String
                let lastName:String
                let image:String
                let location:Country
                let date:String
                
                struct Country:Codable{
                    let name:String
                }
                
                private enum CodingKeys:String, CodingKey{
                    case firstName = "first_name", lastName = "last_name", date = "created_at", location = "user_country", image
                }
            }
            
            struct UserRating:Codable{
                let aggregate:String
            }
        }
        
        struct DataAverage:Codable{
            let totalAverage:String
            
            private enum CodingKeys: String, CodingKey{
                case totalAverage = "total_average"
            }
        }
    }
}

