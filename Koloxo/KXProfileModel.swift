//
//  KXProfileModel.swift
//  Koloxo
//
//  Created by Appzoc on 16/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

class ProfileData {
    var errorCode = "1"
    
    var message = ""
    var id = 0
    var userID = 0
    var documentTitle = ""
    var upload = ""
    var createdAt = ""
    
    var firstName = ""
    var lastName = ""
    var email = ""
    var image = ""
    var mobileNumber = ""
    var countryResidence = 0
    var reputedProfile:String? = ""
    var country_code: String = ""
    
    var docList = [DataDoc]()
    
    func getData(dataDict:[String:Any]) -> ProfileData
    {
        
        self.errorCode = dataDict["ErrorCode"] as! String
        self.message = dataDict["Message"] as! String
        
        let Data1 = dataDict["Data"] as! [String:Any]
        let Data2 = Data1["data"] as! [String:Any]
        
        
        
        self.id = Data2["id"] as! Int
        self.firstName = Data2["first_name"] as! String
        self.lastName = Data2["last_name"] as! String
        self.email = Data2["email"] as! String
        self.image = Data2["image"] as! String
        self.mobileNumber = Data2["mobile_number"] as! String
        self.countryResidence = Data2["country_residence"] as! Int
        self.createdAt = Data2["created_at"] as! String
        self.reputedProfile = Data2["reputed_profile"] as? String
        self.country_code = Data2["country_code"] as? String ?? ""
        
        let docs = Data1["docs"] as! [Any]
        
        for items in docs
        {
            
            let d1 = DataDoc(dataDict: items as! [String : Any])
            self.docList.append(d1)
            
            
        }
        
        return self
    }
}





class DataDoc {
    let title: String
    var docs = [DocDoc]()
    
    init(dataDict:[String:Any])
    {
        
        self.title = dataDict["title"] as! String
        let docs =  dataDict["docs"] as! [Any]
        for items in docs
        {
            let idNameTemp = DocDoc(dataDict: items as! [String : Any])
            self.docs.append(idNameTemp)
        }
    }
}

class DocDoc {
    var id = 0
    var userID = 0
    var documentTitle = ""
    var upload = ""
    var createdAt = ""
    
    init(dataDict:[String:Any]) {
        self.id = dataDict["id"] as! Int
        self.userID = dataDict["user_id"] as! Int
        self.documentTitle = dataDict["document_title"] as! String
        self.upload = dataDict["upload"] as! String
        self.createdAt = dataDict["created_at"] as! String
    }
}

struct MyReviewsModel{
    /*
     id: 1,
     property_id: 151,
     from_user_id: 13,
     to_user_id: 13,
     comment: "jkkjhkjh",
     status: 1,
     type: 0,
     owner_status: 1,
     created_at: "2018-06-05 17:28:02",
     updated_at: "2018-06-05 17:28:02",
     deleted_at: null,
     users: {
     id: 13,
     first_name: "abc",
     last_name: "def",
     image: "images/user_image/VvK7B7NnRrx0ABWNsyVyp6mnWF2OMjWGdaIQtzI7.png",
     mobile_number: "974608200",
     email: "a@abacbbd.com",
     password: "$2y$10$VQqFEtZPoPPF/Q72PvjITebJNS2pK/amzkaVHB02GMX//Q2QhcUHm",
     country_residence: 1,
     status: 1,
     date: null,
     type: 2,
     facebook_id: null,
     hash: "$2y$10$zRuvsA8HvnGhQJ2Rw7tO8e16Orx7gSYXs/dbXyyhQOQ/DhJdln0WW",
     reputed_profile: "1",
     facebook_login: null,
     created_at: "2018-05-17 08:24:27",
     updated_at: "2018-07-09 06:21:50",
     deleted_at: null
     }
 */
    
    let comment:String
    let firstName:String
    let lastName:String
    let userImage:String
    let userLocation:String
    let date:String
    let rating:Int

}

