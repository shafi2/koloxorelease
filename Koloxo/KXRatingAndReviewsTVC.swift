//
//  KXRatingAndReviewsTVC.swift
//  Koloxo
//
//  Created by Appzoc on 16/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import UIKit
import FloatRatingView

class KXRatingAndReviewsTVC: UITableViewCell {
    
    
    @IBOutlet var ratingProfileImageView: BaseImageView!
    @IBOutlet var ratingUserNameLBL: UILabel!
    @IBOutlet var ratingLocationLBL: UILabel!
    @IBOutlet var ratingDateLBL: UILabel!
    @IBOutlet var ratingStarView: FloatRatingView!
    @IBOutlet var ratingNumberLBL: UILabel!
    @IBOutlet var tenantStatusLBL: UILabel!
    @IBOutlet var ratingDescriptionLBL: UILabel!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureWith(data:KXProfileEditVC.DocData.ReviewData, state:ProfileReviewSectionType){
        ratingProfileImageView.kf.setImage(with: URL(string: "\(baseImageURL + (data.users.image) )"), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        ratingUserNameLBL.text = data.users.firstName + " " + data.users.lastName
        ratingLocationLBL.text = data.users.location.name
        ratingDescriptionLBL.text = data.comment
        ratingDateLBL.text = Date.formatDate(date: data.date)
        if data.ratingAverage.count > 0 {
            let rating = data.ratingAverage[0].aggregate
            ratingNumberLBL.text = rating.formatRatingToDisplay()
            ratingStarView.rating = (rating.getRatingInDouble() ?? 0)
        }
        if state == .owner{
            tenantStatusLBL.text = "Previous Tenant"
        }else{
            tenantStatusLBL.text = "Property Owner"
        }
    }

}
