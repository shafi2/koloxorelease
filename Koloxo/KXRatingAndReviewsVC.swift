//
//  KXRatingAndReviewsVC.swift
//  Koloxo
//
//  Created by Appzoc on 16/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXRatingAndReviewsVC: UIViewController {

    
    @IBOutlet var tableRef: UITableView!
    var passingUserID:String = ""
    var sourceData:ReviewDataModel?
    var sourceType:ProfileReviewSectionType = .owner
    var dataSource:[KXProfileEditVC.DocData.ReviewData] = []
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpWebCall()
        // Do any additional setup after loading the view.
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    
    @IBAction func backBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton)
    {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton)
    {
        
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton)
    {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton)
    {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        isLoadedTabSearchChatNotify = .chat
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton)
    {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // to setup chatcount and notification count
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    func setUpWebCall(){
        Webservices.getMethodWith(url: "getAllReviews?user_id=\(passingUserID)", parameter: nil) { (isComplete, json) in
            if isComplete{
                do{
                    let trueData = json["Data"] as? [String:Any]
                    let decoder = JSONDecoder()
                    let jsonData = try JSONSerialization.data(withJSONObject: trueData as Any, options: .prettyPrinted)
                    let parsedData = try decoder.decode(ReviewDataModel.self, from: jsonData)
                    self.sourceData = parsedData
                    self.setUpDataSource()
                    
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func setUpDataSource(){
        guard sourceData != nil else {return}
        if sourceType == .owner{
            dataSource = sourceData!.ownerReview
        }else{
            dataSource = sourceData!.tenantReview
        }
        DispatchQueue.main.async {
            self.tableRef.reloadData()
        }
    }
    
    struct ReviewDataModel:Codable{
        let ownerReview:[KXProfileEditVC.DocData.ReviewData]
        let tenantReview:[KXProfileEditVC.DocData.ReviewData]
        
        private enum CodingKeys: String, CodingKey{
            case ownerReview = "reviews_as_owner", tenantReview = "reviews_as_tenant"
        }
    }
    

}

extension KXRatingAndReviewsVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXRatingAndReviewsTVC") as!  KXRatingAndReviewsTVC
        cell.configureWith(data: dataSource[indexPath.row],state: sourceType)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 219
    }
    
}
