//
//  KXRegisterProfileViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 12/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import Kingfisher

class KXRegisterProfileViewController: UIViewController {
    
    @IBOutlet var lbl_FName: UILabel!
    @IBOutlet var lbl_LName: UILabel!
    @IBOutlet var lbl_Country: UILabel!
    @IBOutlet var lbl_Email: UILabel!
    @IBOutlet var lbl_MobileNumber: UILabel!
    
    
    @IBOutlet var img_countryFlag: UIImageView!
    @IBOutlet var img_UserImage: RoundedImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.lbl_FName.text = BaseMemmory.read(forKey: "firstName") as? String
        self.lbl_LName.text = BaseMemmory.read(forKey: "lastName") as? String
        self.lbl_Country.text = BaseMemmory.read(forKey: "location") as? String
        self.lbl_MobileNumber.text = BaseMemmory.read(forKey: "mobile_number") as? String
        self.lbl_Email.text = Credentials.shared.emailId
        let imag = BaseMemmory.read(forKey: "countryFlag") as? URL
        
        self.img_countryFlag.kf.setImage(with: imag)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Complete_Profile(_ sender: Any) {
        let vc = self.storyboardProperty!.instantiateViewController(withIdentifier: "KXReputedProfileTrueController") as! KXReputedProfileTrueController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func Back_Button(_ sender: Any) {
        let vc = self.storyboardMain?.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Humberger_Menu(_ sender: Any) {
        showSideMenu()
        
    }
    
    
    
    
    
    
    
    @IBAction func Tab_Menu(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Tab_Buttons(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

