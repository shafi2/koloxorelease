//
//  KXReputedProfile.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 24/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
class buttoncell: UITableViewCell
{
    
}

class KXFurnitureDetails: UIViewController,UITableViewDataSource,UITableViewDelegate
{
   
    
    @IBOutlet var Reputed_table: UITableView!
   
//
      private var deselectedCell:KXReputedTV?
    private var isRotatedArrowIMG: Bool = false


//
      private var selectedIndexPath: IndexPath?
    
      private var isExpandedCell: Bool = false
//    private let slotName = "Slot "
      override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func Next_Button(_ sender: Any) {
        let homeScreen = storyboardProperty!.instantiateViewController(withIdentifier: "KXAddplacemarker") as! KXAddplacemarker
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headerArray.count
        
    }
   
    var headerArray = ["Bedroom","Kitchen","Guest Room","Balcony","Other Furniture"]
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! KXReputedTV
        cell.headerLBL.text = headerArray[indexPath.row]
        cell.openCloseCellBTN.indexPath = indexPath
         cell.openCloseCellBTN.addTarget(self, action:
            #selector(openCloseCellBTNTapped(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
     
      if selectedIndexPath == indexPath && !isExpandedCell {selectedIndexPath = nil}
      //print(selectedIndexPath == indexPath ? (isExpandedCell ? 285 :  74) :  74)
        return selectedIndexPath == indexPath ? (isExpandedCell ? 386 : 80) : 80
    }
    
    @objc func openCloseCellBTNTapped(_ sender: IPButton) {
        let indexPath = sender.indexPath
        UIView.animate(withDuration: 2.0, animations: {
          
        })
        guard let selectedCell = Reputed_table.cellForRow(at: indexPath!) as? KXReputedTV else { return }
//        guard let sele
        
        if selectedIndexPath == indexPath {
            isExpandedCell = isExpandedCell ? false : true
        }else if selectedIndexPath == nil {
            isExpandedCell = true
        }
        selectedIndexPath = indexPath
       // Reputed_table.reloadData()
        //updateContent(of: selectedCell, byIndexPath: selectedIndexPath!)
       Reputed_table.beginUpdates()
        self.rotateArrowIMG(of: selectedCell)
        Reputed_table.endUpdates()
        
    }
    
    
    private func rotateArrowIMG(of selectedCell: KXReputedTV){
        let towardsDown = CGFloat(Double.pi/2)
        let towardsRight = CGFloat(-Double.pi/2)
        
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.3, animations: {
                
                if self.deselectedCell == nil {
                    /// tapping on the cell first time
                    selectedCell.imageview.transform = selectedCell.imageview.transform.rotated(by: towardsDown)
                    self.isRotatedArrowIMG = true
                }else if self.deselectedCell == selectedCell {
                    /// tapping on already selected cell
                    if self.isRotatedArrowIMG {
                        selectedCell.imageview.transform = selectedCell.imageview.transform.rotated(by: towardsRight)
                        self.isRotatedArrowIMG = false
                    }else{
                        selectedCell.imageview.transform = selectedCell.imageview.transform.rotated(by: towardsDown)
                        self.isRotatedArrowIMG = true
                    }
                }else{
                    /// tapping on other cell not already selected cell
                    selectedCell.imageview.transform = selectedCell.imageview.transform.rotated(by: towardsDown)
                    guard let dCell = self.deselectedCell else { self.isRotatedArrowIMG = true; return }
                    if self.isRotatedArrowIMG {
                        dCell.imageview.transform = dCell.imageview.transform.rotated(by: towardsRight)
                    }
                    self.isRotatedArrowIMG = true
                }
                self.deselectedCell = selectedCell
            })
        }
    }
    
    
    @IBAction func Back_button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    
    }
    @IBAction func Humberger_Menu(_ sender: Any) {
        showSideMenu()
//        let sideMenuVC = storyboardMain!.instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
    @IBAction func Verify_Profile(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXDashBoardScreenViewController") as! KXDashBoardScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    @IBAction func Tab_Home(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
      
    }
    @IBAction func Tab_Buttons(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

