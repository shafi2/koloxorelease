//
//  KXSearchResultViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 12/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import GoogleMaps
import SideMenu
import GooglePlaces
import MapKit
import Alamofire

class searchBuildingCVC: UICollectionViewCell {
    @IBOutlet var propertyImage: StandardImageView!
    
    @IBOutlet var priceLBL: UILabel!
    @IBOutlet var currencyTypeLBL: UILabel!
    @IBOutlet var bedroomLBL: UILabel!
    @IBOutlet var bathroomLBL: UILabel!
    @IBOutlet var parkingLBL: UILabel!
    @IBOutlet var parkingImage: UIImageView!
    
}
class markercu: GMSMarker {
    func get(){
        
    }
}

class Weak<T: AnyObject> {
    weak var value : T?
    init (value: T) {
        self.value = value
    }
}

class KXSearchResultViewController: KLBaseViewController, CLLocationManagerDelegate {
    
    @IBOutlet var mapview: GMSMapView!
    @IBOutlet weak var collectionRef: UICollectionView!
    @IBOutlet var cityTF: UITextField!
    @IBOutlet var propertyCountInBuildingLBL: UILabel!
    @IBOutlet var multiPropertyView: StandardView!
    @IBOutlet var multiPropertyShadowView: StandardView!
    @IBOutlet var bottomSpaceMultiPropertyView: NSLayoutConstraint!
    @IBOutlet var resultCountLBL: UILabel!
    @IBOutlet var tooltipView: BaseView!
  //  @IBOutlet var tooltipTextView: UITextView!
    @IBOutlet var symbolAvailable: UIView!
    @IBOutlet var symbolAlmostAvailable: UIView!
    @IBOutlet var symbolNotAvailable: UIView!
    @IBOutlet var tooltipTextView: UILabel!
    @IBOutlet var tooltipLeadingAnchor: NSLayoutConstraint!
    
    final var parameterReceived: Json = Json()
    final var isRentalProperty: Bool = false
    final var citySelected: String = ""
    final var isFromFilter: Bool = false
    
    // coordinates of Dubai
    fileprivate var defaultLatitude = 25.2048
    fileprivate var defaultLongitude = 55.2708
    
    fileprivate var availableSource: [KXSearchAvailableModel] = []
    fileprivate var notAvailableSource: [KXSearchAvailableModel] = []
    fileprivate var almostAvailableSource: [KXSearchAvailableModel] = []
    fileprivate var builidngSource: [KXSearchBuildingModel] = []
    
    // for marker handling
    fileprivate var availableBuilding:[KXSearchBuildingModel] = []
    fileprivate var notAvailableBuilding:[KXSearchBuildingModel] = []
    fileprivate var almostAvailableBuilding:[KXSearchBuildingModel] = []
    
    fileprivate var multiBuildingPropertySource: [KXSearchAvailableModel] = []
    fileprivate var multiBuildingAvailability: BuildingAvailability = .available
    
    // for all properties listing
    fileprivate var buildingListViewAllSource: [KXSearchBuildingModel] = []

    private weak var previousMarker:KXCustomMarker?
    
    fileprivate var colorMakerDefaultText: UIColor = .black//UIColor(red: 87/255, green: 86/255, blue: 214/255, alpha: 1.0)

    fileprivate func getCustomMarkerView()-> KXCustomMarker{
        return UINib(nibName: "KXCustomMarker", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! KXCustomMarker
    }
    let locationManager = CLLocationManager()
    fileprivate var isSetCurrentLocation: Bool = false

    private var distance: CGFloat = 2.35 // default searching distance,  it is calculating in viewdidLoad (as per mapview frame)
    private lazy var markerAnimateTime: TimeInterval = 1.25
    private lazy var cameraDelayTime: Double = 0.3
    private var isWebCallOnGoing: Bool = false // this is for to know is getsearchdataweb() function call on going in viewdidload
    private var defaultZoom: Float = 14.0
    private var markerOverlapHandler:[Double] = []
    private var markerOverlapRounding: Int = 4
    private var markerOverlapChange: Double = 0.0005
    private lazy var toolTipAvailable = "This properties available for the given search criteria."
    private lazy var toolTipAlmost = "Property will be available within 90 days."
    private lazy var toolTipNot = "This properties will be available after 90 days."
    /// this is used for on display tooltip for which symbol is clicked
    private enum VisibleSymbol: Int {
        case available
        case almost
        case not
        case hidden
    }
    private lazy var tappedSymbol: VisibleSymbol = .hidden
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let widthOffset = self.mapview.frame.size.width/self.mapview.frame.size.height
//        let currentRadius = self.mapview.getRadius()
//        self.distance = ((currentRadius.cgFloat / 1000)) * widthOffset * 0.4 //*0.42137
//        print("CurrentRadius",currentRadius)
//        print("self.distance",self.distance)
//        print("widthOffset", widthOffset)

        self.cityTF.text = citySelected
        isWebCallOnGoing = true
        getSearchDataWeb()
        
        multiPropertyView.alpha = 0.0
        multiPropertyShadowView.alpha = 0.0
        multiBuildingPropertySource.removeAll()
        
        if let userLat = parameterReceived["latitude"] as? String, userLat.isNotEmpty, let userLon = parameterReceived["longitude"] as? String, userLon.isNotEmpty {
            defaultLatitude = Double(userLat)!
            defaultLongitude = Double(userLon)!
        }
       
        
        let camera = GMSCameraPosition.camera(withLatitude:defaultLatitude , longitude: defaultLongitude , zoom: defaultZoom - 1.0)
        mapview.camera = camera
        //mapview.animate(to: camera)
        addTapGestureToSymbols()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if isFromFilter {
            isFromFilter = false
            mapview.clear()
            clearAllData()
            viewDidLoad()
        }else {
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    /// addding gestures inorder to show tool tip text
    private func addTapGestureToSymbols() {
        let tapGestureRecognizerAvailable = UITapGestureRecognizer(target: self, action: #selector(symbolAvailableTapped(tapGestureRecognizer:)))
        symbolAvailable.isUserInteractionEnabled = true
        symbolAvailable.addGestureRecognizer(tapGestureRecognizerAvailable)

        let tapGestureRecognizerAlmost = UITapGestureRecognizer(target: self, action: #selector(symbolAlmostTapped(tapGestureRecognizer:)))
        symbolAlmostAvailable.isUserInteractionEnabled = true
        symbolAlmostAvailable.addGestureRecognizer(tapGestureRecognizerAlmost)

        let tapGestureRecognizerNot = UITapGestureRecognizer(target: self, action: #selector(symbolNotTapped(tapGestureRecognizer:)))
        symbolNotAvailable.isUserInteractionEnabled = true
        symbolNotAvailable.addGestureRecognizer(tapGestureRecognizerNot)

    }
    
    @objc private func symbolAvailableTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        return
        if tappedSymbol == .available {
            if tooltipView.isHidden {
                tooltipView.isHidden = true
                tooltipTextView.text = toolTipAvailable
                
                //tooltipView.frame.origin.x = symbolAvailable.frame.origin.x
                tooltipLeadingAnchor.constant = 10
                self.view.layoutIfNeeded()
                tooltipView.isHidden = false
                tappedSymbol = .available
                BaseThread.asyncMainDelay(seconds: 2) {
                    if self.tappedSymbol == .available {
                        self.tooltipView.isHidden = true
                        self.tappedSymbol = .hidden
                    }
                }
            }else {
                tooltipView.isHidden = true
                tappedSymbol = .hidden
            }
        }else {
            tooltipView.isHidden = true
            tooltipTextView.text = toolTipAvailable
            tooltipLeadingAnchor.constant = 10
            self.view.layoutIfNeeded()

            tooltipView.isHidden = false
            tappedSymbol = .available

            BaseThread.asyncMainDelay(seconds: 2) {
                if self.tappedSymbol == .available {
                    self.tooltipView.isHidden = true
                    self.tappedSymbol = .hidden
                }

            }

        }

    }
    
    @objc private func symbolAlmostTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        //print("symbol-almost",symbolAlmostAvailable.center.x)

        if tappedSymbol == .almost {
            if tooltipView.isHidden {
                tooltipView.isHidden = true
                tooltipTextView.text = toolTipAlmost
//                tooltipView.center.x = symbolAlmostAvailable.center.x
                
                tooltipLeadingAnchor.constant = (symbolAlmostAvailable.center.x - 85)
                    
                self.view.layoutIfNeeded()
                tooltipView.isHidden = false
                tappedSymbol = .almost

                BaseThread.asyncMainDelay(seconds: 2) {
                    if self.tappedSymbol == .almost {
                        self.tooltipView.isHidden = true
                        self.tappedSymbol = .hidden
                    }
                }
                
            }else {
                tooltipView.isHidden = true
                tappedSymbol = .hidden
            }
        }else {
            tooltipView.isHidden = true
            tooltipTextView.text = toolTipAlmost
            tooltipLeadingAnchor.constant = (symbolAlmostAvailable.center.x - 85)
            self.view.layoutIfNeeded()
            tooltipView.isHidden = false
            tappedSymbol = .almost

            BaseThread.asyncMainDelay(seconds: 2) {
                if self.tappedSymbol == .almost {
                    self.tooltipView.isHidden = true
                    self.tappedSymbol = .hidden
                }
            }
            
        }

    }

    @objc private func symbolNotTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        //return
        if tappedSymbol == .not {
            if tooltipView.isHidden {
                tooltipView.isHidden = true
                tooltipTextView.text = toolTipNot
//                tooltipView.center.x = symbolNotAvailable.frame.origin.x
                tooltipLeadingAnchor.constant = view.frame.width - 180
                self.view.layoutIfNeeded()

                tooltipView.isHidden = false
                tappedSymbol = .not

                BaseThread.asyncMainDelay(seconds: 2) {
                    if self.tappedSymbol == .not {
                        self.tooltipView.isHidden = true
                        self.tappedSymbol = .hidden

                    }
                }
                
            }else {
                tooltipView.isHidden = true
                tappedSymbol = .hidden
            }
        }else {
            tooltipView.isHidden = true
            tooltipTextView.text = toolTipNot
            
            tooltipLeadingAnchor.constant = view.frame.width - 180

            self.view.layoutIfNeeded()
            tooltipView.isHidden = false
            tappedSymbol = .not

            BaseThread.asyncMainDelay(seconds: 2) {
                if self.tappedSymbol == .not {
                    self.tooltipView.isHidden = true
                    self.tappedSymbol = .hidden

                }
                
            }
            
        }

    }

    

    private func clearAllData() {
        availableSource.removeAll()
        almostAvailableSource.removeAll()
        notAvailableSource.removeAll()
        
        availableBuilding.removeAll()
        almostAvailableBuilding.removeAll()
        notAvailableBuilding.removeAll()
        multiBuildingPropertySource.removeAll()
        builidngSource.removeAll()
        buildingListViewAllSource.removeAll()
        
    }
    
    
    @IBAction func Humberger_menu(_ sender: Any) {
        showSideMenu()
    }
    
    @IBAction func Back_Button(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    
    }
    
    @IBAction func filterTapped(_ sender: UIButton) {
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXFilterViewController") as! KXFilterViewController
        
        vc.isMapFilter = true
        
        vc.parameterReceived = self.parameterReceived
        vc.isRentalProperty = self.isRentalProperty
        vc.citySelected = self.citySelected
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .chat
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }

    
    @IBAction func showAllInBuildingTapped(_ sender: UIButton) {
        
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXSearchResultMenu") as! KXSearchResultMenu
        vc.dataSource = multiBuildingPropertySource
        vc.buildingAvailability = multiBuildingAvailability
        vc.propertyLocation = multiBuildingPropertySource[0].location
        vc.citySelected = self.citySelected
        self.navigationController?.pushViewController(vc, animated: true)
        
    }


    @IBAction func showAllPropertiesTapped(_ sender:UIButton){
        
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXSearchResultListingBuildingVC") as! KXSearchResultListingBuildingVC
        var i = 0
        for _ in buildingListViewAllSource {
            i += 1
        }
        
        vc.dataSource = buildingListViewAllSource
        vc.parameterReceived = self.parameterReceived
        vc.citySelected = self.citySelected
        vc.isFromMap = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func selectCityTapped(_ sender: UIButton) {
        
    }
    
    
    @IBAction func setCurrentLocationAndShowAll(_ sender: UIButton) {
        
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                showBanner(title: "Access denied", message: "Please enable location service")
            case .restricted:
                showBanner(title: "Access denied", message: "Please enable location service")
            case .denied:
                showBanner(title: "Access denied", message: "Please enable location service")
            case .authorizedAlways:
                break
            case .authorizedWhenInUse:
                break
            }
            locationManager.startUpdatingLocation()
        }else {
            showBanner(title: "Access denied", message: "Please enable location service")
        }

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        isSetCurrentLocation = true
        locationManager.stopUpdatingLocation()
        let camera = GMSCameraPosition.camera(withLatitude:locValue.latitude , longitude: locValue.longitude , zoom: defaultZoom)
        mapview.animate(to: camera)

    }

    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if let selectedMarker = marker.iconView as? KXCustomMarker {
            //selectedMarker.markerBgImage.image = UIImage(named: "markerBgSelected")
            selectedMarker.setSelectedBgColor()
            selectedMarker.priceLabel.textColor = .white
            if previousMarker == nil {
                previousMarker = selectedMarker
            }else if previousMarker == selectedMarker {
                if selectedMarker.priceLabel.textColor == colorMakerDefaultText {
                   // selectedMarker.markerBgImage.image = UIImage(named: "markerBgSelected")
                    selectedMarker.setSelectedBgColor()
                    selectedMarker.priceLabel.textColor = .white
                }else {
                    //selectedMarker.markerBgImage.image = UIImage(named: "markerBg")
                    selectedMarker.setDeselectedBgColor()
                    selectedMarker.priceLabel.textColor = colorMakerDefaultText
                }
                previousMarker = selectedMarker
            }else{
                //previousMarker?.markerBgImage.image = UIImage(named: "markerBg")
                previousMarker?.setDeselectedBgColor()
                previousMarker?.priceLabel.textColor = colorMakerDefaultText
                previousMarker = selectedMarker
            }
            
        }
        
        if let buildingData = marker.userData as? KXSearchBuildingModel {
            
            if buildingData.property_building_count == 1 {
                UIView.animate(withDuration: 0.0, animations: {
                    self.multiPropertyView.alpha = 0.0
                    self.multiPropertyShadowView.alpha = 0.0
                })

                let propertyId = buildingData.property_buildings[0].id
                let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                vc.propertyId = propertyId
                vc.isFromSearch = true
                self.navigationController?.pushViewController(vc, animated: true)
            }else if buildingData.property_building_count > 1 {
                multiBuildingPropertySource.removeAll()
                for item in buildingData.property_buildings {
                    multiBuildingPropertySource.append(contentsOf: self.availableSource.filter({ $0.id == item.id  }))
                    multiBuildingPropertySource.append(contentsOf: self.almostAvailableSource.filter({ $0.id == item.id  }))
                    multiBuildingPropertySource.append(contentsOf: self.notAvailableSource.filter({ $0.id == item.id  }))
                }
                multiBuildingAvailability = buildingData.buildingAvailability
                propertyCountInBuildingLBL.text = multiBuildingPropertySource.count.description
                collectionRef.reloadData()
                showMultiPropertyView()
                
                
            }
            
        }
        
        return true
    }
    
    fileprivate func showMultiPropertyView(){
        if multiPropertyView.alpha == 0.0 {
            UIView.animate(withDuration: 0.2, animations: {
                self.multiPropertyView.alpha = 1.0
                self.multiPropertyShadowView.alpha = 1.0
            })
        }else {
            UIView.animate(withDuration: 0.2, animations: {
                self.multiPropertyView.alpha = 0.0
                self.multiPropertyShadowView.alpha = 0.0
            })
        }

    }
    
    
    fileprivate func markerGenerator() {
        print("Generating Marker")
        mapview.clear()
        if !isWebCallOnGoing {
            markerAnimateTime = 0.6 // this is for decrease marker animating time (alpha changing 0 to 1) if map scrolled
        }
        // generating available building marker
        buildingListViewAllSource.removeAll()
        markerOverlapHandler.removeAll()
        
        for item in availableBuilding {
           // print("availableBuilding-Loop")
            let customMarker = GMSMarker()
            let customView = getCustomMarkerView()
            customView.alpha = 0.0
            if item.property_building_count == 1 {
                let property = availableSource.filter {$0.id == item.property_buildings[0].id}.first
                if let property = property  {
                    customView.priceLabel.text = property.usd_price.showPriceInMapWithDollar()
                }
            }else {
                customView.priceLabel.text = item.property_building_count.description + " Units"
               // print("property_building_count",item.property_building_count.description)
            }
            
            customView.setLabelCornerRadius()
            customView.setDeselectedBgColor()
            let image = UIImage(named: "1x_0002_map-pin-green") //UIImage(named: "markerAvailableSmall") //
            item.buildingAvailability = .available
            customMarker.userData = item
            buildingListViewAllSource.append(item)
            
            //customView.markerBgImage.image = UIImage(named: "markerBg")
            customView.markerImage.image = image
            customMarker.iconView = customView
            
            var correctedLat: Double = item.latitude.double
            
            while true {
                //print("InfiniteLooop_Times_Available")
                if markerOverlapHandler.contains(correctedLat.roundTo(places: markerOverlapRounding)) {
                    correctedLat = correctedLat - markerOverlapChange // changes location(decrease lat a lightly ) inorder to avoid overlaping
                    if markerOverlapHandler.contains(correctedLat.roundTo(places: markerOverlapRounding)) {
                     //   print("Continues_Loop")
                        continue
                    }else {
                      //  print("Breaking Loop Intinal")
                        
                        break
                    }
                }else {
                    //print("OuterBreak")
                    break
                }
            }
            
            markerOverlapHandler.append(correctedLat.roundTo(places: markerOverlapRounding))
            print("CorrectedLat_Avali:",correctedLat.roundTo(places: markerOverlapRounding))
            customMarker.position = CLLocationCoordinate2D(latitude: correctedLat, longitude: Double(item.longitude)!)
            customMarker.map = mapview
            
            BaseThread.asyncMainDelay(seconds: 0.0, {
                UIView.animate(withDuration: self.markerAnimateTime, animations: {
                    customView.alpha = 1.0
                })

            })
            //print(item.longitude,"available:Lat",item.latitude)
            //print("MarkerLocations:",customMarker.position)

        }
        
        // generating almost available building marker
        for item in almostAvailableBuilding {
            //print("almost-Loop")
            let customMarker = GMSMarker()
            let customMarkerView = getCustomMarkerView()
            customMarkerView.alpha = 0.0

            if item.property_building_count == 1 {
                let property = almostAvailableSource.filter {$0.id == item.property_buildings[0].id}.first
                if let property = property  {
                    customMarkerView.priceLabel.text = property.usd_price.showPriceInMapWithDollar()
                }
            }else {
                customMarkerView.priceLabel.text = item.property_building_count.description + " Units"
                //print("alsmostttttproperty_building_count",item.property_building_count.description)

            }
            customMarkerView.setLabelCornerRadius()
            customMarkerView.setDeselectedBgColor()

            let image = UIImage(named: "1x_0001_map-pin-yellow") // UIImage(named: "markerAlmostSmall")//
            item.buildingAvailability = .almostAvailable
            customMarker.userData = item
            buildingListViewAllSource.append(item)
            //customMarkerView.markerBgImage.image = UIImage(named: "markerBg")
            customMarkerView.markerImage.image = image
            customMarker.iconView = customMarkerView
            
            //print("LatLong-Almost:",item.latitude.double.roundTo(places: 4))
            var correctedLat: Double = item.latitude.double
            
            while true {
                //print("InfiniteLooop_Times_almost")
                if markerOverlapHandler.contains(correctedLat.roundTo(places: markerOverlapRounding)) {
                    correctedLat = correctedLat - markerOverlapChange // changes location(decrease lat a lightly ) inorder to avoid overlaping
                    if markerOverlapHandler.contains(correctedLat.roundTo(places: markerOverlapRounding)) {
                      //  print("Continues_Loop")
                        continue
                    }else {
                       // print("Breaking Loop Intinal")

                        break
                    }
                }else {
                    //print("OuterBreak")

                    break
                }
            }
            
            
            markerOverlapHandler.append(correctedLat.roundTo(places: markerOverlapRounding))
            print("CorrectedLat_Almost_Avali:",correctedLat.roundTo(places: markerOverlapRounding))

            customMarker.position = CLLocationCoordinate2D(latitude: correctedLat, longitude: Double(item.longitude)!)
            customMarker.map = mapview
            BaseThread.asyncMainDelay(seconds: 0.0, {
                UIView.animate(withDuration: self.markerAnimateTime, animations: {
                    customMarkerView.alpha = 1.0
                })
                
            })
            
        }
        
        // generating no available building marker
        for item in notAvailableBuilding {
            let customMarker = GMSMarker()
            let customMarkerView = getCustomMarkerView()
            customMarkerView.alpha = 0.0

            if item.property_building_count == 1 {
                let property = notAvailableSource.filter {$0.id == item.property_buildings[0].id}.first
                if let property = property  {
                    customMarkerView.priceLabel.text = property.usd_price.showPriceInMapWithDollar() //property.usd_price
                }
            }else {
                customMarkerView.priceLabel.text = item.property_building_count.description + " Units"
               // print("notAvailableproperty_building_count",item.property_building_count.description)

            }
            customMarkerView.setLabelCornerRadius()
            customMarkerView.setDeselectedBgColor()

            let image = UIImage(named: "1x_0003_map-pin-red") // UIImage(named: "markerNotAvailableSmall")//
            item.buildingAvailability = .notAvailable
            customMarker.userData = item
            buildingListViewAllSource.append(item)
            //customMarkerView.markerBgImage.image = UIImage(named: "markerBg")
            customMarkerView.markerImage.image = image
            customMarker.iconView = customMarkerView
            
            var correctedLat: Double = item.latitude.double
            
            while true {
                //print("InfiniteLooop_Times_notAvailable")
                if markerOverlapHandler.contains(correctedLat.roundTo(places: markerOverlapRounding)) {
                    correctedLat = correctedLat - markerOverlapChange // changes location(decrease lat a lightly ) inorder to avoid overlaping
                    if markerOverlapHandler.contains(correctedLat.roundTo(places: markerOverlapRounding)) {
                    //    print("Continues_Loop")
                        continue
                    }else {
                    //    print("Breaking Loop Intinal")
                        break
                    }
                }else {
                   // print("OuterBreak")

                    break
                }
            }
            markerOverlapHandler.append(correctedLat.roundTo(places: markerOverlapRounding))

            customMarker.position = CLLocationCoordinate2D(latitude: correctedLat, longitude: Double(item.longitude)!)
            customMarker.map = mapview

            BaseThread.asyncMainDelay(seconds: 0.0, {
                UIView.animate(withDuration: self.markerAnimateTime, animations: {
                    customMarkerView.alpha = 1.0
                })
                
            })

        }

        // list of multiple property building list and append to all building source
        for item in buildingListViewAllSource {
            var propertiesInsideBuilding: [KXSearchAvailableModel] = []
            propertiesInsideBuilding.removeAll()
            for item1 in item.property_buildings {
                propertiesInsideBuilding.append(contentsOf: self.availableSource.filter({ $0.id == item1.id  }))
                propertiesInsideBuilding.append(contentsOf: self.almostAvailableSource.filter({ $0.id == item1.id  }))
                propertiesInsideBuilding.append(contentsOf: self.notAvailableSource.filter({ $0.id == item1.id  }))
            }
        
            item.propertySourceInBuilding = propertiesInsideBuilding
        }
        
        if buildingListViewAllSource.count > 1 {
            resultCountLBL.text = buildingListViewAllSource.count.description + " Results"
            
        }else {
            resultCountLBL.text = buildingListViewAllSource.count.description + " Result"
            
        }
        
        BaseThread.asyncMainDelay(seconds: cameraDelayTime) {
            if self.isWebCallOnGoing {
                self.mapview.animate(toZoom: self.defaultZoom)
            }
        }
        
        /// this is for setting the first webcall indicator is set to false after all updations-> delay is used for holding, until the googlemap-IdleAt function hits.
        BaseThread.asyncMainDelay(seconds: cameraDelayTime + 1.5) {
            self.isWebCallOnGoing = false
        }
        markerOverlapHandler.removeAll()
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if multiPropertyView.alpha == 1.0 {
            UIView.animate(withDuration: 0.2, animations: {
                self.multiPropertyView.alpha = 0.0
                self.multiPropertyShadowView.alpha = 0.0
                if self.previousMarker != nil {
                   // self.previousMarker?.markerBgImage.image = UIImage(named: "markerBg")
                    self.previousMarker?.setDeselectedBgColor()
                    self.previousMarker?.priceLabel.textColor = self.colorMakerDefaultText
                    self.previousMarker = nil

                }else {
                    
                }

            })
        }
        //showMultiPropertyView()
    }
    
    fileprivate func updateSearchData(){
        print("WebCall-updateSearchData")
        let url = "searchProperty"
        var params = parameterReceived
        if isLoggedIn() {
            params["user_id"] = Credentials.shared.userId
        }
        
        if let _ = params["distance"] {
            //filterParams.removeValue(forKey: "distance")
        }else {
            params["distance"] = distance
        }

        
        print(params)
        Webservices.getMethodWithoutLoading(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            //print("SearchResponse:-",response)

            self.buildingListViewAllSource.removeAll()
            self.availableBuilding.removeAll()
            self.almostAvailableBuilding.removeAll()
            self.notAvailableBuilding.removeAll()
            
            
            BaseThread.asyncMain {
                let searchResult = KXSearchListModel.getValue(fromObject: data)
                self.availableSource = searchResult.availables
                self.notAvailableSource = searchResult.not_availables
                self.almostAvailableSource = searchResult.almost_availables
                self.builidngSource = searchResult.buildings
                
                for itemBuilding in self.builidngSource {
                    for itemProperty in itemBuilding.property_buildings {
                        if self.availableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .available
                            self.availableBuilding.append(itemBuilding)
                            break
                        }else if self.almostAvailableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .almostAvailable
                            self.almostAvailableBuilding.append(itemBuilding)
                            break
                        }else if self.notAvailableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .notAvailable
                            self.notAvailableBuilding.append(itemBuilding)
                            break
                        }
                    }
                }
                self.markerGenerator()
            }
        }
    }
    
    fileprivate func getSearchDataWeb(){
        print("WebCall-getSearchDataWeb")
        let url = "searchProperty"
        var params = parameterReceived
        if isLoggedIn() {
            params["user_id"] = Credentials.shared.userId
        }
        
        if let _ = params["distance"] {
            //filterParams.removeValue(forKey: "distance")
        }else {
            params["distance"] = distance
        }
        
        Webservices2.getMethodWith(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else {
                self.isWebCallOnGoing = false
                return
            }
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            //print("SearchResponse:-",response)
            
            BaseThread.asyncMain {
                let searchResult = KXSearchListModel.getValue(fromObject: data)
                self.availableSource = searchResult.availables
                self.notAvailableSource = searchResult.not_availables
                self.almostAvailableSource = searchResult.almost_availables
                self.builidngSource = searchResult.buildings
                
                for itemBuilding in self.builidngSource {
                    for itemProperty in itemBuilding.property_buildings {
                        if self.availableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .available
                            self.availableBuilding.append(itemBuilding)
                            break
                        }else if self.almostAvailableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .almostAvailable
                            self.almostAvailableBuilding.append(itemBuilding)
                            break
                        }else if self.notAvailableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .notAvailable
                            self.notAvailableBuilding.append(itemBuilding)
                            break
                        }
                    }
                }
                self.markerGenerator()
                
            }
            
        }
        
    }
    
    
    
    func invalidateCurrentAlamofireRequest( handler: @escaping ()->Void){
        DispatchQueue.global(qos: .background).async {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                tasks.forEach{$0.cancel()}  // To check $0.originalRequest?.url?.absoluteString == requiredURL
                handler();
            }
            
            print("Current tasks invalidated.")
        }
    }
    
    func updateCurrentSearchingLocation(latitude:String,longitude:String){
        
        print("updateCurrentSearchingLocation");
        BaseThread.asyncMain {
            self.resultCountLBL.text = "Searching..."
        }
        DispatchQueue.global(qos: .background).async {
            self.invalidateCurrentAlamofireRequest(handler: {
                
                self.parameterReceived["latitude"] = latitude
                self.parameterReceived["longitude"] = longitude
                self.updateSearchData()
                
            })
        }
        
    }
    
}



//
extension KXSearchResultViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return multiBuildingPropertySource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchBuildingCVC", for: indexPath) as! searchBuildingCVC

        let source = multiBuildingPropertySource[indexPath.row]
        cell.priceLBL.text = " \(source.usd_price.showPriceInDollar())" //showPriceInKiloWithoutDollar
        cell.bedroomLBL.text = source.property_bedrooms?.data
        cell.bathroomLBL.text = source.property_bathrooms?.data
        if !source.property_images.isEmpty {
            let imageURL = source.property_images[0].image
            cell.propertyImage.kf.setImage(with: URL(string: baseImageURLDetail + imageURL), placeholder:#imageLiteral(resourceName: "buildingPlaceHolder") , options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionRef.frame.width, height: collectionRef.frame.height)
    }
    
    
}


extension KXSearchResultViewController: GMSMapViewDelegate{
 
    /* handles Info Window tap */
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("didTapInfoWindowOf")
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
        vc.isFromSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("didLongPressInfoWindowOf")
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        print("markerInfoWindow")
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: 90, height:40
        ))
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 18
        let lbl1 = UILabel(frame: CGRect.init(x: 8, y: 8, width: view.frame.size.width - 16, height: 15))
        lbl1.text = "$ 300K"
        lbl1.textColor = UIColor.red
        view.addSubview(lbl1)

        let lbl2 = UILabel(frame: CGRect.init(x: lbl1.frame.origin.x, y: lbl1.frame.origin.y + lbl1.frame.size.height + 3, width: view.frame.size.width - 16, height: 15))
        //lbl2.text = "press me"
        if #available(iOS 8.2, *) {
            lbl2.font = UIFont.systemFont(ofSize: 14, weight: .light)
        } else {
            // Fallback on earlier versions
        }
        view.addSubview(lbl2)

        return view
    }
 
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("idleAt")
        if !isWebCallOnGoing {
            print("** Map Location has been Set. Did Finish animating")
            print("Zooommmmm:",mapView.camera.zoom)
            //print("Latitude : \(position.target.latitude.description) Longitude : \(position.target.longitude.description)")
            

            let widthOffset = self.mapview.frame.size.width/self.mapview.frame.size.height
            let currentRadius = self.mapview.getRadius()
            self.distance = ((currentRadius.cgFloat / 1000)) * widthOffset * 0.4 //*0.42137
            
            print("CurrentRadius",currentRadius)
            print("self.distance",self.distance)
            print("widthOffset",    widthOffset)


            let latitude = position.target.latitude
            let longitude = position.target.longitude
            updateCurrentSearchingLocation(latitude: latitude.description, longitude: longitude.description)
            getCityFromLatLong(lat: latitude, lon: longitude)
        }else {
            print("IDLE AT -map function")
        }
    }
    
    private func getCityFromLatLong(lat: CLLocationDegrees, lon: CLLocationDegrees) {
        
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2D.init(latitude: lat, longitude: lon)) { (response, error) in
            if error == nil {
                print("No Error")
                
                if let result = response?.firstResult() {
                    
                    BaseThread.asyncMain {
                        self.cityTF.text = result.subLocality ?? result.locality ??  result.administrativeArea ?? result.country
                        print(result.locality as Any)
                        print(result.subLocality as Any)
                        print(result.administrativeArea as Any)
                        print(result.country as Any)

                    }
                    
                }
                
            }else {
                print("errorerrorerrorerrorerrorerror",error as Any)
            }
        }
    }

}

extension GMSMapView {
    
    func getCenterCoordinate() -> CLLocationCoordinate2D {
        let centerPoint = self.center
        let centerCoordinate = self.projection.coordinate(for: centerPoint)
        return centerCoordinate
    }
    
    func getTopCenterCoordinate() -> CLLocationCoordinate2D {
        // to get coordinate from CGPoint of your map
        let topCenterCoor = self.convert(CGPoint(x: self.frame.size.width, y: 0), from: self)
        let point = self.projection.coordinate(for: topCenterCoor)
        return point
    }
    
    func getRadius() -> CLLocationDistance {
        let centerCoordinate = getCenterCoordinate()
        let centerLocation = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
        let topCenterCoordinate = self.getTopCenterCoordinate()
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        let radius = CLLocationDistance(centerLocation.distance(from: topCenterLocation))
        return round(radius)
    }

}






/*http://koloxo-homes.dev.webcastle.in/api/searchProperty?latitude=9.971094075639815&longitude=76.30818665027618&saletype=2&country_id=101&moving_in=2018-09-16&moving_out=31%20Day&availability=almost_available



// http://koloxo-homes.dev.webcastle.in/api/searchProperty?latitude=10.5284793&longitude=76.2681871&saletype=1&take=4&skip=0&bathrooms[0]=1&bathrooms[1]=3&bathrooms[2]=2&floor[0]=1&building_type[0]=1&building_type[1]=2&currency=7&maxprice=101 */


