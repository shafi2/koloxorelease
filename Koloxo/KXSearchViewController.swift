//
//  KXSearchViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 22/03/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import DropDown
import DatePickerDialog
import GooglePlaces
import MapKit
import GoogleMaps

class parameterContainerForRent {
    var movingInDate: String = ""
    var movingOutDate: String = ""
    var saleType: Int = 1 // rent -> 2, buy 1
    
    static var shared = parameterContainerForRent()
    
    private init(){}
    
    func updateParams(params: Json, isRentalProperty: Bool){
        if let date = params["moving_in"] as? String {
            movingInDate = date
        }else {
            movingInDate = ""
        }
        
        if let date = params["moving_out"] as? String {
            movingOutDate = date
        }else {
            movingOutDate = ""
        }
        
        saleType = isRentalProperty ? 2 : 1
        
    }
    
    func clearParams() {
        movingInDate = ""
        movingOutDate = ""
        saleType = 1
    }
    
}

class KXSearchViewController: UIViewController, UITextFieldDelegate, GMSAutocompleteViewControllerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet var buyRentView: StandardView!
    @IBOutlet var movingView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var BuyAnchor: UIView!
    @IBOutlet var SelectCountrybuy: UILabel!
    @IBOutlet var dateTextField: UILabel!
    
    @IBOutlet var selectmonth: UILabel!
    @IBOutlet var SelectCountryRent: UILabel!
    @IBOutlet var RentAnchor_selectCountry: UIView!
    
    @IBOutlet var buyBTNRef: UIButton!
    @IBOutlet var rentBTNRef: UIButton!
    
    var buyView = BuyView()
    var rentView = RentView()
    
    let desectedColor:UIColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1) //UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
    
    // properties
    fileprivate var monthSources: [KXMonthsModel] = []
    fileprivate var countrySource :[KXCountryListModel] = []
    fileprivate var countryIndexPath: IndexPath?
    fileprivate var countryId: Int?
    fileprivate var geocoder:GMSGeocoder = GMSGeocoder()
    let locationManager = CLLocationManager()
    fileprivate var isDropDownCountry: Bool = true
    
    // search parameter properties
    fileprivate var isRentalProperty: Bool = true
    fileprivate var selectedCountryId: Int = -1
    fileprivate var selectedLatitude: String = ""
    fileprivate var selectedLongitude: String = ""
    fileprivate var placeName: String = ""
    
    
    // MM  /  DD  /  YYYY
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(currentTabSet), name: .searchTabActive, object: nil)
       // guard isLoadedTabSearchChatNotify == .search else { return }
//        buyView = UINib(nibName: "BuyView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BuyView
//        rentView = UINib(nibName: "RentView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RentView
//
//        buyView.selectCityTF.delegate = self
//        rentView.selectCityTF.delegate = self
//        rentView.date.text = getDateAsString(date: Date())
//
//        DispatchQueue.main.async {
//            self.buyView.frame = CGRect(x: 0, y: 0, width: self.bottomView.frame.size.width, height: self.bottomView.frame.size.height)
//            self.rentView.frame = CGRect(x: 0, y: 0, width: self.bottomView.frame.size.width, height: self.bottomView.frame.size.height)
//            self.bottomView.addSubview(self.rentView)
//            self.movingView.frame.size.width = self.buyRentView.frame.size.width/2 + 2
//            self.movingView.frame.origin.x = 0
//            self.buyView.removeFromSuperview()
//        }
//
//        movingView.frame.size.width = buyRentView.frame.size.width/2
//        movingView.frame.origin.x = 0
//
//        setCountryData()
//        setMonthData()
    }
    
    @objc func currentTabSet(){
        buyView = UINib(nibName: "BuyView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BuyView
        rentView = UINib(nibName: "RentView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RentView
        
        buyView.selectCityTF.delegate = self
        rentView.selectCityTF.delegate = self
        rentView.date.text = getDateAsString(date: Date())
        
        DispatchQueue.main.async {
            self.buyView.frame = CGRect(x: 0, y: 0, width: self.bottomView.frame.size.width, height: self.bottomView.frame.size.height)
            self.rentView.frame = CGRect(x: 0, y: 0, width: self.bottomView.frame.size.width, height: self.bottomView.frame.size.height)
            self.bottomView.addSubview(self.rentView)
            self.movingView.frame.size.width = self.buyRentView.frame.size.width/2 + 2
            self.movingView.frame.origin.x = 0
            self.buyView.removeFromSuperview()
        }
        
        movingView.frame.size.width = buyRentView.frame.size.width/2
        movingView.frame.origin.x = 0
        
        setCountryData()
        setMonthData()
    }
    
    
    func setCountryData(){
        if let countryData = SessionStore.current.countryData{
            self.countrySource = countryData
            self.selectCurrentUserLocation()
        }else{
            getCountryData()
        }
    }
    
    func setMonthData(){
        if let monthData = SessionStore.current.monthData{
            self.monthSources = monthData
            BaseThread.asyncMain {
                self.rentView.month.text = self.monthSources[0].data
            }
        }else{
            getMonthsWeb()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    //MARK: Tab Actions
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .chat
            isLoadedTabSearchChatNotify = .chat

            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_button(_ sender: Any) {
        showSideMenu()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        parameterContainerForRent.shared.clearParams()

    }

    
    @IBAction func buyButton(_ sender: Any)
    {

        isRentalProperty = true
        
        if let button = sender as? UIButton{
            button.backgroundColor = .white
            buyBTNRef.backgroundColor = desectedColor
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.movingView.frame.origin.x = 0 //self.buyRentView.frame.size.width/2 - 2
            self.bottomView.addSubview(self.rentView)
            self.buyView.removeFromSuperview()
        })
        
    }
    
    @IBAction func rentButton(_ sender: Any)
    {
        
        isRentalProperty = false
        if let button = sender as? UIButton{
            button.backgroundColor = UIColor.white
            rentBTNRef.backgroundColor = desectedColor
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.movingView.frame.size.width = self.buyRentView.frame.size.width/2 + 2
            self.movingView.frame.origin.x = self.buyRentView.frame.size.width/2 - 2
            self.bottomView.addSubview(self.buyView)
            self.rentView.removeFromSuperview()
        })

    }
    
    @IBAction func selectCountryTapped(_ sender: UIButton) {
        isDropDownCountry = true
        let categorySelectionViewController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        categorySelectionViewController.setTitle(title: "Select Country")

        categorySelectionViewController.tableSource = countrySource.map{$0.name}
        categorySelectionViewController.delegate = self
        categorySelectionViewController.selectedIndexPath = countryIndexPath
        
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)

    }
    
    // delegate from county selection
    func selectedValue(index: IndexPath) {
        
        if isDropDownCountry {
            let country = countrySource[index.row]
            countryId = country.id
            countryIndexPath = index
            rentView.SelectCountry.text = country.name
            buyView.SelectCountry.text = country.name
            rentView.selectCityLBL.text = ""
            buyView.selectCityLBL.text = ""
        }else {
            
            let monthValue = monthSources[index.row]
            rentView.month.text = monthValue.data
            
        }
        
        
    }
    
    
    @IBAction func selectCityTapped(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    fileprivate func updateUIwithGeocoderResult(result: GMSAddress, latitude: CLLocationDegrees, longitude: CLLocationDegrees, isMyLocation: Bool){
        BaseThread.asyncMain {
            
            if isMyLocation {
                self.rentView.selectCityLBL.text = result.subLocality ?? result.locality ?? result.administrativeArea ?? ""
                self.buyView.selectCityLBL.text = result.subLocality ?? result.locality ?? result.administrativeArea ?? ""
            }else {
                self.rentView.selectCityLBL.text = self.placeName
                self.buyView.selectCityLBL.text = self.placeName

            }
            
            self.selectedLatitude = latitude.description
            self.selectedLongitude = longitude.description
            self.rentView.SelectCountry.text = result.country ?? ""
            self.buyView.SelectCountry.text = result.country ?? ""
            let countryName = result.country ?? ""
            let searchableCountyName = countryName.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
            
            self.countryId = self.countrySource.filter{ $0.name.trimmingCharacters(in: .whitespacesAndNewlines).lowercased() == searchableCountyName}.first?.id
//            print(countryName)
//            print(searchableCountyName)
//            print("CountyId",self.countryId as Any)
//            debugPrint(result as AnyObject)
            
        }
    }

    // autocomplete delegates
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        
        placeName = place.name
        
        geocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude:  place.coordinate.latitude, longitude:  place.coordinate.longitude)) { (response, err) in
            if err == nil{
                if let result = response?.firstResult(){
                    self.updateUIwithGeocoderResult(result: result, latitude: place.coordinate.latitude, longitude: place.coordinate.longitude, isMyLocation: false)
                }
            }else{
                print(err as Any)
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }

    
    
    @IBAction func selectCityTextFieldDidEnd(_ sender: UITextField) {
        
    }
    
    
    @IBAction func selectCurrentLocation(_ sender: UIButton) {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.delegate = self
        if CLLocationManager.locationServicesEnabled() {

            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                showBanner(title: "Access denied", message: "Please enable location service")
            case .restricted:
                showBanner(title: "Access denied", message: "Please enable location service")
            case .denied:
                showBanner(title: "Access denied", message: "Please enable location service")
            case .authorizedAlways:
                break
            case .authorizedWhenInUse:
                break
            }
            print("location enabled")
            locationManager.startUpdatingLocation()
        }else {
            print("location disabled")
            showBanner(title: "Access denied", message: "Please enable location service")
        }
       

    }
    
    func selectCurrentUserLocation(){
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                break
                //showBanner(title: "Access denied", message: "Please enable location service")
            case .restricted:
                break
                //showBanner(title: "Access denied", message: "Please enable location service")
            case .denied:
                break
                //showBanner(title: "Access denied", message: "Please enable location service")
            case .authorizedAlways:
                break
            case .authorizedWhenInUse:
                break
            }
            print("location enabled")
            locationManager.startUpdatingLocation()
        }
        
        
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        geocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude:  locValue.latitude, longitude:  locValue.longitude)) { (response, err) in
            if err == nil{
                if let result = response?.firstResult(){
                    self.updateUIwithGeocoderResult(result: result, latitude: locValue.latitude, longitude: locValue.longitude, isMyLocation: true)
                }
            }else{
                print(err as Any)
            }
        }
        locationManager.stopUpdatingLocation()
    }


    
    // date selection
    @IBAction func rentView_datePicker(_ sender: Any) {
        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/ yyyy"
                self.rentView.date.text = formatter.string(from: dt)
            }
        }
    }
    
    // month selection
    @IBAction func DropMonths(_ sender: Any) {
        isDropDownCountry = false
        let categorySelectionViewController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        categorySelectionViewController.tableSource = monthSources.map{$0.data}
        categorySelectionViewController.setTitle(title: "Select Months")
        categorySelectionViewController.delegate = self
        categorySelectionViewController.selectedIndexPath = countryIndexPath
        
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)

    }
    
    
    fileprivate func isValidateData() -> Bool {
        
        if isRentalProperty {
            print("Rental Property")
            if BaseValidator.isNotEmpty(string: rentView.SelectCountry.text) && rentView.SelectCountry.text != "Select the country"
                && BaseValidator.isNotEmpty(string: rentView.date.text) && rentView.date.text != "MM / DD / YYYY"
                && BaseValidator.isNotEmpty(string: rentView.month.text) && self.countryId != nil
                && BaseValidator.isNotEmpty(string: rentView.selectCityLBL.text) && rentView.selectCityLBL.text != "Select your city" {
                
                return true
                
            }else {
                showBanner(message: "Please fill all fields")
                return false
            }
        }else{
            if BaseValidator.isNotEmpty(string: buyView.SelectCountry.text) && buyView.SelectCountry.text != "Select the country"
                && self.countryId != nil
                && BaseValidator.isNotEmpty(string: buyView.selectCityLBL.text) && buyView.selectCityLBL.text != "Select your city" {
                
                return true
                
            }else {
                showBanner(message: "Please fill all fields")
                return false
            }
        }
        
        
    }
    
    @IBAction func Search_button(_ sender: Any) {
        
        //http://koloxo-homes.dev.webcastle.in/api/searchProperty?latitude=9.971094075639815&longitude=76.30818665027618&saletype=2&country_id=101&moving_in=2018-09-16&moving_out=31%20Day&availability=almost_available

        
        
        // http://koloxo-homes.dev.webcastle.in/api/searchProperty?latitude=10.5284793&longitude=76.2681871&saletype=1&take=4&skip=0&bathrooms[0]=1&bathrooms[1]=3&bathrooms[2]=2&floor[0]=1&building_type[0]=1&building_type[1]=2&currency=7&maxprice=101
        
        guard isValidateData() else { return }
        
        var passingParam = Json()
        
        passingParam["latitude"] = self.selectedLatitude
        passingParam["longitude"] = self.selectedLongitude
        passingParam["saletype"] = isRentalProperty ? 2 : 1
        passingParam["country_id"] = countryId
        
        if isRentalProperty {
            passingParam["moving_in"] = getDateString(from: self.rentView.date.text, format: "MM/dd/yyyy", outputFormat: "yyyy-MM-dd")
            passingParam["moving_out"] = self.rentView.month.text
        }else{
            
            if let _ = passingParam["moving_in"] {
                passingParam.removeValue(forKey: "moving_in")
            }
            if let _ = passingParam["moving_out"] {
                passingParam.removeValue(forKey: "moving_out")
            }
            
        }
        print("Passing Param",passingParam)
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXSearchResultViewController") as! KXSearchResultViewController
        
        vc.parameterReceived = passingParam
        vc.isRentalProperty = self.isRentalProperty
        vc.citySelected = self.buyView.selectCityLBL.text ?? ""
        parameterContainerForRent.shared.updateParams(params: passingParam, isRentalProperty: self.isRentalProperty) // pass paramters to details page
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}


// Handling web service
extension KXSearchViewController: CategorySelectionDelegate {
    
    private func getMonthsWeb(){
        let url = "getMonths"
        Webservices2.getMethod(url: url) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["Data"] as? JsonArray, !dataArray.isEmpty else { return }
            print("Data",dataArray)
            let months = KXMonthsModel.getValue(fromArray: dataArray)
            self.monthSources = months.sorted(by: { $0.display_order < $1.display_order })

            BaseThread.asyncMain {
               self.rentView.month.text = self.monthSources[0].data
            }

        }
   }

    private func getCountryData(){
        getCountryListWeb { (list) in
            //guard let _ = self else { return }
            self.countrySource = list
            self.selectCurrentUserLocation()
        }
    }

    
    

}
    
    















