//
//  KXT&CViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 11/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXTCViewController: UIViewController {

    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var termsWebView: UIWebView!
    
    final var isFromRegistration: Bool = false
    
    private lazy var termsHTML: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromRegistration {
            //titleLBL.text = "Terms of Use"
        }else {
            //titleLBL.text = "Terms of Use"

        }

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getDataFromWeb()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

    private func getDataFromWeb() {
        Webservices.getMethod(url: "terms_condition") { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let contentData = response["Data"] as? JsonArray else { return }
            if self.isFromRegistration {
                if !contentData.isEmpty, let data = contentData.first {
                    self.termsHTML = data["content"] as? String ?? ""
                    BaseThread.asyncMain {
                        self.termsWebView.loadHTMLString(self.termsHTML, baseURL: nil)
                    }
                }
            }else {
                if contentData.count > 1 {
                    self.termsHTML = contentData[1]["content"] as? String ?? ""
                    BaseThread.asyncMain {
                        self.termsWebView.loadHTMLString(self.termsHTML, baseURL: nil)
                    }
                }

            }
            
            
            
            
            BaseThread.asyncMain {
                
                
                
            }
            
            
        }
    }
    

}
