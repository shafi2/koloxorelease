//
//  KXWebViewController.swift
//  Koloxo
//
//  Created by Appzoc on 11/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXWebViewController: UIViewController {

    var link:String = ""
    var isLocal:Bool = false
    var dataForLocal:Data?
    
    @IBOutlet weak var webVIew: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("link: ",link)
        if isLocal{
            showLocalFile()
        }else{
            showServerFIle()
        }
        
        // Do any additional setup after loading the view.
    }
    
    func showLocalFile(){
       // if let loadData = dataForLocal{
            //webVIew.load(loadData, mimeType: "application/pdf", textEncodingName: "", baseURL: URL(string:link)!)
            let request = URLRequest(url: URL(string:link)!)
            webVIew.loadRequest(request)
        //}
    }
    
    func showServerFIle(){
        let username = "dev"
        let password = "dev"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        var request = URLRequest(url: URL(string: "\(baseDocURL)\(link)")!)
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        
        webVIew.loadRequest(request)
    }

  
    @IBAction func closeBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
