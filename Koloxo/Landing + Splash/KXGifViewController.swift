//
//  KXGifViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 27/03/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import FLAnimatedImage

class KXGifViewController: UIViewController {
    @IBOutlet var imageview: FLAnimatedImageView!
    @IBOutlet var descriptionLBL: UILabel!
    
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()

        BaseThread.asyncMainDelay(seconds: 1.0) {
            
            let gifImageUrl = Bundle.main.path(forResource: "Koloxo", ofType: "gif")
            let gifData = try? Data(contentsOf: URL(fileURLWithPath: gifImageUrl!))
            let animGIFImage = FLAnimatedImage(animatedGIFData: gifData)
            self.imageview.animatedImage = animGIFImage
            self.timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.navigate), userInfo: nil, repeats: true)
            self.descriptionLBL.isHidden = false
        }
        
        getMonthsWeb()
        getCountryListWeb()
    }
    
    @objc func navigate()
    {
        timer.invalidate()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "KXLandingScreenViewController") as! KXLandingScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    private func getMonthsWeb(){
        let url = "getMonths"
        Webservices.getMethodWithoutLoading(url: url) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["Data"] as? JsonArray, !dataArray.isEmpty else { return }
            let months = KXMonthsModel.getValue(fromArray: dataArray)
            SessionStore.current.monthData = months.sorted(by: { $0.display_order < $1.display_order })
            print("receivedMonths",SessionStore.current.monthData?.count as Any)
        }
    }

    public func getCountryListWeb(){
        let countyresidenceURL = "get_countyresidence"
        Webservices.getMethodWithoutLoading(url: countyresidenceURL) { (Succeeded, response) in
            guard Succeeded else { return }
            guard let data = response["Data"] as? Json, let countryArray = data["country"] as?  JsonArray, !countryArray.isEmpty else { return }
            let countryData = KXCountryListModel.getValue(fromArray: countryArray)
            KXCountryListModel.shared.list = countryData
            SessionStore.current.countryData = countryData
        }
    }

}
