//
//  KXLandingScreenCollectionViewCell.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 13/03/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXLandingScreenCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var label_1: UILabel!
    @IBOutlet var label_2: UILabel!
    @IBOutlet var imageview: UIImageView!
    @IBOutlet var informationTextView: UITextView!
}
