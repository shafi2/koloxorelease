//
//  KXLandingScreenViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 13/03/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import Kingfisher

class KXLandingScreenViewController: UIViewController, InterfaceSettable {
    
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var descriptionCV: UICollectionView!
    
    
    fileprivate var currentIndexPath = IndexPath(item: 0, section: 0)
    fileprivate var subtitlleArray = ["BUY, RENT OR SELL FROM AROUND THE WORLD","HOW TO MAKE DEAL WITHOUT MIDDLE HAND","HOW TO FIND A GOOD TENANT AROUND THE WORLD","THE SOLUTION IS KOLOXO HOME"]
    
    fileprivate var informationArray:[[String]] = []
    
    
    fileprivate var dataSource: [KXLandingModel] = []
    fileprivate var count = Int()
    fileprivate let images: [UIImage] = [UIImage(named: "1")!,UIImage(named: "2")!,UIImage(named: "globe_edited")!] // // UIImage(named: "4")!
    fileprivate var offerTimer:Timer? = Timer()
    private var shouldHoldTimer: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            setupInformations()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPress.minimumPressDuration = 0.5
        longPress.delaysTouchesBegan = true
        self.descriptionCV.addGestureRecognizer(longPress)

    }
    
    
    
    //UILongPressGestureRecognizer action - Collection view
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizerState.ended {
            //When lognpress is start or running
            print("started long press gesture")
            shouldHoldTimer = true
        }
        else {
            //When lognpress is finish
            shouldHoldTimer = false
        }
    }

    
    
    fileprivate func setupInformations(){
       let informationStringArray1 = ["The only social media of property to buy sell and rent all over the world, a revolution App","No middle hand","No Commissions","Direct contract with the owner / tenant / buyer", "100% free app"]
        
        let informationStringArray2 = ["What if! I can book an apartment in advance, even if it come to one year before I need to move","What if! I can find some one who looking for the same apartment I have and want to move in on the day I move out","What if! I can get the background of the person I deal with","What if! all this was real!"]
        
        //let informationStringArray3 = ["Welcome to Koloxo Home"]
        
        let informationStringArray4 = ["Avoid problems","Use experience of other benefits","Reserve your move in advance time","Find your best location within the search area","Find your type of tenant for your property","Save commissions and increase the transparency of the operation","Build your record for your future deals"]
        
        informationArray.append(informationStringArray1)
        informationArray.append(informationStringArray2)
        //informationArray.append(informationStringArray3)
        informationArray.append(informationStringArray4)

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //setUpInterface()

    }
    
    func setUpInterface() {
//        descriptionCV.isHidden = true
        descriptionCV.reloadData()
        self.pageControl.transform = CGAffineTransform(scaleX: 1, y: 1)
        self.descriptionCV.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        offerTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        offerTimer?.invalidate()
        offerTimer = nil
    }
    
    @IBAction func skipButton(_ sender: Any)
    {
        offerTimer?.invalidate()
        offerTimer = nil
        let homeScreen = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
}


// Handling Collection view delegates and data sources
extension KXLandingScreenViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return informationArray.count // dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        //print("indexpath \(indexPath.row)")
        
        let sourceInformation = informationArray[indexPath.row]
        
        if indexPath.row == informationArray.count + 1
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXLandingLastScreenCollectionViewCell", for: indexPath) as! KXLandingLastScreenCollectionViewCell
//
//            let unicodeCircle: String = "\u{25CF}" //"\u{25CF}"   2B24 26AB
//            var fullText: NSMutableAttributedString = NSMutableAttributedString()
//
//            for item in sourceInformation {
//                let lineText = unicodeCircle + " " + item + "\n"
//                fullText.append(NSAttributedString(string: lineText))
//            }
//            cell.informationTextView.attributedText = fullText
            return cell
            
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXLandingScreenCollectionViewCell", for: indexPath) as! KXLandingScreenCollectionViewCell
            
            if indexPath.row == 0
            {
                cell.label_1.isHidden = true
            }
            else
            {
                cell.label_1.isHidden = true
            }
            
            cell.label_2.isHidden = true
            cell.informationTextView.isHidden = true

//            cell.informationTextView.scrollRangeToVisible(NSRange(location:0, length:0))

            /*if indexPath.row == 2 {
                cell.label_2.isHidden = false
                cell.informationTextView.isHidden = true
            }else {
                cell.label_2.isHidden = true
                cell.informationTextView.isHidden = false

            }
*/

            let unicodeCircle: String = "- " //"\u{25CF}"          // "\u{2022}" // //"\u{25CF}"   2B24 26AB U+2332 U+1390
            let fullText: NSMutableAttributedString = NSMutableAttributedString()
            let style = NSMutableParagraphStyle()
            style.headIndent = 10
            style.lineSpacing = 2
            let attributes = [NSAttributedStringKey.font : UIFont(name: "Montserrat-Regular", size: 15), NSAttributedStringKey.paragraphStyle : style]
           
            for item in sourceInformation {
                let whatIfText = "What if!"

                if item.contains(whatIfText) {
                    let lineText = item + "\n\n"

                    let newLineText = lineText.replacingOccurrences(of: whatIfText, with: "")
                    let attributedText = NSAttributedString(string: newLineText, attributes: attributes as Any as? [NSAttributedStringKey : Any])

                    let boldAttributes = [NSAttributedStringKey.font : UIFont(name: "Montserrat-Bold", size: 15), NSAttributedStringKey.paragraphStyle : style]
                    let boldAttributedText = NSMutableAttributedString(string:whatIfText, attributes: boldAttributes as Any as? [NSAttributedStringKey : Any])
                    
                    let hyphenAttributedText = NSMutableAttributedString(string: unicodeCircle, attributes: attributes as Any as? [NSAttributedStringKey : Any])
                    
                    boldAttributedText.append(attributedText)
                    hyphenAttributedText.append(boldAttributedText)
                    fullText.append(hyphenAttributedText)
                    
                }else {
                    let lineText = unicodeCircle + item + "\n\n"

                    let attributedText = NSAttributedString(string: lineText, attributes: attributes as Any as? [NSAttributedStringKey : Any])
                    fullText.append(attributedText)

                }
                
            }
            
            cell.informationTextView.attributedText = fullText
            //cell.label_2.text = subtitlleArray[indexPath.row]
            cell.imageview.image = images[indexPath.row]
            cell.imageview.contentMode = .scaleAspectFill
            
            cell.informationTextView.scrollRangeToVisible(NSRange(location:0, length:0))
            if indexPath.item == 0 {
                BaseThread.asyncMainDelay(seconds: 0.6, {
                    cell.informationTextView.isHidden = false
                    cell.informationTextView.flashScrollIndicators()
                })

            }else {
                cell.informationTextView.isHidden = false

            }
            return cell
        }
        
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        
        return CGSize(width: width, height: height)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UICollectionView {
            offerTimer?.invalidate()
            offerTimer = nil
            //print("Velocity:",scrollView.panGestureRecognizer.velocity(in: scrollView),"\n Translation: ",scrollView.panGestureRecognizer.translation(in: scrollView))
            if currentIndexPath.row == 3{
                let homeScreen = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
                self.navigationController?.pushViewController(homeScreen, animated: true)
            }
    //        if scrollView.panGestureRecognizer.velocity(in: scrollView) == CGPoint(x: 0, y: 0){
    //            let homeScreen = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
    //            self.navigationController?.pushViewController(homeScreen, animated: true)
    //        }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("scrollViewDidEndDecelerating")
        if let _ = scrollView as? UICollectionView {
            offerTimer?.invalidate()
            offerTimer = nil
            offerTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
            var visibleRect = CGRect()
            visibleRect.origin = descriptionCV.contentOffset
            visibleRect.size = descriptionCV.bounds.size
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = descriptionCV.indexPathForItem(at: visiblePoint) else { return }
            currentIndexPath = indexPath
            pageControl.currentPage = currentIndexPath.item
        }
    }
    
    @objc func scrollToNextCell() {
        //print("scrollToNextCell")
        guard !shouldHoldTimer else { return }
        //print("scrollToNextCell-Executing..")

        guard informationArray.count > 0 else { return }
        if informationArray.count > currentIndexPath.item + 1{
            let nextIndexpath = IndexPath(item: currentIndexPath.item + 1, section: 0)
            descriptionCV.scrollToItem(at: nextIndexpath, at: .centeredHorizontally, animated: true)
            currentIndexPath = nextIndexpath
            pageControl.currentPage = currentIndexPath.item
        }else{
            offerTimer?.invalidate()
            offerTimer = nil
            let homeScreen = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)

        }
    }
}

// Mark:- Handing web service

extension KXLandingScreenViewController {
    
    fileprivate func geDataWeb(){
        let url = ""
        Webservices2.getMethod(url: url) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? JsonArray, !data.isEmpty else { return }
            self.dataSource = KXLandingModel.getValue(fromArray: data)
            BaseThread.asyncMain {
                self.descriptionCV.reloadData()
            }
        }
        
    }
    
}



/*
 // Commented codes
 
 func setTimer()
 {
 timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
 }
 
 
 
 @objc func autoScroll()
 {
 if x < 4
 {
 self.pageControl.currentPage = x
 let indexPath = IndexPath(item: x, section: 0)
 self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
 x = x + 1
 }
 else
 {
 timer.invalidate()
 let homeScreen = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
 self.navigationController?.pushViewController(homeScreen, animated: true)
 }
 
 
 }
 
 */
