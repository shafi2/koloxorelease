//
//  KXForgetViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 05/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXForgetViewController: UIViewController {

    @IBOutlet var emailTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        emailTF.addPaddingLeft(size: 10)
//        emailTF.addPaddingRight(size: 10)
    }

    @IBAction func Request_send(_ sender: Any) {
    
        postData()
        
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KXCheckmailViewController") as! KXCheckmailViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    
    }
}


extension KXForgetViewController{
    fileprivate func postData(){
        guard validateData() else { return }
        
        var parameter = Json()
        let forgotPasswordURL = "forgot_password"
        parameter.updateValue(emailTF.text!, forKey: "email")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_token")
        
        WebservicesLogin.postMethod(url: forgotPasswordURL, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            if let ErrorCode = response["ErrorCode"] as? String {
                if ErrorCode == "0" {
                    BaseThread.asyncMain {
                        //showBanner(message: "Successfully send email", style: .success)
                        let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXCheckmailViewController") as! KXCheckmailViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else {
//                    if let message = response["Message"] as? String {
//                        showBanner(message: message)
//                    }else {
//                        showErrorBanner(result: response)
//                    }
                    if ErrorCode == "4" {
                        BaseAlert.completionAlert(withTitle: "Email not exit", message: "Kindly create an account", tintColor: .themeColor, andPresentOn: self, completion: {
                            let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXSignup") as! KXSignup
                            self.navigationController?.pushViewController(vc, animated: true)

                        })
                        
                    }
                    
                    
                }
            }
            
            debugPrint("FORGETPASSWORD:",response as AnyObject)
        }
        
        
    }
    
    fileprivate func validateData() -> Bool {
        if BaseValidator.isValid(email: emailTF.text) {
            return true
        }else{
            showBanner(message: "Please Enter Valid Email")
            return false
        }
    }
}
