//
//  KXLoginViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 13/03/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import FacebookLogin

//func stringClassFromString(_ className: String) -> AnyClass! {
//
//    /// get namespace
//    let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;
//
//    /// get 'anyClass' with classname and namespace
//    let cls: AnyClass = NSClassFromString("\(namespace).\(className)")!;
//
//    // return AnyClass!
//    return cls;
//}


//class KXRestoreClassHandler{
//
//
//
//
//
//}

class KXLoginViewController: UIViewController {

    @IBOutlet var emailTF: UITextField!
    @IBOutlet var passwordTF: UITextField!

    fileprivate var facebookId: String = ""
    fileprivate var facebookEmail: String = ""
    fileprivate var facebookFirstName: String = ""
    fileprivate var facebookLastName: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func Login_Button(_ sender: Any) {
        self.postNormalLoginWeb()
    }
    
    @IBAction func FbLogin_button(_ sender: Any) {
        
        Facebook.LoginRequest(viewcontroller: self) { (isSucceeded, userInfo) in
            if isSucceeded {
                
                self.facebookId = userInfo.facebookId
                self.facebookEmail = userInfo.email
                self.facebookFirstName = userInfo.firstName
                self.facebookLastName = userInfo.lastName
                self.postFacebookLoginWeb()
            }else {
                showBanner(message: "Facebook login failed.Try Again")
            }
        }
        
    }
    

    @IBAction func forget_password(_ sender: Any) {
    
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXForgetViewController") as! KXForgetViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Sign_up(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXSignup") as! KXSignup
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Close_Button(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

}



// Mark:- Handling Web service

extension KXLoginViewController {
    
    fileprivate func postNormalLoginWeb() {
        let  loginURL = "login"

        guard validateData() else { return }
        var parameter = Json()
        parameter.updateValue(emailTF.text!, forKey: "email")
        parameter.updateValue(passwordTF.text!, forKey: "password")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_token")
        
        WebservicesLogin.postMethod(url: loginURL, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let Errorcode = response["ErrorCode"] as? String else{ return }
            if Errorcode == "0" {
                guard let dataObject = response["Data"] as? Json, !dataObject.isEmpty else { return }
                let data = KXLoginModel.getValue(dataObject)
                BaseMemmory.write(object: data.user_id.description, forKey: "userId")
                BaseMemmory.write(object: data.first_name, forKey: "firstName")
                BaseMemmory.write(object: data.last_name, forKey: "lastName")
                BaseMemmory.write(object: data.country_residence, forKey: "location")
                BaseMemmory.write(object: data.reputed, forKey: "reputed")

                Credentials.shared.userId = data.user_id.description
                Chat.registerForPush()
                Credentials.shared.emailId = data.email.description
                Credentials.shared.firstName = data.first_name
                Credentials.shared.lastName = data.last_name
                Credentials.shared.mobile = data.mobile_number.description
                //Credentials.shared.country = data.country_residence
                Credentials.shared.profileImage = data.image
                Credentials.shared.reputed = data.reputed.description
                MySearchesManager.instance.removeDataFromStore()
                Chat.initializeChat()
                
                BaseThread.asyncMain {
                    self.segueHome()
                }
            }else if Errorcode == "2" {
            }else if Errorcode == "3" {
            }else if Errorcode == "4" {
                let Message = response["Message"] as? String ?? "User Not Found"
                showBanner(message: Message)
            }else if Errorcode == "5" {
                BaseThread.asyncMain {
                    let Message = response["Message"] as? String ?? "User OTP not Verified"
                    showBanner(message: Message)
                    guard let dataObject = response["Data"] as? Json, !dataObject.isEmpty else { return }

                    let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXVerificationController") as! KXVerificationController
                    let otp = dataObject["otp"] as? Int ?? 0
                    let email = dataObject["email"] as? String ?? ""
                    let reputedStatus = dataObject["reputed"] as? Int ?? 0
                    vc.otpNumber = otp.description
                    vc.emailId = email
                    vc.user_id = dataObject["user_id"] as? Int ?? 0
                    vc.reputedStatus = reputedStatus
                    
                    self.navigationController?.pushViewController(vc, animated: true)

                }
            }

            
        }
        
    }

    fileprivate func postFacebookLoginWeb(){
        let fbLoginURL = "facebook_login"
        
        var parameter = Json()
        parameter.updateValue(facebookEmail, forKey: "email")
        parameter.updateValue(facebookId, forKey: "facebook_id")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_token")
        
        WebservicesLogin.postMethod(url: fbLoginURL, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let Errorcode = response["ErrorCode"] as? String else{ return }
            if Errorcode == "0" {
                guard let dataObject = response["Data"] as? Json, !dataObject.isEmpty else { return }
                let data = KXLoginModel.getValue(dataObject)
                print(data.user_id)
                BaseMemmory.write(object: data.user_id.description, forKey: "userId")
                BaseMemmory.write(object: data.first_name, forKey: "firstName")
                BaseMemmory.write(object: data.last_name, forKey: "lastName")
                BaseMemmory.write(object: data.country_residence, forKey: "location")
                BaseMemmory.write(object: data.reputed, forKey: "reputed")

                Credentials.shared.userId = data.user_id.description
                Chat.registerForPush()
                Credentials.shared.emailId = data.email.description
                Credentials.shared.firstName = data.first_name
                Credentials.shared.lastName = data.last_name
                Credentials.shared.mobile = data.mobile_number.description
               // Credentials.shared.country = data.country_residence
                Credentials.shared.profileImage = data.image
                Credentials.shared.reputed = data.reputed.description
                MySearchesManager.instance.removeDataFromStore()
                Chat.initializeChat()
                
                BaseThread.asyncMain {
                    self.segueHome()
                }
            }else if Errorcode == "2" {
            }else if Errorcode == "3" {
            }else if Errorcode == "4" {
                BaseThread.asyncMain {
                    let Message = response["Message"] as? String ?? "User not registered. Please register"
                    showBanner(message: Message)
                    
                    let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXFbViewController") as! KXFbViewController
                    vc.firstName = self.facebookFirstName
                    vc.lastName = self.facebookLastName
                    vc.email = self.facebookEmail
                    vc.facebookId = self.facebookId
                    self.navigationController?.pushViewController(vc, animated: true)

                }
            }else if Errorcode == "5" {
                // email field is empty case
                BaseThread.asyncMain {
                    let Message = response["Message"] as? String ?? "User not registered. Please provide email address"
                    showBanner(message: Message)
                    self.showAlertWithTextEntry()
                }
            }
            

        }
        

    }
    
    // alert with textfield
    fileprivate func showAlertWithTextEntry(){
        let alert = UIAlertController(title: "", message: "Enter your email to continue", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:
            { [weak alert] (_) in
                let textField = alert?.textFields![0]
                if BaseValidator.isValid(email: textField!.text!) {
                    self.postFacebookLoginWeb()
                }
                else {
                    showBanner(message: "Please enter a valid email")
                    self.showAlertWithTextEntry()
                }
        }))
        alert.addAction((UIAlertAction(title: "Cancel", style: .cancel, handler: nil)))
        self.present(alert, animated: true, completion: nil)
        
    }

    // validation
    fileprivate func validateData() -> Bool {
        if BaseValidator.isValid(email: emailTF.text) && BaseValidator.isNotEmpty(string: passwordTF.text) {
            return true
        }else{
            if !BaseValidator.isValid(email: emailTF.text) {
                showBanner(message: "Please enter a valid email")
                return false
            }
            if !BaseValidator.isNotEmpty(string: passwordTF.text) {
                showBanner(message: "Enter password")
                return false
            }
            return false
        }
    }
    
    fileprivate func segueHome(){
       let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

