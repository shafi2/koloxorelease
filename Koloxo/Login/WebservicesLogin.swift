//
//  WebservicesFacebookLogin.swift
//  Koloxo
//
//  Created by Appzoc on 01/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//


import Foundation
import Alamofire
import NotificationBannerSwift




final class WebservicesLogin
{
    private init(){}
    
    
    //Mark:- POST Methods
    class func postMethod(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request("\(baseURL2)\(url)",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0" {
                            // success
                            CompletionHandler(true, result)
                            
                        }else if Errorcode == "1" {
                            // failure
                            CompletionHandler(false, result)
                            showErrorBanner(result: result)
                        }else {
                            // success with multiple validation error conditions
                            CompletionHandler(true, result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                }
        }
    }

    
    
    class func getMethod(url:String, CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL2)\(url)")
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL2)\(url)",
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    break
                }
        }
    }
    
    // Parameterised GET Method
    class func getMethodWith(url:String, parameter: [String:Any]?,CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL2)\(url)")
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        print(parameter as Any)
        Alamofire.request("\(baseURL2)\(url)",
            method: .get,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                debugPrint(response as AnyObject)
                switch(response.result)
                {
                case .success(_):
                    activity.stop()
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        let Message = result["Message"] as? String ?? "Error occured while fetching data"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)
                            //showBanner(title: "", message: Message)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    break
                }
        }
    }
    
    
    
    
    
    // Mark:- Muiltipart data web call
    
    class func postMethodMultiPartImage(url:String, parameter:[String:Any], imageParameter: [String:[UIImage]] , CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL2)\(url)")
        
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        Alamofire.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
                if !imageParameter.isEmpty {
                    let imageKey = imageParameter[imageParameter.startIndex].key
                    let imageArray = imageParameter[imageKey]
                    var index: Int = 1
                    for image in imageArray! {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = imageKey + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                    
                }
                
        },
                         to: "\(baseURL)\(url)",
            headers : headers,
            encodingCompletion:
            { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        if response.result.value != nil
                        {
                            activity.stop()
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["ErrorCode"] as? String ?? "1"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["":""])
                                showErrorBanner(result: result)
                                //showBanner(title: "", message: Message)
                            }
                        }
                    })
                    
                case .failure(let error):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                }
        }
        )
        
    }
}





