//
//  PhotoViewerVC.swift
//  sample
//
//  Created by Appzoc on 04/11/17.
//  Copyright © 2017 appzoc. All rights reserved.
//

import UIKit

class PhotoViewerVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource
{
    @IBOutlet var largeImageView: UIImageView!
    @IBOutlet var contentCollectionView: UICollectionView!
    @IBOutlet var imageScrollView: UIScrollView!
    
    @IBOutlet var countLBL: UILabel!
     
    @IBOutlet weak var leftArrow: UIImageView!
    
     var iSMale = false
      var imagesArray_String = [""]
    var imageFrom = [""]
    
    
    var selectedIndexPath:Int = 0
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.leftArrow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        
        
        imageScrollView.minimumZoomScale = 1.0
        imageScrollView.maximumZoomScale = 5.0
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        imageScrollView.addGestureRecognizer(doubleTapGest)
        
        
        
        
        //self.leftArrow.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI - 3.14159));
            
    
//        var leftImage = UIImage(CGImage: cgImage(resourceName: "Arrow").CGImage, scale: 1.0, orientation: .DownMirrored)
//
//
//    leftArrow.image = leftArrow!
        

    }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated) // No need for semicolon
          
          
          imagesArray_String.removeAll()
          imagesArray_String = imageFrom

          
          
          largeImageView.kf.setImage(with: URL(string: baseImageURL2 + imagesArray_String[0]), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
//        sd_setImage(with: URL(string:imagesArray_String[0]), placeholderImage: UIImage(named: "male-with-bg.png"))
        
          
          
          contentCollectionView.dataSource = self
          contentCollectionView.delegate = self
          contentCollectionView.reloadData()
          
          
          imageScrollView.delegate = self
          imageScrollView.backgroundColor = UIColor(red: 90, green: 90, blue: 90, alpha: 0.90)
          imageScrollView.alwaysBounceVertical = false
          imageScrollView.alwaysBounceHorizontal = false
          imageScrollView.showsVerticalScrollIndicator = true
          imageScrollView.flashScrollIndicators()
          
          imageScrollView.minimumZoomScale = 1.0
          imageScrollView.maximumZoomScale = 3.0
          
          
          largeImageView.clipsToBounds = true
          
          countLBL.text = "\(selectedIndexPath+1) / \(imagesArray_String.count) Photos"
          
          
     }
    
    
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if imageScrollView.zoomScale == 1 {
            imageScrollView.zoom(to: zoomRectForScale(scale: imageScrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            imageScrollView.setZoomScale(1, animated: true)
        }
    }
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return largeImageView
    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = largeImageView.frame.size.height / scale
        zoomRect.size.width  = largeImageView.frame.size.width  / scale
        let newCenter = largeImageView.convert(center, from: imageScrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func forwardPressed(_ sender: UIButton)
    { forwardAction()
    }
    
    @IBAction func backwardPressed(_ sender: UIButton)
    { backwardAction()
    }

    @IBAction func swipedRight(_ sender: UISwipeGestureRecognizer)
    { forwardAction()
    }
    
    @IBAction func swipedLeft(_ sender: UISwipeGestureRecognizer)
    { backwardAction()
    }
    
    
    func forwardAction()
    {
        if selectedIndexPath < (imagesArray_String.count-1)
        {
            selectedIndexPath+=1
            countLBL.text = "\(selectedIndexPath+1) / \(imagesArray_String.count) Photos"
             largeImageView.kf.setImage(with: URL(string: baseImageURL2 + imagesArray_String[0]), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            //sd_setImage(with: URL(string:imagesArray_String[selectedIndexPath]), placeholderImage: UIImage(named: "male-with-bg.png"))
            contentCollectionView.scrollToItem(at: IndexPath(item: selectedIndexPath, section: 0),
                                               at: .centeredHorizontally, animated: true)
        }
    }
    
    func backwardAction()
    {
        if selectedIndexPath > 0
        {
            selectedIndexPath-=1
            countLBL.text = "\(selectedIndexPath+1) / \(imagesArray_String.count) Photos"
          largeImageView.kf.setImage(with: URL(string: baseImageURL2 + imagesArray_String[0]), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
//            sd_setImage(with: URL(string:imagesArray_String[selectedIndexPath]), placeholderImage: UIImage(named: "male-with-bg.png"))
          
            contentCollectionView.scrollToItem(at: IndexPath(item: selectedIndexPath, section: 0),
                                               at: .centeredHorizontally, animated: true)
        }
    }
    
    
    
}

// Collection View Delegates
extension PhotoViewerVC
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imagesArray_String.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoViewerCVC
     
       cell.contentImageView.kf.setImage(with: URL(string: baseImageURL2 + imagesArray_String[indexPath.row]), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
//        sd_setImage(with: URL(string:imagesArray_String[indexPath.item]), placeholderImage: UIImage(named: "male-with-bg.png"))
     
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        selectedIndexPath = indexPath.item
        countLBL.text = "\(selectedIndexPath+1) / \(imagesArray_String.count) Photos"
     largeImageView.kf.setImage(with: URL(string: baseImageURL2 + imagesArray_String[indexPath.row]), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
//        sd_setImage(with: URL(string:imagesArray_String[indexPath.item]), placeholderImage: UIImage(named: "male-with-bg.png"))
        
        contentCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }

}
