//
//  RentView.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 18/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class RentView: UIView {

    @IBOutlet var monthAnchor: UIView!
    @IBOutlet var rentAnchor: UIView!
    @IBOutlet var SelectCountry: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var month: UILabel!
    
    @IBOutlet var selectCityTF: UITextField!
    
    
    @IBOutlet weak var topContainerView: StandardView!
    
    @IBOutlet var selectCityLBL: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
