//
//  KXGuestSideMenuViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 13/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu

class KXGuestSideMenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back_button(_ sender: Any)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Home(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Login(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
     @IBAction func My_Search(_ sender: Any) {
        let vc = storyboardOfferList!.instantiateViewController(withIdentifier: "KXMySearchesController") as! KXMySearchesController
        self.navigationController?.pushViewController(vc, animated: true)
        
     }
    
    @IBAction func Profile_change(_ sender: Any) {
        return
        let vc = storyboardMain!.instantiateViewController(withIdentifier:    "KXProfileEditVC") as! KXProfileEditVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func but_callAction(_ sender: UIButton) {
        
        
        let alertController = UIAlertController(title: "Koloxo", message: "Do you want to call ? +34 662 121 468", preferredStyle: .alert)
        let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            
             let ownerPhoneNumber = "+34662121468"
             ownerPhoneNumber.makeACall()
            alertController.dismiss(animated: true, completion: nil)
        })
        let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(yesPressed)
        alertController.addAction(noPressed)
        present(alertController, animated: true, completion: nil)
        
    }
    // MARK: - Navigation
/*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
