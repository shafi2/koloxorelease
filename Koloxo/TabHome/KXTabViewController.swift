import UIKit

enum KXSegueType: Int {
    case search
    case chat
    case notification
}

var isLoadedTabSearchChatNotify: KXSegueType = .search
var isFromNotification: Bool = false

class KXTabViewController: UIViewController {
    
    @IBOutlet var bottomImage5: UIImageView!
    @IBOutlet var bottomImage4: UIImageView!
    @IBOutlet var bottomImage3: UIImageView!
    @IBOutlet var bottomImage2: UIImageView!
    @IBOutlet var bottomImage1: UIImageView!
    @IBOutlet var tab5BottomView: UIView!
    @IBOutlet var tab4BottomView: UIView!
    @IBOutlet var tab3BottomView: UIView!
    @IBOutlet var tab2BottomView: UIView!
    @IBOutlet var tab1BottomView: UIView!
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var tab3Container: UIView!
    @IBOutlet var tab4Container: UIView!
    @IBOutlet var tab5NotificationContainerView: UIView!
    
    @IBOutlet var bottomButton1: UIButton!
    @IBOutlet var bottomButton2: UIButton!
    @IBOutlet var bottomButton3: UIButton!
    @IBOutlet var bottomButton4: UIButton!
    @IBOutlet var bottomButton5: UIButton!
    
 //   unreadChatCountLBL notificationCountLBL
    @IBOutlet var notificationCountLBL: UILabel!
    @IBOutlet var unreadChatCountLBL: UILabel!
    
    
    final var segueType: KXSegueType = .search

    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch segueType {
        case .search:
            isLoadedTabSearchChatNotify = segueType
            setUpTabBarSearch()
        case .chat:
            isLoadedTabSearchChatNotify = segueType
            setUpTabBarChat()
        case .notification:
            isLoadedTabSearchChatNotify = segueType
            setUpTabBarNotification()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
   
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
           self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
              self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    

    func  setUpTabBarSearch()
    {
        NotificationCenter.default.post(name: .searchTabActive, object: nil)
        self.tab3Container.isHidden = false // search
        self.tab4Container.isHidden = true // chat
        self.tab5NotificationContainerView.isHidden = true // notification
    }
    
    func  setUpTabBarChat()
    {
        NotificationCenter.default.post(name: .chatTabActive, object: nil)
        self.tab3Container.isHidden = true
        self.tab4Container.isHidden = false
        self.tab5NotificationContainerView.isHidden = true
    }
    
    func setUpTabBarNotification()
    {
        // Handling tabs
        NotificationCenter.default.post(name: .notificationTabActive, object: nil)
        self.tab3Container.isHidden = true
        self.tab4Container.isHidden = true
        self.tab5NotificationContainerView.isHidden = false
    }

    @IBAction func bottomButton1(_ sender: UIButton)
    {
        
    }
    
    @IBAction func homeTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bottomButton2(_ sender: UIButton)
    {
        //setUpTabBarSearch()
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        setUpTabBarSearch()
    }
    
    @IBAction func addPropertyTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func chatTapped(_ sender: UIButton) {
        if isLoggedIn(){
            setUpTabBarChat()
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func notificationTapped(_ sender: UIButton) {
        if isLoggedIn(){
            setUpTabBarNotification()
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func bottomButton3(_ sender: UIButton)
    {
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func bottomButton4(_ sender: UIButton)
    {
        
        //setUpTabBarChat()

    }
    
    // Notification
    @IBAction func bottomButton5(_ sender: UIButton)
    {
       setUpTabBarNotification()
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}

