//
//  ActivityWithProgress.swift
//  Koloxo
//
//  Created by Appzoc on 01/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit

class ActivityWithProgress{
    
    var progressBackground:UIView = {
        let view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor(red:0.26, green:0.26, blue:0.26, alpha:0.8)
        return view
    }()
    
    var progressBar:UIProgressView = {
        return UIProgressView()
    }()
    
    var activityIndicator:UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityView.startAnimating()
       return activityView
    }()
    
    
    func start(){
        
        UIApplication.shared.keyWindow?.addSubview(progressBackground)
        activityIndicator.frame = CGRect(x: (progressBackground.bounds.maxX/2)-20,
                                         y: (progressBackground.bounds.maxY/2)-40,
                                         width: 40, height: 40)
        progressBar.frame = CGRect(x: (progressBackground.bounds.maxX/4),
                                    y: (progressBackground.bounds.maxY/2) + 2,
                                    width: progressBackground.bounds.maxX/2, height: 1)
        progressBackground.addSubview(progressBar)
        progressBackground.addSubview(activityIndicator)
    }
    
    func updateWith(progress:Progress){
        DispatchQueue.main.async {
            if progress.completedUnitCount >= progress.totalUnitCount{
                self.stop()
            }else{
                self.progressBar.setProgress(Float(progress.fractionCompleted), animated: true)
            }
        }
    }
    
    func stop(){
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.progressBackground.removeFromSuperview()
        }
    }
    
}
