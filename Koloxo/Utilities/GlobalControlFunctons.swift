//
//  GlobalControlFunctons.swift
//  Koloxo
//
//  Created by Appzoc on 23/07/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit

class AppBadgeControl{
    private init(){}
    
    static let shared:AppBadgeControl = AppBadgeControl()
    
    func updateBadgeIcon(){
        var currentBadge = UserDefaults.standard.object(forKey: "badge") as? Int ?? 0
        currentBadge += 1
        UIApplication.shared.applicationIconBadgeNumber = currentBadge
        UserDefaults.standard.set(currentBadge, forKey: "badge")
    }
    
    func clearBadgeIcon(){
        let currentBadge = 0
        UIApplication.shared.applicationIconBadgeNumber = currentBadge
        UserDefaults.standard.set(currentBadge, forKey: "badge")
    }
}

class StringManipulation{
    private init(){}
    
    static let shared:StringManipulation = StringManipulation()
    
    func revertFromK(word:String) -> String{
        var line = word
        line.removeLast(2)
        print("Line: ",line)
        if var number = Double(line){
            number *= 1000
            return String(format: "%g", number)
        }else{
            return ""
        }
    }
}
