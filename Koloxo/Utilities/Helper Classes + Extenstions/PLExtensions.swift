//
//  Extensions.swift
//  Popular
//
//  Created by Appzoc on 21/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

// Check whether the optional String is empty or not
protocol StringEmptyable{}
extension String: StringEmptyable{}
extension Optional where Wrapped: StringEmptyable {
    
    var unwrap: String{
        switch self {
        case .none:
            return ""
        case .some(let value):
            return value as! String
        }
    }
}

// Check whether the optional Int is empty or not
protocol IntEmptyable{
}
extension Int: IntEmptyable{
}
extension Optional where Wrapped: IntEmptyable {
    var unwrap: Int{
        switch self {
        case .none:
            return 0
        case .some(let value):
            return value as! Int
        }
    }
}

// Check whether the optional Double is empty or not
protocol DoubleEmptyable{
}
extension Double: DoubleEmptyable{
}
extension Optional where Wrapped: DoubleEmptyable {
   var unwrap: Double{
        switch self {
        case .none:
            return 0.0
        case .some(let value):
            return value as! Double
        }
    }
}

// Check whether the optional Bool is empty or not
protocol BoolEmptyable{
}
extension Bool: BoolEmptyable{
}
extension Optional where Wrapped: BoolEmptyable {
    var unwrap: Bool{
        switch self {
        case .none:
            return false
        case .some(let value):
            return value as! Bool
        }
    }
}

// String is not empty

extension String {
    public var isNotEmpty: Bool {
        return !self.isEmpty
    }
}

// Show price in Kilo "K"

extension String {
    var doubleValue: Double {
        return Double(self)!
    }

}

extension Double {
    
//    func showPriceInKilo() -> String{
//        if self >= 1000 {
//            let dollarPrice:Double = Double(self / 1000)
//            let dollarPriceString = String(format: "%.1f", ceil(dollarPrice*10)/10) // "3.15"
//
//            let priceString = "" + dollarPriceString + " K"
//            return priceString
//        }else {
//            let priceString = "" + self.description
//            return priceString
//        }
//
//    }
    
    func showPriceInKilo() -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        //formatter.currencyCode = "USD"
        let returnString =  formatter.string(from: NSNumber(value: self)) ?? ""
        return returnString
    }
    
    func showPriceInDollar() -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        let currencyString =  formatter.string(from: NSNumber(value: self)) ?? ""
        //let returnString = String(format: "%0f", currencyString)
        return currencyString
    }
    
    func showPriceInMapWithDollar() -> String{
        if self >= 1000000 {
            let dollarPrice:Double = Double(self / 1000000)
            let dollarPriceString = String(format: "%.0f", ceil(dollarPrice*10)/10) // "3.15"
            
            let priceString = "$ " + dollarPriceString + " M"
            return priceString
        }else if self >= 10000 {
            let dollarPrice:Double = Double(self / 1000)
            let dollarPriceString = String(format: "%.0f", ceil(dollarPrice*10)/10) // "3.15"
            
            let priceString = "$ " + dollarPriceString + " K"
            return priceString
        }else {
            let dollarPrice:Double = self
            let dollarPriceString = String(format: "%.0f", ceil(dollarPrice*10)/10) // "3.15"
            let priceString = "$ " + dollarPriceString
            return priceString
        }
        
    }
    
    func showPriceInKiloWithDollar() -> String{
        if self >= 1000 {
            let dollarPrice:Double = Double(self / 1000)
            let dollarPriceString = String(format: "%01f", ceil(dollarPrice*10)/10) // "3.15"
            
            let priceString = "$ " + dollarPriceString + " K"
            return priceString
        }else {
            let priceString = "$ " + self.description
            return priceString
        }
        
    }

    func showPriceInKiloWithoutDollar()-> String{
        if self >= 1000 {
            let dollarPrice:Double = Double(self / 1000)
            let dollarPriceString = String(format: "%01f", ceil(dollarPrice*10)/10) // "3.15"  .1f
            
            let priceString = dollarPriceString + " K"
            return priceString
        }else {
            let priceString = self.description
            return priceString
        }

    }
    
//    let myDouble = 3.141
//    let doubleStr = String(format: "%.2f", myDouble) // "3.14"

    func showPrice2Precision()-> String{
        return String(format: "%.0f", self) // "3.15"  .1f

    }
}



// present view controller from right and dissmiss

extension UIViewController {
    
    var storyboardMain: UIStoryboard? {
        get {
            let device = SessionStore.current.currentDevice
            if device == .iPhone{
                return UIStoryboard(name: "Main", bundle: nil)
            }else{
                return UIStoryboard(name: "Main-iPad", bundle: nil)
            }
        }
    }
    
    var storyboardSearch: UIStoryboard? {
        get {
            let device = SessionStore.current.currentDevice
            if device == .iPhone{
                return UIStoryboard(name: "Search", bundle: nil)
            }else{
                return UIStoryboard(name: "Search-iPad", bundle: nil)
            }
        }
    }
    
    var storyboardProperty: UIStoryboard? {
        get {
            let device = SessionStore.current.currentDevice
            if device == .iPhone{
                return UIStoryboard(name: "Property", bundle: nil)
            }else{
                return UIStoryboard(name: "Property-iPad", bundle: nil)
            }
        }
    }
    
    var storyboardChat: UIStoryboard? {
        get {
            let device = SessionStore.current.currentDevice
            if device == .iPhone{
                return UIStoryboard(name: "Chat", bundle: nil)
            }else{
                return UIStoryboard(name: "Chat-iPad", bundle: nil)
            }
        }
    }
    
    var storyboardDashboard: UIStoryboard? {
        get {
            let device = SessionStore.current.currentDevice
            if device == .iPhone{
                return UIStoryboard(name: "Dashboard", bundle: nil)
            }else{
                return UIStoryboard(name: "Dashboard-iPad", bundle: nil)
            }
        }
    }
    
    var storyboardOfferList: UIStoryboard? {
        get {
            let device = SessionStore.current.currentDevice
            if device == .iPhone{
                return UIStoryboard(name: "Offerlist", bundle: nil)
            }else{
                return UIStoryboard(name: "Offerlist-iPad", bundle: nil)
            }
        }
    }
    
    var storyboardBooking: UIStoryboard? {
        get{
            let device = SessionStore.current.currentDevice
            if device == .iPhone{
                return UIStoryboard(name: "Booking", bundle: nil)
            }else{
                return UIStoryboard(name: "Booking-iPad", bundle: nil)
            }
        }
    }
    
    func presentRight(_ viewControllerToPresent: UIViewController, duration: CFTimeInterval = 0.25 ) {
        let transition = CATransition()
        transition.duration = duration
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissRight(duration: CFTimeInterval = 0.25) {
        let transition = CATransition()
        transition.duration = duration
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
}

extension UIColor {
    
    static let KXNavigationblue = UIColor(red: 76/255, green: 82/255, blue: 202/255, alpha: 1)
    static let KXgradientblue = UIColor(red: 121/255, green: 95/255, blue: 250/255, alpha: 1)
    static let KXViolet = UIColor(red: 83/255, green: 125/255, blue: 255/255, alpha: 1)
    
}


extension UIColor {
    convenience init(rgb: Int) {
        self.init(red: CGFloat((rgb >> 16) & 0xFF), green: CGFloat((rgb >> 8) & 0xFF), blue: CGFloat(rgb & 0xFF), alpha: 1.0)
    }
    
    static var rejectRed = UIColor(red: 1, green: 38/255, blue: 0, alpha: 1)
    static var rejectRedLight = UIColor(red: 247/255, green: 0/255, blue: 0/255, alpha: 1.0)//(red: 222/255, green: 41/255, blue: 13/255, alpha: 1.0)
    static var acceptGreen = UIColor(red: 0/255, green: 143/255, blue: 0/255, alpha: 1)
    static var pendingYellow = UIColor(red: 252/255, green: 101/255, blue: 0/255, alpha: 1)


    static var themeGray = UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1)
    static var pendingOrange = UIColor(red: 232/255,green: 101/255, blue: 24/255, alpha: 1)

    static var availableGreen = UIColor(red: 93/255, green: 226/255, blue: 84/255, alpha: 1)
    static var almostAvailableYellow = UIColor(red: 242/255, green: 194/255, blue: 1/255, alpha: 1)
    static var notAvailableRed = UIColor(red: 233/255, green: 37/255, blue: 1/255, alpha: 1)

    static var themeColor = UIColor(red: 87/255, green: 86/255, blue: 214/255, alpha: 1)

    static var labelPlaceholderColor = UIColor(red: 182/255, green: 183/255, blue: 187/255, alpha: 1)

    static var textViewPlaceholderColor = UIColor(red: 191/255, green: 196/255, blue: 208/255, alpha: 1)
    static var typingTextColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)

    static func from(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    static func formatDate(date:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let dateObj = dateFormatter.date(from: date){
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let dateString = dateFormatter.string(from: dateObj)
            return dateString
        }else{
            return ""
        }
    }
    
    enum DaySimplified{
        case tomorrow
        case yesterday
    }
}

extension String {
    
    func formatRatingToDisplay() -> String{
        return String(self.prefix(3))
    }
    
    func getRatingInDouble() -> Double?{
        let rating = String(self.prefix(3))
        if let ratingValue = Double(rating){
            return ratingValue
        }else{
            return nil
        }
    }
    
    func roundTo(places: Int) -> String {
        return (Double(String(format: "%.\(places)f", self))?.description)!
    }
    
    var double: Double {
        return Double(self)!
    }
    
    var uint64: UInt64 {
        return UInt64(self)!
    }

    var int64: Int64 {
        return Int64(self)!
    }
    
    var cgFloat: CGFloat {
        return CGFloat(self.double)
    }
    
    func convertDate(inputFormat: String, outputFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = inputFormat
        if let date = formatter.date(from: self) {
            formatter.dateFormat = outputFormat
            let dateString = formatter.string(from: date)
            print("DateString:",dateString)
            return dateString
        }else {
            print("DateCalculation Wrongggggggggg , \(self)")
            return ""
        }
        
    }
    
    func convertToADate(inputFormat: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = inputFormat
        if let date = formatter.date(from: self) {
            print("Converted To Date:",date)
            return date
        }else {
            print("Convertion Failure")
            return Date()
        }
        
    }



}

extension Int {
    var float: Float {
        return Float(self)
    }

}

extension Double {
    func roundTo(places: Int = 5) -> Double {
        return (Double(String(format: "%.\(places)f", self)))!
    }
    
    var int64: Int64 {
        return Int64(self)
    }
    
    var Uint64: UInt64 {
        return UInt64(self)
    }

    
    var cgFloat: CGFloat {
        return CGFloat(self)
    }

}

extension Float {
    var int: Int {
        return Int(self)
    }
    
    func roundTo(places: Int) -> Float {
        return (Float(String(format: "%.\(places)f", self)))!
    }

}


extension Int64 {
    var double: Double {
        return Double(self)
    }
    
    var cgFloat: CGFloat {
        return CGFloat(self)
    }

}

extension CGFloat {
    var double: Double {
        return Double(self)
    }
    
    var int64: Int64 {
        return Int64(self)
    }

}
extension Int {
    var double: Double {
        return Double(self)
    }
}
extension URL {
    var filesSize: Double {
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: self.path)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                return size.doubleValue
            }else {
                return 0.0
            }
        } catch {
            do {
                let resources = try self.resourceValues(forKeys:[.fileSizeKey])
                let size = resources.fileSize!
                return size.double
            } catch {
                return 0.0
            }
        }

    }
    
    var fileName: String {
        return (self.lastPathComponent as NSString).deletingPathExtension
    }
}

extension UIViewController {
    public func showSideMenu() {
        BaseThread.asyncMain {
            
            if isLoggedIn() {
                let sideMenuVC =  self.storyboardMain!.instantiateViewController(withIdentifier: "SideMenuNavigationLoggin") as! UISideMenuNavigationController
                self.present(sideMenuVC, animated: true, completion: nil)
                
            }else {
                
                let sideMenuVC =  self.storyboardMain!.instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
                self.present(sideMenuVC, animated: true, completion: nil)
                
            }
            
        }
    }
    
    func back(number: Int, from viewControlller : UIViewController) {
        let viewsBack = number + 1
        let viewControllers: [UIViewController] = viewControlller.navigationController!.viewControllers as [UIViewController]
        viewControlller.navigationController!.popToViewController(viewControllers[viewControllers.count - viewsBack], animated: true)
    }
    
}

extension UIView {
    func setActive() {
        self.isUserInteractionEnabled = true
        self.alpha = 1.0
    }
    
    func setInactive() {
        self.isUserInteractionEnabled = false
        self.alpha = 0.3

    }
}


extension CALayer {
    
    func addSpecificBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        addSublayer(border)
    }
}


//CGRect(0.0, 0.0, aSize.width, aSize.height)
extension UIImage {
    
    func scaleToSize(aSize :CGSize) -> UIImage {
        if (self.size.equalTo(aSize)) { return self };
        UIGraphicsBeginImageContextWithOptions(aSize, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: aSize.width, height: aSize.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return image!
        
    }
    
    func compressToBelow1MB(sizeInKB: Int = 1000) -> UIImage{
        if let imageData = UIImageJPEGRepresentation(self, 1.0), Double(imageData.count / sizeInKB) <= 1.0 {
            return self
        }else {
            if let currentData = UIImageJPEGRepresentation(self, 0.5), Double(currentData.count / sizeInKB) <= 1.0 {
                let currentImage = UIImage(data: currentData)
                return currentImage!
            }else {
                if let currentData = UIImageJPEGRepresentation(self, 0.1) {
                    let currentImage = UIImage(data: currentData)
                    return currentImage!
                }else {
                    return self
                }
                
            }
            
        }
        
    }
    

    //currentImage.resized(toWidth: 700)!.compressToBelow1MB(sizeInKB: 800)

    
}


// Make Call From App with phone number validation
extension String {
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeACall() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}

// Create a padding view for padding on right
extension UITextField {
    
    func addPaddingLeft(size: CGFloat) {
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: size, height: self.frame.height))
        self.leftViewMode = .always

    }
    func addPaddingRight(size: CGFloat) {
        self.rightView = UIView(frame: CGRect(x: 0, y: 0, width: size, height: self.frame.height))
        self.rightViewMode = .always
    }

}


extension UITextView {
    
    func addlineSpacing(point: CGFloat) {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 0
        let attributes = [NSAttributedStringKey.paragraphStyle : style]
        self.attributedText = NSAttributedString(string: self.text, attributes: attributes)
    }
}

extension Notification.Name{
    static let userInvitedToChat = Notification.Name("userInvitedToChat")
    static let searchSourceInfoChanged = Notification.Name("searchSourceInfoChanged")
    static let chatTabActive = Notification.Name("chatTabActive")
    static let notificationTabActive = Notification.Name("notificationTabActive")
    static let searchTabActive = Notification.Name("searchTabActive")
}

extension NSLayoutConstraint {
    override open var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}



//extension Int {
//    func randomIntFrom(start: Int, to end: Int) -> Int {
//        var a = start
//        var b = end
//        
//        // swap to prevent negative integer crashes
//        if a > b {
//            swap(&a, &b)
//        }
//        return Int(arc4random_uniform(UInt32(b - a + 1))) + a
//    }
//}
//extension Int {
//    init(_ range: Range<Int> ) {
//        let delta = range.lowerBound < 0 ? abs(range.lowerBound) : 0
//        let min = UInt32(range.lowerBound + delta)
//        let max = UInt32(range.upperBound   + delta)
//        self.init(Int(min + arc4random_uniform(max - min)) - delta)
//    }
//}



