//
//  PLHelper.swift
//  Popular
//
//  Created by Appzoc on 23/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import NotificationBannerSwift
import CommonCrypto
import SideMenu


public typealias Json = [String: Any]
public typealias JsonArray = [Json]
public typealias JsonParameter = [String: String]
public var mobileLimit: Int = 7 // minimum mobile count
var currentBannerMessage: String = ""

public func showBanner(title: String = "", message: String, style: BannerStyle = .danger){
    BaseThread.asyncMain {
        
        /// this is for showing a banner once if the message is same and if already there is a banner
        if currentBannerMessage == message {
            if NotificationBannerQueue.default.numberOfBanners > 0 {
                return
            }
        }

        let banner = NotificationBanner(title: title, subtitle: message, style: style)
       // banner.backgroundColor = #colorLiteral(red: 0.5224015117, green: 0.2821943164, blue: 0.8902367949, alpha: 1)
        banner.duration = 3
        banner.show(queuePosition: .front, bannerPosition: .top)
        currentBannerMessage = message

    }
}

public func showBannerOn(VC: UIViewController, title: String = "", message: String, style: BannerStyle = .danger){
    BaseThread.asyncMain {
        let banner = NotificationBanner(title: title, subtitle: message, style: style)
        banner.duration = 3
        banner.show(bannerPosition: .top)
    }
}

public func showPriceInKilo(ofPrice: Double) -> String{
    if ofPrice >= 1000 {
        let dollarPrice:Double = Double(ofPrice / 1000)
        let dollarPriceString = String(format: "%.1f", ceil(dollarPrice*10)/10) // "3.15"
        
        let priceString = "$ " + dollarPriceString + " K"
        return priceString
    }else {
        let priceString = "$ " + ofPrice.description
        return priceString
    }
    
}

public func getDateAsString(date:Date?)->String{
    if let dt = date{
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter.string(from: dt)
    }else{
        return ""
    }
}


public func getCountryListWeb(_ completion:@escaping( ([KXCountryListModel])->())){
    
    if KXCountryListModel.shared.list.isEmpty {
        let countyresidenceURL = "get_countyresidence"
        Webservices2.getMethod(url: countyresidenceURL) { (Succeeded, response) in
            guard Succeeded else { return }
            guard let data = response["Data"] as? Json, let countryArray = data["country"] as?  JsonArray, !countryArray.isEmpty else { return }
            KXCountryListModel.shared.list = KXCountryListModel.getValue(fromArray: countryArray)
            completion(KXCountryListModel.shared.list)
        }
    }else {
        completion(KXCountryListModel.shared.list)
    }
}



//public func showBannerStyle(title: String, message: String, style: BannerStyle){
//    NotificationBanner(title: "", subtitle: "Invalid Email", style: .danger).show(queuePosition: .front)
//}

public func getSegueVC(storyboardName: String, identifier: String) -> UIViewController{
    let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
    let segueVC = storyBoard.instantiateViewController(withIdentifier: identifier)
    return segueVC
}


//public func random(digits:Int) -> Int {
//    let min = Int(pow(Double(10), Double(digits-1))) - 1
//    let max = Int(pow(Double(10), Double(digits))) - 1
//    return Int(min...max)
//}

public func randomIntFrom(start: Int, to end: Int) -> Int {
    var a = start
    var b = end
    
    // swap to prevent negative integer crashes
    if a > b {
        swap(&a, &b)
    }
    return Int(arc4random_uniform(UInt32(b - a + 1))) + a
}


public func generateMD5(from fromString: String) -> String{
    
    let messageData = fromString.data(using:.utf8)!
    var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
    _ = digestData.withUnsafeMutableBytes {digestBytes in
        messageData.withUnsafeBytes {messageBytes in
            CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
        }
    }
    print("MD5Sting=",digestData.map { String(format: "%02hhx", $0) }.joined())
    return digestData.map { String(format: "%02hhx", $0) }.joined()
}

func getDateString(from string: String?, format: String = "yyyy-MM-dd HH:mm:ss", outputFormat: String = "MMM dd yyyy") -> String {
    guard let dateString = string, dateString != "" else { return "" }
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    if let timeZone = TimeZone(abbreviation: "UTC"){
        dateFormatter.timeZone = timeZone
    }
    let date = dateFormatter.date(from: dateString)
    dateFormatter.dateFormat = outputFormat
    return dateFormatter.string(from: date!)
}

