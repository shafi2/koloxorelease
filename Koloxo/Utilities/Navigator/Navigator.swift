//
//  Navigator.swift
//  Koloxo
//
//  Created by Appzoc on 06/07/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit

class Navigator{
    private init(){}
    
    static var instance:Navigator = Navigator()
    
    enum Destination{
        case AddPropertyPage
        case SearchPage
    }
}
