//
//  WebService2.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 21/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

//
//  Webservices.swift
//  Exhibiz
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import Alamofire
import NotificationBannerSwift
import CommonCrypto
import Kingfisher


//let baseURL2 = "http://192.168.1.165/Ramsiya/koloxo-homes/api/"
//let baseImageURL2 = "http://192.168.1.165/Ramsiya/koloxo-homes/"
//let baseImageURLDetail = "http://192.168.1.165/Ramsiya/koloxo-homes"


// current
let baseURL2 = "http://koloxo-homes.dev.webcastle.in/api/"
let baseImageURL2 = "http://koloxo-homes.dev.webcastle.in/"
let baseImageURLDetail = "http://koloxo-homes.dev.webcastle.in"

////LIVE URL
//let baseURL2 = "http://app.koloxohome.com/api/"
//let baseImageURL2 = "http://app.koloxohome.com/api"
//let baseImageURLDetail = "http://app.koloxohome.com/api"
//  baseDocURL chnaged to baseImageURLDetail


///Local Server URLs - http://192.168.1.209/koloxo/
//let baseURL2 = "http://192.168.1.209/koloxo/api/"
//let baseImageURL2 = "http://192.168.1.209/koloxo/"
//let baseImageURLDetail = "http://192.168.1.209/koloxo"


let authUserName2 = "dev"
let authPassword2 = "dev"

public func isLoggedIn() -> Bool {
    if let id = BaseMemmory.read(forKey: "userId") as? String, !id.isEmpty, id != "0", id != "-1" {
        Credentials.shared.userId = id
        return true
    }else {
        return false
    }
}

func getImageAuthorization() -> AnyModifier {
    let modifier = AnyModifier { request in
        var requestMutable = request
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        
        requestMutable.setValue("Basic \(base64Credentials)", forHTTPHeaderField: "Authorization")
        
        return requestMutable
    }
    return modifier
}

func getMessageFromJson(response: Json) -> String {
    var messageString = "Error occured while fetching data"
    if let messageJson = response["Message"] as? Json {
        let allkeys = Array(messageJson.keys)
        for item in allkeys {
            let messageArray = messageJson[item]  as! [String]
            messageString = messageArray[0]
        }
    }
    return messageString
}

func showErrorBanner(result: Json){
    if let Message = result["Message"] as? String {
        showBanner(message: Message)
    }else {
        showBanner(message: getMessageFromJson(response: result))
    }
}


final class Webservices2
{

    private init(){}
    
    
    
    
    class func getMethod(url:String, CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL2)\(url)")
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL2)\(url)",
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        //let Message = result["Message"] as? String ?? "Error"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {
                            
                            print("parse error")
                            showErrorBanner(result: result)
                            CompletionHandler(false, ["":""])

                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    break
                }
        }
    }
    
    // Parameterised GET Method
    class func getMethodWith(url:String, parameter: [String:Any]?,CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL2)\(url)")
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        print(parameter as Any)
        Alamofire.request("\(baseURL2)\(url)",
            method: .get,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                debugPrint(response as AnyObject)
                switch(response.result)
                {
                case .success(_):
                    activity.stop()
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)

                            //showBanner(title: "", message: Message)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    break
                }
        }
    }
    
    
    // Mark:- GetMothod with Multiple response Handling
    
    class func getMethodWithMultipleErrorCode(url:String, parameter: [String:Any]?,CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL2)\(url)")
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        print(parameter as Any)
        Alamofire.request("\(baseURL2)\(url)",
            method: .get,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                debugPrint("Response",response as AnyObject)
                switch(response.result)
                {
                case .success(_):
                    activity.stop()
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else if Errorcode == "1"
                        {   CompletionHandler(false, [:])
                            showErrorBanner(result: result)
                            //showBanner(title: "", message: Message)
                        }else {
                            CompletionHandler(true, result)

                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    break
                }
        }
    }
    
    //Mark:- POST Methods
    class func postMethod(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request("\(baseURL2)\(url)",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .responseJSON
            { response in
                activity.stop()
                print("Response-webserviec2Post",response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print("result",result)
                        
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        let Message = result["Message"] as? String ?? "Error occured while fetching data"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
//                            showBanner(title: "", message: Message)
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                }
        }
    }
    
    /// Temp method
    class func postMethod2(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request("\(baseURL2)\(url)",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        
                        let Errorcode = result["ErrorCode"] as? Int ?? 1
                        
                        if Errorcode == 0
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }

                }
        }
    }

    // Mark:- Muiltipart data web call
    
    class func postMethodMultiPartImage(url:String, parameter:[String:Any], imageParameter: [String:[UIImage]] , CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL2)\(url)")
        
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        Alamofire.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
                if !imageParameter.isEmpty {
                    let imageKey = imageParameter[imageParameter.startIndex].key
                    let imageArray = imageParameter[imageKey]
                    var index: Int = 1
                    for image in imageArray! {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = imageKey + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                    
                }
                
        },
                         to: "\(baseURL)\(url)",
            headers : headers,
            encodingCompletion:
            { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        if response.result.value != nil
                        {
                            activity.stop()
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["ErrorCode"] as? String ?? "1"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["":""])
                                showErrorBanner(result: result)
                            }
                        }
                    })
                    
                case .failure(let error):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                }
        }
        )
        
    }
}


// Mark:- Without activity indicator
extension Webservices2 {
    class func getMethodNoActivity(url:String, CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        print("\(baseURL2)\(url)")
        let credentialData = "\(authUserName2):\(authPassword2)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL2)\(url)",
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    break
                }
        }
    }
}



