//
//  SessionStore.swift
//  Koloxo
//
//  Created by Appzoc on 25/07/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit


class SessionStore{
    private init(){}
    
    static let current:SessionStore = SessionStore()
    
    var monthData:[KXMonthsModel]?
    var countryData:[KXCountryListModel]?
    
    var currentDevice:CurrentDevice = .iPhone
}

enum CurrentDevice{
    case iPhone
    case iPad
}
