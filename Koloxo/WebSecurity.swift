//
//  WebSecurity.swift
//  Koloxo
//
//  Created by Appzoc on 04/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import CommonCrypto

//Token and TimeStamp generator

struct SecureSocket{
    
    static func getTimeStamp() -> String{
       return "\(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))"
    }
    
    static func getToken(with timestamp:String) -> String{
        
        let crypticString = "appzoc-koloxohomes-appwebcastle" + timestamp
        let messageData = crypticString.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        _ = digestData.withUnsafeMutableBytes
            {digestBytes in
            messageData.withUnsafeBytes
                {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        print("MD5Sting=",digestData.map { String(format: "%02hhx", $0) }.joined())
    
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
}


//let unixTime = Date().millisecondsSince1970
//
//print("UnixTime",unixTime.description)
//
//let hashKeyString = "appzoc-koloxohomes-appwebcastle" + unixTime.description
//
//print(hashKeyString)
//
//let tockenKey = generateMD5(from: hashKeyString)
//
//parameter.updateValue(tockenKey, forKey: "tokenkey")
//
//parameter.updateValue(unixTime, forKey: "unix_time")



