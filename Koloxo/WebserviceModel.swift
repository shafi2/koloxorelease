//
//  WebserviceModel.swift
//  Koloxo
//
//  Created by Appzoc on 07/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

class KXDetailTypesModel
{
    var id:Int = 0
    var type:String = ""

    init() {
    }
    
    init(dictionary: Json) {
        
        id = dictionary["id"] as? Int ?? -1
        type = dictionary["type"] as? String ?? ""
    }

    class func getValue(fromObject: Json?) -> KXDetailTypesModel {
        guard let fromObject = fromObject else { return KXDetailTypesModel()}
        return KXDetailTypesModel(dictionary: fromObject)
    }

    class func getValue(fromArray: JsonArray) -> [KXDetailTypesModel]
    {
        var result:[KXDetailTypesModel] = []
        for item in fromArray
        {
            result.append(KXDetailTypesModel.getValue(fromObject: item))
        }
        return result
    }

}


class KXDetailsPropertyImageModel: Hashable, Equatable {
    var hashValue: Int { get { return id.hashValue } }
    
    static func ==(lhs: KXDetailsPropertyImageModel, rhs: KXDetailsPropertyImageModel) -> Bool {
        return lhs.id == rhs.id
    }
    

    var id:Int = 0
    var property_id:Int = 0
    var type_id:Int = 0
    var image:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    var types:KXDetailTypesModel?

    init(){}
    
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? -1
        property_id = dictionary["property_id"] as? Int ?? -1
        type_id = dictionary["type_id"] as? Int ?? -1
        image = dictionary["image"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
        types = KXDetailTypesModel.getValue(fromObject: dictionary["types"] as? Json)
    }

    class func getValue(fromObject: Json?) -> KXDetailsPropertyImageModel {
        guard let fromObject = fromObject else { return KXDetailsPropertyImageModel() }
        return KXDetailsPropertyImageModel(dictionary: fromObject)
    }

    class func getValue(fromArray: JsonArray) -> [KXDetailsPropertyImageModel]
    {
        var result:[KXDetailsPropertyImageModel] = []
        for item in fromArray
        {
            result.append(KXDetailsPropertyImageModel.getValue(fromObject: item))
        }
        return result
    
    }

}

class KXDetailPropertyFacilityModel
{
    var id:Int = 0
    var property_id:Int = 0
    var facility_id:Int = 0
    var created_at:String = ""
    var updated_at:String = ""
    var facility: KXDetailPropertyFacilityTypeModel?
    
    init() {}
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? -1
        property_id = dictionary["property_id"] as? Int ?? -1
        facility_id = dictionary["facility_id"] as? Int ?? -1
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
        facility = KXDetailPropertyFacilityTypeModel.getValue(fromObject: dictionary["facility"] as? Json)
    }

    class func getValue(fromObject: Json?) -> KXDetailPropertyFacilityModel {
        guard let fromObject = fromObject else { return KXDetailPropertyFacilityModel() }
        return KXDetailPropertyFacilityModel(dictionary: fromObject)
    }

    class func getValue(fromArray: JsonArray) -> [KXDetailPropertyFacilityModel]
    {
        var result:[KXDetailPropertyFacilityModel] = []
        for item in fromArray
        {
            result.append(KXDetailPropertyFacilityModel.getValue(fromObject: item))
        }
        return result
    }

}


class KXDetailPropertyFacilityTypeModel
{
    var id:Int = 0
    var facilities:String = ""
    
    init() {}
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? -1
        facilities = dictionary["facilities"] as? String ?? ""
    }
    
    class func getValue(fromObject: Json?) -> KXDetailPropertyFacilityTypeModel {
        guard let fromObject = fromObject else { return KXDetailPropertyFacilityTypeModel() }
        return KXDetailPropertyFacilityTypeModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailPropertyFacilityTypeModel]
    {
        var result:[KXDetailPropertyFacilityTypeModel] = []
        for item in fromArray
        {
            result.append(KXDetailPropertyFacilityTypeModel.getValue(fromObject: item))
        }
        return result
    }
    
}


class KXDetailBrochureModel
{
    var id:Int = 0
    var property_id:Int = 0
    var brochure:String = ""
    var created_at:String = ""
    var updated_at:String = ""

    init(){}
    init(dictionary: Json) {

        id = dictionary["id"] as? Int ?? -1
        property_id = dictionary["property_id"] as? Int ?? -1
        brochure = dictionary["brochure"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
    }

    class func getValue(fromObject: Json?) -> KXDetailBrochureModel {
        guard let fromObject = fromObject else { return KXDetailBrochureModel() }
        return KXDetailBrochureModel(dictionary: fromObject)
    }

    class func getValue(fromArray: JsonArray) -> [KXDetailBrochureModel]
    {
        var result:[KXDetailBrochureModel] = []
        for item in fromArray
        {
            result.append(KXDetailBrochureModel.getValue(fromObject: item))
        }
        return result
    }

}

class KXDetailDeedModel
{
    var id:Int = 0
    var property_id:Int = 0
    var titleDeed:String = ""
    var created_at:String = ""
    var updated_at:String = ""

    init() {
    }
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? -1
        property_id = dictionary["property_id"] as? Int ?? -1
        titleDeed = dictionary["titleDeed"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
    }
   
    class func getValue(fromObject: Json?) -> KXDetailDeedModel {
        guard let fromObject = fromObject else { return KXDetailDeedModel() }
        return KXDetailDeedModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailDeedModel]
    {
        var result:[KXDetailDeedModel] = []
        for item in fromArray
        {
            result.append(KXDetailDeedModel.getValue(fromObject: item))
        }
        return result
    }

}

class KXDetailsUsersModel
{
    var id:Int = 0
    var first_name:String = ""
    var last_name:String = ""
    var image:String = ""
    var mobile_number:String = ""
    var email:String = ""
    var password:String = ""
    var country_residence:Int = 0
    var status:Int = 0
    var date:String = ""
    var type:Int = 0
    var facebook_id:String = ""
    var hash:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    var deleted_at:String = ""
    var reputed_profile: String = ""
    var facebook_login: String = ""

    init(){}
    init(dictionary: Json) {

        id = dictionary["id"] as? Int ?? -1
        first_name = dictionary["first_name"] as? String ?? ""
        last_name = dictionary["last_name"] as? String ?? ""
        image = dictionary["image"] as? String ?? ""
        mobile_number = dictionary["mobile_number"] as? String ?? ""
        email = dictionary["email"] as? String ?? ""
        password = dictionary["password"] as? String ?? ""
        country_residence = dictionary["country_residence"] as? Int ?? -1
        status = dictionary["status"] as? Int ?? -1
        date = dictionary["date"] as? String ?? ""
        type = dictionary["type"] as? Int ?? -1
        facebook_id = dictionary["facebook_id"] as? String ?? ""
        hash = dictionary["hash"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
        deleted_at = dictionary["deleted_at"] as? String ?? ""
        reputed_profile = dictionary["reputed_profile"] as? String ?? ""
        facebook_login = dictionary["facebook_login"] as? String ?? ""

    }
    
    class func getValue(fromObject: Json?) -> KXDetailsUsersModel {
        guard let fromObject = fromObject else { return KXDetailsUsersModel() }
        return KXDetailsUsersModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailsUsersModel]
    {
        var result:[KXDetailsUsersModel] = []
        for item in fromArray
        {
            result.append(KXDetailsUsersModel.getValue(fromObject: item))
        }
        return result
    }

}

class KXPropertyModel
{
    var id:Int = 0
    var user_id:Int = 0
    var property_name:String = ""
    var about_property:String = ""
    var price:Int = 0
    var currency_id:Int = 0
    var usd_price:Double = 0.0
    var deposite:String = ""
    var pirce_negotiable:Int = 0
    var reputation:Int = 0
//    var building_type:Int = 0
    var property_type:Int = 1
    var age_building:Int = 0
    var elevator:Int = 0
    var floor_no:Int = 0
    var address:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var state:String = ""
    var country:String = ""
    var profile_status:Int = 0
    var city:String = ""
    var bedroom_count:Int = 0
    var bathroom_count:Int = 0
    var parking_count:Int = 0
    var date_moving:String = ""
    var duration_month:String = ""
    var availableStatus:Int = 1
    var building_id:Int = 0
    var states_completed:Int = 0
    var admin_approval:Int = 0
    var visitors_count:String = ""
    var property_description:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    var deleted_at:String = ""
    var property_ratings_total: [KXDetailRatingAverageModel] = []
    var property_images:[KXDetailsPropertyImageModel] = []
    var property_facilitys:[KXDetailPropertyFacilityModel] = []
    var property_brochures:[KXDetailBrochureModel] = []
    var property_deeds:[KXDetailDeedModel] = []
    var users:KXDetailsUsersModel?
    var news_feeds:[KXDetailNewsFeedModel] = []
    var property_bedrooms: KXFilterCommonTypeModel?
    var property_bathrooms: KXFilterCommonTypeModel?

    var property_ratings:[KXDetailUserReviewModel] = []

    var floorSize:String = "0"
    var buildingName:String = ""
    var architecture_type: String = ""
    var water_heating: String = ""
    var energy_type: String = ""
    
    var building_type:KXDetailsBuildingTypeModel?

    
    
    init() {
        
    }
    
    init(dictionary: Json) {

        id = dictionary["id"] as? Int ?? -1
        user_id = dictionary["user_id"] as? Int ?? -1
        property_name = dictionary["property_name"] as? String ?? ""
        about_property = dictionary["about_property"] as? String ?? ""
        floorSize = dictionary["floor_size"] as? String ?? "0"
        price = dictionary["price"] as? Int ?? 0
        currency_id = dictionary["currency_id"] as? Int ?? -1
        usd_price = dictionary["usd_price"] as? Double ?? 0.0
        deposite = dictionary["deposite"] as? String ?? ""
        pirce_negotiable = dictionary["pirce_negotiable"] as? Int ?? 0
        reputation = dictionary["reputation"] as? Int ?? -1
//        building_type = dictionary["building_type"] as? Int ?? -1
        property_type = dictionary["property_type"] as? Int ?? 1
        age_building = dictionary["age_building"] as? Int ?? 0
        elevator = dictionary["elevator"] as? Int ?? 0
        floor_no = dictionary["floor_no"] as? Int ?? 0
        address = dictionary["address"] as? String ?? ""
        latitude = dictionary["latitude"] as? String ?? ""
        longitude = dictionary["longitude"] as? String ?? ""
        state = dictionary["state"] as? String ?? ""
        country = dictionary["country"] as? String ?? ""
        profile_status = dictionary["profile_status"] as? Int ?? -1
        city = dictionary["city"] as? String ?? ""
        bedroom_count = dictionary["bedroom_count"] as? Int ?? 0
        bathroom_count = dictionary["bathroom_count"] as? Int ?? 0
        parking_count = dictionary["parking_count"] as? Int ?? 0
        date_moving = dictionary["date_moving"] as? String ?? ""
        duration_month = dictionary["duration_month"] as? String ?? ""
        availableStatus = dictionary["availableStatus"] as? Int ?? 1
        building_id = dictionary["building_id"] as? Int ?? -1
        states_completed = dictionary["states_completed"] as? Int ?? -1
        admin_approval = dictionary["admin_approval"] as? Int ?? -1
        visitors_count = dictionary["visitors_count"] as? String ?? ""
        property_description = dictionary["property_description"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
        deleted_at = dictionary["deleted_at"] as? String ?? ""
        property_ratings_total = KXDetailRatingAverageModel.getValue(fromArray: (dictionary["property_ratings_total"] as? JsonArray)!)
        property_images = KXDetailsPropertyImageModel.getValue(fromArray: dictionary["property_images"] as! JsonArray)
        property_facilitys = KXDetailPropertyFacilityModel.getValue(fromArray: dictionary["property_facility"] as! JsonArray)
        property_brochures = KXDetailBrochureModel.getValue(fromArray: dictionary["property_brochure"] as! JsonArray)
        property_deeds = KXDetailDeedModel.getValue(fromArray: dictionary["property_deed"] as! JsonArray)
        users = KXDetailsUsersModel.getValue(fromObject: dictionary["users"] as? Json)
        news_feeds = KXDetailNewsFeedModel.getValue(fromArray: dictionary["news_feed"] as! JsonArray)
        property_ratings = KXDetailUserReviewModel.getValue(fromArray: dictionary["property_rating"] as! JsonArray)
        property_bedrooms = KXFilterCommonTypeModel.getValue(fromObject: dictionary["property_bedrooms"] as? Json)
        property_bathrooms = KXFilterCommonTypeModel.getValue(fromObject: dictionary["property_bathrooms"] as? Json)

        if let buidlingDetail = dictionary["building_detail"] as? [String:Any]{
           self.buildingName = buidlingDetail["name"] as! String
        }
        
        architecture_type = dictionary["architecture_type"] as? String ?? ""
        energy_type = dictionary["energy_type"] as? String ?? ""
        water_heating = dictionary["water_heating"] as? String ?? ""

        
        if let buildingTypeData = dictionary["building_type"] as? Json, !buildingTypeData.isEmpty {
            building_type = KXDetailsBuildingTypeModel.init(object: buildingTypeData)
        }

    }

    class func getValue(fromObject: Json?) -> KXPropertyModel {
        guard let fromObject = fromObject else { return KXPropertyModel() }
        return KXPropertyModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXPropertyModel]
    {
        var result:[KXPropertyModel] = []
        for item in fromArray
        {
            result.append(KXPropertyModel.getValue(fromObject: item))
        }
        return result
    }

    
}

class KXDetailsBuildingTypeModel {
    var id: Int = 0
    var data: String = ""
    var type: Int = 0
    init() {
        
    }
    init(object: Json) {
        id = object["id"] as? Int ?? 0
        data = object["data"] as? String ?? ""
        type = object["type"] as? Int ?? 0
    }
    
}

class KXDetailPropertyFurnitureModel
{
    var id:Int = -1
    var title:String = ""
    var description:String = ""
    var furniture_key:Int = -1
    var property_id:Int = -1
    var attachment:String = ""
    var updated_at:String = ""
    var created_at:String = ""

    init(){}
    init(dictionary: Json) {

        id = dictionary["id"] as? Int ?? -1
        title = dictionary["title"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        furniture_key = dictionary["furniture_key"] as? Int ?? -1
        property_id = dictionary["property_id"] as? Int ?? -1
        attachment = dictionary["attachment"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
    }

    class func getValue(fromObject: Json?) -> KXDetailPropertyFurnitureModel {
        guard let fromObject = fromObject else { return KXDetailPropertyFurnitureModel()}
        return KXDetailPropertyFurnitureModel(dictionary: fromObject)
    }

    class func getValue(fromArray: JsonArray) -> [KXDetailPropertyFurnitureModel]
    {
        var result:[KXDetailPropertyFurnitureModel] = []
        for item in fromArray
        {
            result.append(KXDetailPropertyFurnitureModel.getValue(fromObject: item))
        }
        return result
    }

}

class KXDetailFurnitureModel
{
    var id:Int = 0
    var category:String = ""
    var property_furnitures:[KXDetailPropertyFurnitureModel] = []
    var display_order: Int = 0
    init(){}
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? -1
        category = dictionary["category"] as? String ?? ""
        display_order = dictionary["display_order"] as? Int ?? 0
        property_furnitures = KXDetailPropertyFurnitureModel.getValue(fromArray: dictionary["property_furniture"] as! JsonArray)
    }
    class func getValue(fromObject: Json?) -> KXDetailFurnitureModel {
        guard let fromObject = fromObject else { return KXDetailFurnitureModel()}
        return KXDetailFurnitureModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailFurnitureModel]
    {
        var result:[KXDetailFurnitureModel] = []
        for item in fromArray
        {
            result.append(KXDetailFurnitureModel.getValue(fromObject: item))
        }
        return result
    }


}

class KXPropertyDetailModel
{
    var property:KXPropertyModel?
    var furnitures:[KXDetailFurnitureModel] = []
    var wishlist: Int = 0
    var wishlist_id: Int = 0
    var date_available: String = "" // this is for showing when the property will available (almost_available properties)
    
    init(){}
    init(dictionary: Json) {
        property = KXPropertyModel.getValue(fromObject: dictionary["property"] as? Json)
        furnitures = KXDetailFurnitureModel.getValue(fromArray: dictionary["furnitures"] as! JsonArray)
        wishlist = dictionary["wishlist"] as? Int ?? 0
        wishlist_id = dictionary["wishlist_id"] as? Int ?? 0
        date_available = dictionary["date_available"] as? String ?? ""
    }

    class func getValue(fromObject: Json?) -> KXPropertyDetailModel {
        guard let fromObject = fromObject else { return KXPropertyDetailModel() }
        return KXPropertyDetailModel(dictionary: fromObject)
    }


    class func getValue(fromArray: JsonArray) -> [KXPropertyDetailModel]
    {
        var result:[KXPropertyDetailModel] = []
        for item in fromArray
        {
            result.append(KXPropertyDetailModel.getValue(fromObject: item))
        }
        return result
    }

}

class KXDetailNewsFeedModel
{
    var id:Int = 0
    var property_id:Int = 0
    var url:String = ""
    var title:String = ""
    var description:String = ""
    var upload:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    var deleted_at:String = ""
    
    init(){}
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? -1
        property_id = dictionary["property_id"] as? Int ?? -1
        url = dictionary["url"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        upload = dictionary["upload"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
        deleted_at = dictionary["deleted_at"] as? String ?? ""
    }
    
    class func getValue(fromObject: Json?) -> KXDetailNewsFeedModel {
        guard let fromObject = fromObject else { return KXDetailNewsFeedModel() }
        return KXDetailNewsFeedModel(dictionary: fromObject)
    }
    
    
    class func getValue(fromArray: JsonArray) -> [KXDetailNewsFeedModel]
    {
        var result:[KXDetailNewsFeedModel] = []
        for item in fromArray
        {
            result.append(KXDetailNewsFeedModel.getValue(fromObject: item))
        }
        return result
    }
    
}


// Review Models

class KXDetailUserReviewDetailModel
{
    var id:Int = -1
    var first_name:String = ""
    var last_name:String = ""
    var image:String = ""
    var country_residence:Int = -1
    
    init(){}
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? -1
        first_name = dictionary["first_name"] as? String ?? ""
        last_name = dictionary["last_name"] as? String ?? ""
        image = dictionary["image"] as? String ?? ""
        country_residence = dictionary["country_residence"] as? Int ?? -1
    }
    class func getValue(fromObject: Json?) -> KXDetailUserReviewDetailModel {
        guard let fromObject = fromObject else { return KXDetailUserReviewDetailModel()}
        return KXDetailUserReviewDetailModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailUserReviewDetailModel]
    {
        var result:[KXDetailUserReviewDetailModel] = []
        for item in fromArray
        {
            result.append(KXDetailUserReviewDetailModel.getValue(fromObject: item))
        }
        return result
    }

}


class KXDetailRatingAverageModel
{
    var aggregate:String = ""
    var rating_id:Int = -1
    
    init(){}
    init(dictionary: Json) {
        
        aggregate = dictionary["aggregate"] as? String ?? ""
        rating_id = dictionary["rating_id"] as? Int ?? -1
    }
    
    class func getValue(fromObject: Json?) -> KXDetailRatingAverageModel {
        guard let fromObject = fromObject else { return KXDetailRatingAverageModel()}
        return KXDetailRatingAverageModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailRatingAverageModel]
    {
        var result:[KXDetailRatingAverageModel] = []
        for item in fromArray
        {
            result.append(KXDetailRatingAverageModel.getValue(fromObject: item))
        }
        return result
    }

}

class KXDetailUserReviewModel
{
    var id:Int = 0
    var property_id:Int = 0
    var from_user_id:Int = 0
    var to_user_id:Int = 0
    var comment:String = ""
    var status:Int = 0
    var type:Int = 0
    var created_at:String = ""
    var updated_at:String = ""
    var deleted_at:String = ""
    var users: KXDetailUserReviewDetailModel?
    var rating_averages:[KXDetailRatingAverageModel] = []
    
    init(){}
    init(dictionary: Json) {
        
        id = dictionary["id"] as? Int ?? -1
        property_id = dictionary["property_id"] as? Int ?? -1
        from_user_id = dictionary["from_user_id"] as? Int ?? -1
        to_user_id = dictionary["to_user_id"] as? Int ?? -1
        comment = dictionary["comment"] as? String ?? ""
        status = dictionary["status"] as? Int ?? -1
        type = dictionary["type"] as? Int ?? -1
        created_at = dictionary["created_at"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
        deleted_at = dictionary["deleted_at"] as? String ?? ""
        users = KXDetailUserReviewDetailModel.getValue(fromObject: dictionary["users"] as? Json)
        rating_averages = KXDetailRatingAverageModel.getValue(fromArray: dictionary["rating_average"] as! JsonArray)
    }
    
    class func getValue(fromObject: Json?) -> KXDetailUserReviewModel {
        guard let fromObject = fromObject else { return KXDetailUserReviewModel()}
        return KXDetailUserReviewModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailUserReviewModel]
    {
        var result:[KXDetailUserReviewModel] = []
        for item in fromArray
        {
            result.append(KXDetailUserReviewModel.getValue(fromObject: item))
        }
        return result
    }

}


// review status model


class KXDetailsRequestStatusResponseModel
{
    var request_id:Int = 0
    var status:Int = 3
    
    var property_id: Int?
    var user_id: Int?
    var deal_confirmation: Int?// default status 10
    var moving_in: String?
    var moving_out: String?
    
    init(){}
    init(dictionary: Json) {
        
        request_id = dictionary["Request id"] as? Int ?? -1
        status = dictionary["status"] as? Int ?? -1
        
        if let value = dictionary["property_id"] as? Int {
            property_id = value
        }

        if let value = dictionary["user_id"] as? Int {
            user_id = value
        }
        if let value = dictionary["deal_confirmation"] as? Int {
            deal_confirmation = value
        }
        if let value = dictionary["moving_in"] as? String {
            moving_in = value
        }
        if let value = dictionary["moving_out"] as? String {
            moving_out = value
        }

        
    }
    
    class func getValue(fromObject: Json?) -> KXDetailsRequestStatusResponseModel {
        guard let fromObject = fromObject else { return KXDetailsRequestStatusResponseModel()}
        return KXDetailsRequestStatusResponseModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailsRequestStatusResponseModel]
    {
        var result:[KXDetailsRequestStatusResponseModel] = []
        for item in fromArray
        {
            result.append(KXDetailsRequestStatusResponseModel.getValue(fromObject: item))
        }
        return result
    }

}

class KXDetailsRequestStatusStatusModel
{
    var status0:String = ""
    var status1:String = ""
    var status2:String = ""
    
    init(){}
    init(dictionary: Json) {
        
        status0 = dictionary["status0"] as? String ?? ""
        status1 = dictionary["status1"] as? String ?? ""
        status2 = dictionary["status2"] as? String ?? ""
    }
    class func getValue(fromObject: Json?) -> KXDetailsRequestStatusStatusModel {
        guard let fromObject = fromObject else { return KXDetailsRequestStatusStatusModel()}
        return KXDetailsRequestStatusStatusModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailsRequestStatusStatusModel]
    {
        var result:[KXDetailsRequestStatusStatusModel] = []
        for item in fromArray
        {
            result.append(KXDetailsRequestStatusStatusModel.getValue(fromObject: item))
        }
        return result
    }

}

class KXDetailsRequestStatusModel
{
    var response:KXDetailsRequestStatusResponseModel?
    var status:[String] = []
    var deal_confirmation:Json?
    
    init(){}
    init(dictionary: Json) {
        response = KXDetailsRequestStatusResponseModel.getValue(fromObject: dictionary["response"] as? Json)
        if let object = dictionary["response"] as? Json {
            response = KXDetailsRequestStatusResponseModel.getValue(fromObject: object)
        }else {
            
            if let objectArray = dictionary["response"] as? JsonArray, !objectArray.isEmpty,let object = objectArray.last {
                response = KXDetailsRequestStatusResponseModel.getValue(fromObject: object)
            }
            
        }
        
        status = (dictionary["status"] as? [String])!
        if let deal = dictionary["deal_confirmation"] as? Json {
            deal_confirmation = deal
        }
        
    }
    
    class func getValue(fromObject: Json?) -> KXDetailsRequestStatusModel {
        guard let fromObject = fromObject else { return KXDetailsRequestStatusModel()}
        return KXDetailsRequestStatusModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXDetailsRequestStatusModel]
    {
        var result:[KXDetailsRequestStatusModel] = []
        for item in fromArray
        {
            result.append(KXDetailsRequestStatusModel.getValue(fromObject: item))
        }
        return result
    }

}









