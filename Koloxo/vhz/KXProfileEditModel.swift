//
//  KXRegisterProfileModel.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 19/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

public class KXProfileEditModel{
    public var id: Int = 0
    public var first_name : String = ""
    public var last_name : String = ""
    public var mobile_number : String = ""
    public var email : String = ""
    public var image : String = ""
    public var country_residence : String = ""
    public var reputedprofile: String = ""
    public var created_at: String = ""
    public var user_documents:[userdocumentModel] = []
    
    public class func getValue(fromObject: Json?) -> KXProfileEditModel {
         guard let fromObject = fromObject else { return KXProfileEditModel() }
        return KXProfileEditModel(object: fromObject)!
    }
    
    public class func getValue(fromArray: [Json]?) -> [ KXProfileEditModel] {
        var modelArray : [ KXProfileEditModel] = []
        guard let fromArray = fromArray else { return modelArray }
        for item in fromArray{
         let object = KXProfileEditModel.getValue(fromObject: item)
        modelArray.append(object)
        }
       return modelArray
    }
    
    required public init?(object:Json){
        first_name = object["first_name"] as? String ?? ""
        last_name = object["last_name"] as? String ?? ""
        mobile_number = object["mobile_number"] as? String ?? ""
        email = object["email"] as? String ?? ""
        image = object["image"] as? String ?? ""
        country_residence = object["country_residence"] as? String ?? ""
        reputedprofile = object["reputed_profile"] as? String ?? ""
        created_at = object["created_at"] as? String ?? ""
        user_documents =  userdocumentModel.getValue(from: object["user_doccuments"] as? JsonArray)
    }
    
    private init(){}
    
}


public class userdocumentModel{
    public var id: Int = 0
    public var user_id: Int = 0
    public var document_title : String = ""
    public var document_image:String = ""
    public var created_at: String = ""
        
        class func getValue(from object: Json?) -> userdocumentModel? {
            guard let object = object else { return userdocumentModel()}
            return userdocumentModel(object: object)
        }
        
        class func getValue(from array: JsonArray?) -> [userdocumentModel] {
            var modelArray : [userdocumentModel] = []
            guard let array = array else { return modelArray }
            for item in array{
                let object = userdocumentModel.getValue(from: item)
                modelArray.append(object!)
            }
            return modelArray
        }
    
        required public init?(object:Json){
            id = object["id"] as? Int ?? 0
            user_id = object["user_id"] as? Int ?? 0
            document_title = object["document_title"] as? String ?? ""
            document_image = object["document_image"] as? String ?? ""
            created_at = object["created_at"] as? String ?? ""
        }
    
       private init(){}
}
