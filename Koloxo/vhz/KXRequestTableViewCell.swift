//
//  KXRequestTableViewCell.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 17/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXRequestTableViewCell: UITableViewCell {

    @IBOutlet var propertyImage: UIImageView!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var propertyRequestCountLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
