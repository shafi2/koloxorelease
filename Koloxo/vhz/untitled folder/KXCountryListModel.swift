//
//  KXCountryListModel.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 16/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

public class KXCountryListModel {
    public var id : Int = 0
    public var name : String = ""
    public var country_code: String = ""
    public var flag_image : String = ""
    public var flag_image_128: String = ""
    public var telephone_code: String = ""
    public var status: Int = 0
    public var created_at: String = ""
    public var updated_at: String = ""
    public var deleted_at: String = ""
    public var list:[KXCountryListModel] = []
    
    static var shared: KXCountryListModel = KXCountryListModel()

    init(){}

    public class func getValue(fromObject: Json?) -> KXCountryListModel {
        guard let fromObject = fromObject else { return KXCountryListModel() }
        return KXCountryListModel(object: fromObject)
    }
    
    public class func getValue(fromArray: [Json]?) -> [KXCountryListModel] {
        var modelArray : [KXCountryListModel] = []
        guard let fromArray = fromArray else { return modelArray }
        for item in fromArray{
            let object = KXCountryListModel.getValue(fromObject: item)
            modelArray.append(object)
        }
        return modelArray
    }
    
    init(object: Json) {
        id = object["id"] as? Int ?? 0
        name = object["name"] as? String ?? ""
        country_code = object["country_code"] as? String ?? ""
        flag_image = object["flag_image"] as? String ?? ""
        flag_image_128 = object["flag_image_128"] as? String ?? ""
        telephone_code = object["telephone_code"] as? String ?? ""
        status = object["status"] as? Int ?? 0
        created_at = object["created_at"] as? String ?? ""
        updated_at = object["updated_at"] as? String ?? ""
        deleted_at = object["deleted_at"] as? String ?? ""
    }


}
