//
//  KXOtpVerifyModel.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 17/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
public class  KXOtpVerifyModel {
    public var user_id : Int = 0
    public var otp : Int = 0
    public var image : String = ""
    public var first_name : String = ""
    public var last_name : String = ""
    public var mobile_number : Int = 0
    public var email : String = ""
    public var country_residence : String = ""
    
    
    
    public class func getValue(_ fromObject: Json?) -> KXOtpVerifyModel {
        guard let fromObject = fromObject else { return KXOtpVerifyModel() }
        return KXOtpVerifyModel(object: fromObject)!
    }
    
    required public init?(object: Json) {
        
        user_id = object["user_id"] as? Int ?? 0
        otp = object["otp"] as? Int ?? 0
        first_name = object["first_name"] as? String ?? ""
        last_name = object["last_name"] as? String ?? ""
        mobile_number = object["mobile_number"] as? Int ?? 0
        email = object["email"] as? String ?? ""
        country_residence = object["country_residence"] as? String ?? ""
        image = object["image"] as? String ?? ""
    }
    
    private init(){
        
    }
}
